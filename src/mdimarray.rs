use std::vec::Vec;

use super::LVResult;
use super::LVError;

/// Multidimentional array implemented on top of two vectors holding dimension sizes and actual 
/// values.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct MDimArray<T> {
    /// Dimension sizes.
    pub sizes : Vec<usize>,
    /// All values.
    pub data : Vec<T>,
}

impl<T> MDimArray<T> {
    pub fn new_from_nested_2d_array(array: &[&[T]]) -> LVResult<Self>
        where T: Clone
    {
        let mut sizes = Vec::with_capacity(2);
        sizes.push(array.len());
        if array.len() == 0 {
            sizes.push(0);
            Ok(Self {
                sizes: sizes,
                data: vec![],
            })
        } else {
            sizes.push(array[0].len());
            let mut data = Vec::with_capacity(sizes[0] * sizes[1]);
            for (i, row) in array.iter().enumerate() {
                if row.len() != sizes[1] {
                    Err(LVError::ParseError(format!(
                            "Length of row {} does not match length of row 0",
                            i)))?
                }
                for element in row.iter() {
                    data.push(element.clone());
                }
            }
            Ok(Self {
                sizes: sizes,
                data: data,
            })
        }
    }
}
