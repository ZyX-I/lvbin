/// Fixed-point type description: number of bits, signedness, etc
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct FixedPointOptions {
    /// True if number is signed.
    pub signed: bool,
    /// Number of bits used for number.
    pub total_bits: u8,
    /// Number of bits used for integral part.
    ///
    /// May not be less than [`total_bits`].
    pub int_bits: u8,
}

/// Fixed-point data structure.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct FixedPoint {
    /// Fixed-point number.
    pub data: u64,
    /// Type description.
    pub options: FixedPointOptions,
}
