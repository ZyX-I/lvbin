use std::fmt;
use std::num::FpCategory;
use std::f64;

use super::LVParse;
use super::LVValue;
use super::LVResult;

/// Structure representing LabVIEW extended precision float.
///
/// May be dropped and replaced with actual f128 in the future.
#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct F128 {
    sign: bool,
    exponent: u16,
    significand: u128,
}

impl F128 {
    const SIGNIFICAND_BITS: u8 = 112;
    const EXPONENT_BITS: u8 = 15;
    const LEADING_BITS: u8 = F128::EXPONENT_BITS + 1;

    const EXPONENT_BITS_MASK: u16 = (1 << F128::EXPONENT_BITS) - 1;
    const EXPONENT_BIAS: i16 = (F128::EXPONENT_BITS_MASK >> 1) as i16;
    const EXPONENT_MIN: i16 = 1 - F128::EXPONENT_BIAS;

    pub fn from_parts(sign: bool, exponent: u16, significand: u128) -> Self {
        F128 {
            sign: sign,
            exponent: exponent,
            significand: significand,
        }
    }

    pub fn new_from_u128(n: u128) -> Self {
        let sign = n & (1u128 << (F128::SIGNIFICAND_BITS
                                  + F128::EXPONENT_BITS)) != 0;
        let exponent = ((n >> F128::SIGNIFICAND_BITS) as u16)
                       & F128::EXPONENT_BITS_MASK;
        let significand = n & ((1u128 << F128::SIGNIFICAND_BITS) - 1);
        F128::from_parts(sign, exponent, significand)
    }

    pub fn new_from_f64(f: f64) -> Self {
        match f.classify() {
            FpCategory::Infinite => F128::from_parts(
                f.is_sign_negative(), F128::EXPONENT_BITS_MASK, 0),
            FpCategory::Nan => F128::from_parts(
                f.is_sign_negative(), F128::EXPONENT_BITS_MASK,
                0xFFFF_FE00_0000_0000_0000_0000_0000),
            FpCategory::Zero => F128::from_parts(f.is_sign_negative(), 0, 0),
            _ => {
                let sign = f.is_sign_negative();
                let exponent = f.abs().log2().floor();
                let significand = (f.abs() * (-exponent).exp2()).fract();
                let actual_exponent
                    = (exponent + (F128::EXPONENT_BIAS as f64)) as u16;
                let actual_significand
                    = (significand * (F128::SIGNIFICAND_BITS as f64).exp2())
                      as u128;
                assert!(actual_exponent & (!F128::EXPONENT_BITS_MASK) == 0);
                F128::from_parts(sign, actual_exponent, actual_significand)
            },
        }
    }

    #[inline]
    pub fn signnum(&self) -> f64 {
        if self.exponent == F128::EXPONENT_BITS_MASK && self.significand != 0 {
            f64::NAN
        } else if self.sign {
            -1.0
        } else {
            1.0
        }
    }

    #[inline]
    pub fn exponent(&self) -> f64 {
        match self.exponent {
            F128::EXPONENT_BITS_MASK => f64::NAN,
            0 => {
                if self.significand == 0 {
                    1.0
                } else {
                    (F128::EXPONENT_MIN as f64)
                    - ((self.significand.leading_zeros()
                        - (F128::LEADING_BITS as u32)) as f64)
                }
            },
            exponent => {
                ((exponent as i16) - F128::EXPONENT_BIAS).into()
            },
        }
    }

    #[inline]
    pub fn significandfrac(&self) -> f64 {
        (self.significand as f64) * (-(F128::SIGNIFICAND_BITS as f64)).exp2()
    }

    pub fn to_f64(&self) -> f64 {
        match self.exponent {
            0x0000 => {
                self.signnum()
                * self.exponent().exp2()
                * self.significandfrac()
            },
            F128::EXPONENT_BITS_MASK => {
                match self.significand {
                    0 => {
                        if self.sign {
                            f64::NEG_INFINITY
                        } else {
                            f64::INFINITY
                        }
                    },
                    _ => {
                        f64::NAN
                    },
                }
            },
            _ => {
                self.signnum()
                * self.exponent().exp2()
                * (1.0 + self.significandfrac())
            },
        }
    }
}

impl LVValue for F128 {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        Ok(F128::new_from_u128(p.read_lv()?))
    }
}

impl fmt::Display for F128 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let sf64 = self.to_f64();
        match sf64.classify() {
            FpCategory::Infinite
                => write!(f, "{}INFINITY",
                          if sf64.is_sign_negative() { "NEG_" } else { "" }),
            FpCategory::Nan => write!(f, "NAN"),
            _ => write!(f, "{:?}", sf64)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bignum_from_f64_display() {
        assert_eq!("1.25", format!("{}", F128::new_from_f64(1.25)));
        assert_eq!("0.25", format!("{}", F128::new_from_f64(0.25)));
        assert_eq!("0.0025", format!("{}", F128::new_from_f64(0.0025)));
        assert_eq!("105422.0", format!("{}", F128::new_from_f64(105422.0)));
        assert_eq!("NAN", format!("{}", F128::new_from_f64(f64::NAN)));
        assert_eq!("INFINITY",
                   format!("{}", F128::new_from_f64(f64::INFINITY)));
        assert_eq!("NEG_INFINITY",
                   format!("{}", F128::new_from_f64(f64::NEG_INFINITY)));
    }
}

// vim: tw=80
