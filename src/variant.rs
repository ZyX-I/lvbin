/*!
This module provides types and functions which deal with LabVIEW variants.

Each LabVIEW enum when dumped has the following structure:

1. First goes variant header.

   1. In variant header first goes 32-bit LabVIEW version. With LabVIEW 2015 and 
      2015 SP1 it always happens to be `0x1500_8000`.
   2. Then goes 32-bit number of type descriptions in variant header.
   3. Then for each type description you see

      1. Description length. Interesting fact is that description length of 
         element N is defined so that description length of element N+1 may be 
         found at the end of the description N, not just after it.

         If there is no next description description length always happens to be 
         set to one.
      2. Some single-byte flags bit field value. Currently the only encountered 
         known bit was `0x40` which determines whether (when set) or not (when 
         cleared) type description contains value label.
      3. Single-byte type field.
      4. Some data specific to the type. For many types this is nothing.
      5. Value label. May be not present.

   4. Root type index: index of the element which describes type actually stored 
      in the variant. All other elements should be directly or indirectly 
      referenced by the root element.
2. Then there is variant data. This part is type-specific and may only be parsed 
   after parsing variant header as there is no length data stored.
3. Unknown 32-bit constant which happens to be always zero.

 */
use std::io;
use std::io::Read;
use std::fmt;
use std::cmp;
use std::vec::Vec;
use std::vec;
use std::slice;
use std::boxed::Box;
use std::iter::IntoIterator;

use num_traits::FromPrimitive;
use num_complex::Complex;
use num_complex::Complex32;
use num_complex::Complex64;

use super::LVParse;
use super::LVValue;
use super::LVResult;
use super::LVError;
use super::timestamp::Timestamp;
use super::binstr::BinaryString;
use super::binstr::BinaryStrings;
use super::binstr::read_lv_string;
use super::fixedpoint::FixedPoint;
use super::fixedpoint::FixedPointOptions;
use super::mdimarray::MDimArray;
use super::path::Path;
use super::bigfloat::F128;
use super::class::Class;
use super::class::ClassName;
use super::waveform::Waveform;
use super::waveform::DigitalData;
use super::waveform::DigitalWaveform;

/// LabVIEW type number.
///
/// Maps type bytes that may be found in LabVIEW variant dumps to their meaning.
#[derive(Debug, Clone, Copy, Eq, PartialEq, FromPrimitive)]
pub enum VariantType {
    /// Variant with no data.
    Nothing = 0x00,
    /// Signed byte number.
    I8 = 0x01,
    /// Signed two-byte number.
    I16 = 0x02,
    /// Signed four-byte number.
    I32 = 0x03,
    /// Signed eight-byte number.
    I64 = 0x04,
    /// Unsigned byte number.
    U8 = 0x05,
    /// Unsigned two-byte number.
    U16 = 0x06,
    /// Unsigned four-byte number.
    U32 = 0x07,
    /// Unsigned eight-byte number.
    U64 = 0x08,
    /// Single-precision floating point number.
    F32 = 0x09,
    /// Double-precision floating point number.
    F64 = 0x0A,
    /// Quadruple-precision floating point number.
    F128 = 0x0B,
    /// Complex number cosisting of two single precision floating point numbers.
    Complex32 = 0x0C,
    /// Complex number cosisting of two double precision floating point numbers.
    Complex64 = 0x0D,
    /// Complex number cosisting of two quadruple precision floating point 
    /// numbers.
    Complex128 = 0x0E,
    /// U8 enum value.
    ///
    /// Only used for enums in LabVIEW 2018 (checked 2015sp1 and 2018).
    U8Enum = 0x15,
    /// U16 enum value.
    ///
    /// Only used for enums in LabVIEW 2018 (checked 2015sp1 and 2018).
    U16Enum = 0x16,
    /// U32 enum value.
    ///
    /// Only used for enums in LabVIEW 2018 (checked 2015sp1 and 2018).
    U32Enum = 0x17,
    /// Boolean value.
    Boolean = 0x21,
    /// Binary string (string in 8-bit locale encoding).
    String = 0x30,
    /// Path in 8-bit locale encoding.
    Path = 0x32,
    /// Unknown data which includes some variant.
    Unknown37 = 0x37,
    /// N-dimensional array.
    Array = 0x40,
    /// Cluster.
    Cluster = 0x50,
    /// Variant, used when variant gets wrapped in a variant.
    Variant = 0x53,
    /// Timestamped value.
    ///
    /// Like with Extended type there is a two-byte constant following which 
    /// defines actual type. So far only types known happen to contain timestamp 
    /// hence the name.
    Timestamped = 0x54,
    /// Fixed-point number.
    FixedPoint = 0x5F,
    /// Type which did not deserve a separate byte here.
    ///
    /// Variants with extended type have a two-byte constant following which 
    /// defines actual type.
    Extended = 0x70,
    /// Type definition.
    Typedef = 0xF1,
}

/// LabVIEW extended type number which represents [`VariantType::Extended`] 
/// subtypes.
///
/// Maps type constants (two bytes) that may be found in LabVIEW variant dumps 
/// with [`VariantType::Extended`] primary type byte to their meaning.
#[derive(Debug, Clone, Copy, Eq, PartialEq, FromPrimitive)]
pub enum VariantExtendedType {
    /// Invalid refnum.
    InvalidRefnum = 0x0000,
    /// Occurrence.
    Occurrence = 0x0004,
    /// Instrument descriptor.
    InstrumentDescriptor = 0x000F,
    /// Notifier, or, rather a pointer to it.
    Notifier = 0x0011,
    /// Queue, or, rather a pointer to it.
    Queue = 0x0012,
    /// DAQmx task.
    DAQmxTask = 0x0015,
    /// LabVIEW class.
    Class = 0x001E,
    /// User event, or, rather a pointer to it.
    UserEvent = 0x0019,
    /// Data value reference, or, rather a pointer to it.
    DataValueReference = 0x0020,
}

/// LabVIEW extended type number which represents [`VariantType::Timestamped`] 
/// subtypes.
///
/// Maps type constants (two bytes) that may be found in LabVIEW variant dumps 
/// with [`VariantType::Timestamped`] primary type byte to their meaning.
#[derive(Debug, Clone, Copy, Eq, PartialEq, FromPrimitive)]
pub enum VariantTimestampedType {
    /// Waveform constant with two-byte signed integer Y data.
    I16Waveform = 0x0002,
    /// Waveform constant with double precision Y data.
    F64Waveform = 0x0003,
    /// Waveform constant with single precision Y data.
    F32Waveform = 0x0005,
    /// Actual timestamp.
    Timestamp = 0x0006,
    /// Digital data.
    ///
    /// This one is actually not timestamped though.
    DigitalData = 0x0007,
    /// Digital waveform.
    DigitalWaveform = 0x0008,
    /// Waveform constant with quadruple precision Y data.
    F128Waveform = 0x000A,
    /// Waveform constant with single-byte unsigned integer Y data.
    U8Waveform = 0x000B,
    /// Waveform constant with two-byte unsigned integer Y data.
    U16Waveform = 0x000C,
    /// Waveform constant with four-byte unsigned integer Y data.
    U32Waveform = 0x000D,
    /// Waveform constant with single-byte signed integer Y data.
    I8Waveform = 0x000E,
    /// Waveform constant with four-byte signed integer Y data.
    I32Waveform = 0x000F,
    /// Waveform constant with single precision complex Y data.
    Complex32Waveform = 0x0010,
    /// Waveform constant with double precision complex Y data.
    Complex64Waveform = 0x0011,
    /// Waveform constant with quadruple precision complex Y data.
    Complex128Waveform = 0x0012,
    /// Waveform constant with eight-byte signed integer Y data.
    I64Waveform = 0x0013,
    /// Waveform constant with eight-byte unsigned integer Y data.
    U64Waveform = 0x0014,
}

/// LabVIEW variant type description.
///
/// This enum contains actual type description for typedefs, links to stored 
/// types for arrays and queues and just names for scalars.
#[derive(Clone, Eq, PartialEq)]
pub enum VariantTypeDescr {
    /// `Nothing` item is used for variants with no data *and* for <String, 
    /// Variant> attributes map. Given that *all* variants have trailing u32 
    /// number it is assumed that all variants may have attributes rather then 
    /// variant with no data also denotes attributes dictionary.
    ///
    /// You may get one by wrapping empty variant constant (default value for 
    /// variant, also obtainable by placing “variant constant” from the relevant 
    /// palette). It appears that those do not even happen to have a name.
    ///
    /// You may also get more complex one by adding attributes to a waveform. 
    /// Most likely, not only waveform. Those have no name too.
    // FIXME: Check whether attributes can really be attached to a regular 
    //        variant.
    Nothing,
    /// `Scalar` item is used for various simple values only have a name stored.
    ///
    /// This statement applies to all numbers, booleans and variants.
    Scalar {
        /// Value label, if any.
        label: BinaryString,
    },
    /// `Enum` item is used for enums (LabVIEW 2018).
    ///
    /// Enum variants store list of enum items.
    Enum {
        /// Value label, if any.
        label: BinaryString,
        /// Enum items.
        items: Vec<BinaryString>,
    },
    /// `Typedef` item is used for typedefs.
    ///
    /// It appears that typedefs basically store path to the typedef (i.e. 
    /// a vector with library/class names ending with a control name without any 
    /// actual filesystem paths) and actual type description. Note that if you 
    /// are seeking for a label you must look inside the inner [`type_header`].
    Typedef {
        unknown_at_start: u32,
        /// Name of typedef control. If control happens to be stored in a class 
        /// then vector will contain class name then control name.
        typedef_name: BinaryStrings,
        // FIXME: Check whether this unknown0 is (not) length of type_header
        unknown0: u16,
        /// Actual type data.
        type_header: Box<VariantTypeHeader>,
    },
    // FIXME: check whether it is possible to create queue with two root type 
    //        indexes.
    /// `Synchronization` item is used for synchronization primitives like 
    /// queues and notifiers.
    ///
    /// It appears that queues and notifiers store some unknown two-byte 
    /// constant (which always appears to be 1 so it was speculatively parsed as 
    /// [`root_type_idxes`] array header), index of the type in the 
    /// queue/notifier (so far [`root_type_idxes`] only ever contained one 
    /// element) and usual value label.
    Synchronization {
        /// Actual type of the primitive.
        subtype: VariantExtendedType,
        /// Index of the root element.
        ///
        /// Was speculatively joined with the preceding constant 1 into an array 
        /// of indexes.
        root_type_idxes: Vec<u16>,
        /// Value label, if any.
        label: BinaryString,
    },
    /// `Class` item is used for LabVIEW class constants.
    ///
    /// Note that while classes are essentially clusters (which happen to 
    /// implicitly contain parents’ clusters), LabVIEW does not expose types of 
    /// data which may be found in these clusters.
    Class {
        /// Unknown constant which happens to be always zero.
        ///
        /// It is known that when `unknown0 & 0x0001` is `1` then this constant 
        /// will be dumped back (by `unparse_variant.vi`) as `0x0001`, otherwise 
        /// it will be dumped back as `0x0000`. How to generate `0x0001` without 
        /// messing with variant dump or what exactly does it mean is not known.
        unknown0: u16,
        /// Class name: vector, containing fully qualified class name.
        ///
        /// That is, it contains class name, owning library name, library owning 
        /// owning library name, etc. Order is reversed comparing to the one 
        /// from previous sentence: you get root name first just like when 
        /// writing filesystem path.
        class_name: ClassName,
        /// Value label, if any.
        label: BinaryString,
    },
    /// `Array` item is used for LabVIEW arrays.
    Array {
        /// Number of dimensions.
        num_dimensions: usize,
        /// Array of values of size [`num_dimensions`] which defines size bounds 
        /// for fixed size arrays.
        ///
        /// Normally all values in this array are all `0xFFFF_FFFF`’s. If *all* 
        /// values in this arry are *not* `0xFFFF_FFFF`’s then array is 
        /// considered as “bounded-size array” (term appears in variant 
        /// indicator).
        ///
        /// When dumping such arrays back in LabVIEW there is still dimension 
        /// size header (but bounds data is preserved even if one of the bounds 
        /// is `0xFFFF_FFFF`).
        ///
        /// When reading such array in LabVIEW all dimension sizes are clipped 
        /// to (note: clipped to, not set to) the specified size. Excess 
        /// elements are still read though. Also based on the error message when 
        /// I tried using `0xFFFF_FFFE` as size (LabVIEW complained about 
        /// insufficient memory) it tries to allocate memory for maximum number 
        /// of elements.
        size_bounds: Vec<u32>,
        /// Index of array element type.
        root_type_idx: u16,
        /// Value label, if any.
        label: BinaryString,
    },
    /// `FixedPoint` item is used for LabVIEW fixed-point numbers.
    FixedPoint {
        // FIXME: check what happens if these bits are altered.
        /// Unknown value which always happens to be 0x0311 for unsigned and 
        /// 0x0351 for signed numbers. This looks like a bit field, but what are 
        /// bits other then 0x0040 for is unknown.
        unknown0: u16,
        /// Options: how many bits are used for the whole number, for the 
        /// integer part of it and for the sign.
        ///
        /// For some reason all of this information is repeated two or three 
        /// times.
        options: FixedPointOptions,
        /// Bit mask for sign part of the number.
        ///
        /// Normally contains either all zeroes (for unsigned numbers) or ones 
        /// up to the point where sign bit actually is located.
        sign_bit_mask: u64,
        /// Bit mask for non-sign part of number.
        ///
        /// Contains zeroes up to the point where first significant bit is in 
        /// actual number. At and after the point it contains ones.
        bit_mask: u64,
        /// Unknown 64-bit value, most likely containing another kind of bit 
        /// mask.
        unknown1: u64,
        /// Unknown 64-bit value which happens to always be zero.
        unknown2: u64,
        /// Value label, if any.
        label: BinaryString,
    },
    /// `Cluster` item is used for LabVIEW clusters.
    ///
    /// This item basically contains an array referencing elements in cluster in 
    /// order and cluster name (if any).
    Cluster {
        /// Cluster elements’ indexes, in order.
        root_type_idxes: Vec<u16>,
        /// Value label.
        label: BinaryString,
    },
    /// `String` item is used for actual strings and also paths.
    String {
        /// String size bound.
        ///
        /// Normally this is `0xFFFF_FFFF` meaning “unbounded”. Much like for 
        /// [`Array`] if size is limited then string read by LabVIEW appears as 
        /// “bounded-size string” and is clipped to the given size should it 
        /// happen to be larger.
        ///
        /// Size is not *set* though too, and neither size data disappears from 
        /// (when dumbing back from LabVIEW) or is ignored in (when LabVIEW 
        /// reads variant back) the variant: if you have `0x0000_00FF` in the 
        /// relevant position but only three bytes in string then you get string 
        /// with three bytes and `0xFF` size bound. If you have `0x0000_0001` in 
        /// the relevant position and 3-byte string (e.g. 
        /// `b"\x00\x00\x00\x03abc"`) then you will have 1-byte string after 
        /// parsing the variant and `b"\x00\x00\x00\x01a"` in the output.
        ///
        /// All zeroes in size bound looks like some sort of special occasion 
        /// because it hangs LabVIEW (2015 SP1) for some minutes (making 
        /// aborting fail to work) and then produces error 116 “LabVIEW: 
        /// Unflatten or byte stream read operation failed due to corrupt, 
        /// unexpected, or truncated data.”
        ///
        /// Though again if you set size bound to `0xFFFF_FFFE` you get
        size_bound: u32,
        /// Value label, if any.
        label: BinaryString,
    },
    /// `Timestamp` item is used for LabVIEW values with timestamps.
    Timestamped {
        /// Subtype.
        subtype: VariantTimestampedType,
        /// Value label, if any.
        label: BinaryString,
    },
    /// `Unknown37` item is some type used by instrument descriptor and DAQmx 
    /// types.
    ///
    /// The only two occurrences seen so far contain two unknown constants 
    /// (first is always 0xFFFF_FFFF, second is 0x0003 for instrument descriptor 
    /// and 0x0009 for task) and some variant (with no data).
    Unknown37 {
        unknown0: u32,
        unknown1: u16,
        unknown2: Box<Variant>,
    },
    /// `InstrumentDescriptor` item is used for instrument descriptors.
    ///
    /// The only occurrence seen so far contains reference to 
    /// [`VariantTypeDescr::Unknown37`] type in an array of u16 integers with 
    /// u16 header, some unknown u16 constant (zero) and unknown variant.
    InstrumentDescriptor {
        ivi_class: BinaryString,
        root_type_idxes: Vec<u16>,
        unknown0: u16,
        unknown1: Box<Variant>,
        label: BinaryString,
    },
    // FIXME: Check whether reference to Unknown37 type is actually where it was 
    //        “found”. Do that by putting task inside a cluster as non-first 
    //        entry.
    /// `DAQmxTask` item is used for DAQmx tasks.
    ///
    /// The only occurrence seen so far contains reference to 
    /// [`VariantTypeDescr::Unknown37`] type (uncertain, just assumed that two 
    /// zero bytes in certain position stand for the reference).
    DAQmxTask {
        type_name: BinaryString,
        root_type_idx: u16,
        unknown0: u16,
        class_name: BinaryString,
        unknown1: Box<Variant>,
        label: BinaryString,
    },
    /// `PrimitiveRefnum` item is used for refnums which only have label as 
    /// a data.
    ///
    /// This is known to be the case for [`VariantExtendedType::Occurrence`] and 
    /// [`VariantExtendedType::InvalidRefnum`].
    PrimitiveRefnum {
        subtype: VariantExtendedType,
        label: BinaryString,
    },
}

pub type Complex128 = Complex<F128>;

/// Structure used for containing attributes.
///
/// Attributes is basically a <BinaryString, Variant> map.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Attributes(pub Vec<(BinaryString, Variant)>);

/// LabVIEW data that may be stored in a variant.
#[derive(Clone)]
pub enum VariantData {
    /// Empty variant contains nothing.
    Nothing,
    /// I8 number.
    I8(i8),
    /// I16 number.
    I16(i16),
    /// I32 number.
    I32(i32),
    /// I64 number.
    I64(i64),
    /// U8 number.
    U8(u8),
    /// U16 number.
    U16(u16),
    /// U32 number.
    U32(u32),
    /// U64 number.
    U64(u64),
    /// F32 number.
    F32(f32),
    /// F64 number.
    F64(f64),
    /// F128 number.
    F128(F128),
    /// Complex32 number.
    Complex32(Complex32),
    /// Complex64 number.
    Complex64(Complex64),
    /// Complex128 number.
    Complex128(Complex128),
    /// Boolean data.
    Boolean(bool),
    /// Binary string.
    String(BinaryString),
    /// Path.
    Path(Path),
    /// 1D array of any data. Inside array all values should have the same type. 
    /// Array may be empty.
    Array1D(Vec<VariantData>),
    /// Multidimensional array of any data. Inside array all values should have 
    /// the same type. Array may be empty.
    ArrayMDim(MDimArray<VariantData>),
    /// Cluster of any data. Cluster may store values of different types, but 
    /// there may not be a cluster with no values.
    Cluster(Vec<VariantData>),
    /// Variant may store another variant inside.
    Variant(Box<Variant>),
    /// Waveform with two-byte signed values in Y array.
    I16Waveform(Box<Waveform<i16>>),
    /// Waveform with double precision floating point values in Y array.
    F64Waveform(Box<Waveform<f64>>),
    /// Waveform with single precision floating point values in Y array.
    F32Waveform(Box<Waveform<f32>>),
    /// Timestamp.
    Timestamp(Timestamp),
    /// Digital data.
    DigitalData(DigitalData),
    /// Digital waveform.
    DigitalWaveform(Box<DigitalWaveform>),
    /// Waveform with quadruple precision floating point values in Y array.
    F128Waveform(Box<Waveform<F128>>),
    /// Waveform with single byte unsigned values in Y array.
    U8Waveform(Box<Waveform<u8>>),
    /// Waveform with two-byte unsigned values in Y array.
    U16Waveform(Box<Waveform<u16>>),
    /// Waveform with four-byte unsigned values in Y array.
    U32Waveform(Box<Waveform<u32>>),
    /// Waveform with single byte signed values in Y array.
    I8Waveform(Box<Waveform<i8>>),
    /// Waveform with four-byte signed values in Y array.
    I32Waveform(Box<Waveform<i32>>),
    /// Waveform with single precision floating point complex values in Y array.
    Complex32Waveform(Box<Waveform<Complex32>>),
    /// Waveform with double precision floating point complex values in Y array.
    Complex64Waveform(Box<Waveform<Complex64>>),
    /// Waveform with quadruple precision floating point complex values in 
    /// Y array.
    Complex128Waveform(Box<Waveform<Complex128>>),
    /// Waveform with eight-byte signed values in Y array.
    I64Waveform(Box<Waveform<i64>>),
    /// Waveform with eight-byte unsigned values in Y array.
    U64Waveform(Box<Waveform<u64>>),
    /// Fixed point number.
    FixedPoint(FixedPoint),
    /// LabVIEW class.
    ///
    /// Note that while classes are essentially clusters (which happen to 
    /// implicitly contain parents’ clusters), LabVIEW does not expose types of 
    /// data which may be found in these clusters. So all you get here is class 
    /// name, class version and some binary blob.
    Class(Class),
}

/// LabVIEW type description.
#[derive(Clone, Eq, PartialEq)]
pub struct VariantTypeHeader {
    /// Some flags.
    ///
    /// Currently it is only known to either be 0x40 or zero with 0x40 
    /// indicating whether there is (no) name.
    pub flags: u8,
    /// Element type.
    pub vtype: VariantType,
    /// Additional type description.
    pub descr: VariantTypeDescr,
}

/// Element inside LabVIEW variant header.
#[derive(Clone, Eq, PartialEq)]
pub struct VariantTypeElement {
    /// Length of this element.
    pub descr_length: usize,
    /// Actual type description.
    pub header: VariantTypeHeader,
    /// Length of next element.
    pub next_descr_length: usize,
}

/// LabVIEW variant.
#[derive(Clone, Eq, PartialEq)]
pub struct Variant {
    pub version: u32,
    pub types: Vec<VariantTypeElement>,
    pub data: VariantData,
    pub root_type_idx: u16,
    pub attributes: Attributes,
}

/// Constant used as a size bound when no bound is present.
pub const UNBOUND_SIZE: u32 = 0xFFFF_FFFF;

trait IOHelpers : io::Read + LVParse {
    fn read_rest(&mut self) -> LVResult<Vec<u8>> {
        let mut rest = Vec::new();
        self.read_to_end(&mut rest)?;
        Ok(rest)
    }

    fn read_rest_as_usize(&mut self) -> LVResult<usize>
        where Self: Sized
    {
        let mut ret = 0usize;
        for (i, byte) in self.bytes().enumerate() {
            let byte = byte?;
            if i > 3 {
                Err(LVError::ParseError(format!(
                            "Unexpected byte {:02x} while reading \
                             trailing size bytes: there should be no more then \
                             three bytes",
                            byte)))?
            }
            ret = (ret << 8) | (byte as usize);
        }
        Ok(ret)
    }

    #[inline]
    fn read_string_u8(&mut self) -> LVResult<BinaryString> {
        read_lv_string::<u8, Self>(self)
    }

    #[inline]
    fn read_string_u16(&mut self) -> LVResult<BinaryString> {
        read_lv_string::<u16, Self>(self)
    }

    fn read_label(&mut self, flags: u8, has_filler: bool)
        -> LVResult<BinaryString>
    {
        if flags & 0x40 == 0 {
            Ok(BinaryString(vec![]))
        } else if has_filler {
            match self.read_byte() {
                Err(e) => Err(e),
                Ok(0x00) => self.read_string_u8(),
                Ok(n) => {
                    Err(LVError::ParseError(
                        format!("Expected filler byte 0x00, got 0x{:02x}", n)))
                },
            }
        } else {
            self.read_string_u8()
        }
    }
}

fn read_data_by_vtype<T>(p: &mut T, vtype: VariantType) -> LVResult<VariantData>
    where T: LVParse + ?Sized
{
    Ok(match vtype {
        VariantType::I8 => VariantData::I8(p.read_lv()?),
        VariantType::I16 => VariantData::I16(p.read_lv()?),
        VariantType::I32 => VariantData::I32(p.read_lv()?),
        VariantType::I64 => VariantData::I64(p.read_lv()?),
        VariantType::U8 => VariantData::U8(p.read_lv()?),
        VariantType::U8Enum => VariantData::U8(p.read_lv()?),
        VariantType::U16 => VariantData::U16(p.read_lv()?),
        VariantType::U16Enum => VariantData::U16(p.read_lv()?),
        VariantType::U32 => VariantData::U32(p.read_lv()?),
        VariantType::U32Enum => VariantData::U32(p.read_lv()?),
        VariantType::U64 => VariantData::U64(p.read_lv()?),
        VariantType::F32 => VariantData::F32(p.read_lv()?),
        VariantType::F64 => VariantData::F64(p.read_lv()?),
        VariantType::F128 => VariantData::F128(p.read_lv()?),
        VariantType::Complex32 => VariantData::Complex32(p.read_lv()?),
        VariantType::Complex64 => VariantData::Complex64(p.read_lv()?),
        VariantType::Complex128 => VariantData::Complex128(p.read_lv()?),
        VariantType::Boolean => VariantData::Boolean(p.read_lv()?),
        VariantType::String => VariantData::String(p.read_lv()?),
        VariantType::Path => VariantData::Path(p.read_lv()?),
        VariantType::Variant => VariantData::Variant(
            Box::new(p.read_lv()?)),
        vtype => {
            Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Unexpected type: {:?}", vtype)))?
        },
    })
}

fn read_data_by_type<T>(p: &mut T, types: &Vec<VariantTypeElement>,
                        root_type_idx: u16)
    -> LVResult<VariantData>
    where T: LVParse + ?Sized
{
    if root_type_idx as usize >= types.len() {
        Err(LVError::ParseError(
                format!("Root type description index is too big: {} >= {}",
                        root_type_idx, types.len())))?
    }
    let root_type = &types[root_type_idx as usize];
    let header = root_type.header.header();
    match header.descr {
        VariantTypeDescr::Nothing => {
            Ok(VariantData::Nothing)
        },
        VariantTypeDescr::Scalar { .. }
        | VariantTypeDescr::String { .. }
        | VariantTypeDescr::Enum { .. } => {
            read_data_by_vtype(p, root_type.header.vtype())
        },
        VariantTypeDescr::Timestamped { subtype, .. } => {
            macro_rules! match_subtype {
                ($p:expr, $subtype:expr => { $($type:ident,)* }) => (
                    match $subtype {
                        $(VariantTimestampedType::$type
                          => VariantData::$type($p.read_lv()?),)*
                    }
                );
            }
            Ok(match_subtype!(p, subtype => {
                I16Waveform,
                F64Waveform,
                F32Waveform,
                Timestamp,
                DigitalData,
                DigitalWaveform,
                F128Waveform,
                U8Waveform,
                U16Waveform,
                U32Waveform,
                I8Waveform,
                I32Waveform,
                Complex32Waveform,
                Complex64Waveform,
                Complex128Waveform,
                I64Waveform,
                U64Waveform,
            }))
        },
        VariantTypeDescr::Synchronization { .. }
        | VariantTypeDescr::PrimitiveRefnum { .. } => {
            Ok(VariantData::U32(p.read_lv()?))
        },
        VariantTypeDescr::InstrumentDescriptor { .. }
        | VariantTypeDescr::DAQmxTask { .. } => {
            Ok(VariantData::String(p.read_lv()?))
        },
        VariantTypeDescr::Class { .. } => {
            Ok(VariantData::Class(p.read_lv()?))
        },
        VariantTypeDescr::FixedPoint { ref options, .. } => {
            let data = p.read_lv()?;
            Ok(VariantData::FixedPoint(FixedPoint {
                data: data,
                options: options.clone(),
            }))
        },
        VariantTypeDescr::Cluster { ref root_type_idxes, .. } => {
            let mut data = vec![];
            for idx in root_type_idxes {
                data.push(read_data_by_type(p, types, *idx)?);
            }
            Ok(VariantData::Cluster(data))
        },
        VariantTypeDescr::Array { num_dimensions, root_type_idx: idx, .. } => {
            let mut data = vec![];
            if num_dimensions == 1 {
                let data_size: u32 = p.read_lv()?;
                for _ in 0 .. data_size {
                    data.push(read_data_by_type(p, types, idx)?);
                }
                Ok(VariantData::Array1D(data))
            } else {
                let mut sizes = vec![];
                let mut num_elements: usize = 1;
                for _ in 0 .. num_dimensions {
                    let size = p.read_lv::<u32>()? as usize;
                    sizes.push(size);
                    num_elements = num_elements.checked_mul(size).ok_or(
                        LVError::ParseError(format!(
                                "Size {} * {} is too big to fit into usize",
                                size, num_elements)))?;
                }
                for _ in 0 .. num_elements {
                    data.push(read_data_by_type(p, types, idx)?);
                }
                Ok(VariantData::ArrayMDim(MDimArray {
                    sizes: sizes,
                    data: data,
                }))
            }
        },
        VariantTypeDescr::Unknown37 { .. } => {
            Err(LVError::ParseError("Parsing Unknown37 data is not supported"
                                    .to_string()))
        },
        VariantTypeDescr::Typedef { .. } => unreachable!(),
    }
}

impl<T: io::Read> IOHelpers for T {}

#[cfg(any(test, feature = "_dbg"))]
macro_rules! dbg {
    ($($arg:tt)*) => ({eprintln!($($arg)*);})
}
#[cfg(not(any(test, feature = "_dbg")))]
macro_rules! dbg {
    ($($arg:tt)*) => ({})
}

fn read_type_header<T>(p: &mut T) -> LVResult<VariantTypeHeader>
    where T: IOHelpers + LVParse + ?Sized
{
    dbg!(">>> New entry");
    let flags: u8 = p.read_lv()?;
    let vtype = p.read_lv()?;
    dbg!("  vtype {:?}", vtype);
    let descr = match vtype {
        VariantType::I8
        | VariantType::I16
        | VariantType::I32
        | VariantType::I64
        | VariantType::U8
        | VariantType::U16
        | VariantType::U32
        | VariantType::U64
        | VariantType::F32
        | VariantType::F64
        | VariantType::F128
        | VariantType::Complex32
        | VariantType::Complex64
        | VariantType::Complex128 => {
            VariantTypeDescr::Scalar {
                label: p.read_label(flags, true)?,
            }
        },
        VariantType::U8Enum
        | VariantType::U16Enum
        | VariantType::U32Enum => {
            let items_len: u16 = p.read_lv()?;
            dbg!("    Getting {} enum items", items_len);
            let mut items = Vec::with_capacity(items_len as usize);
            for _ in 0 .. items_len {
                items.push(p.read_string_u8()?);
            }
            let label = p.read_label(flags, true)?;
            VariantTypeDescr::Enum {
                label: label,
                items: items,
            }
        },
        VariantType::Boolean
        | VariantType::Variant => {
            VariantTypeDescr::Scalar {
                label: p.read_label(flags, false)?,
            }
        },
        VariantType::String
        | VariantType::Path => {
            let size_bound = p.read_lv()?;
            let label = p.read_label(flags, false)?;
            VariantTypeDescr::String {
                size_bound: size_bound,
                label: label,
            }
        },
        VariantType::Timestamped => {
            let subtype = p.read_lv()?;
            let label = p.read_label(flags, false)?;
            VariantTypeDescr::Timestamped {
                subtype: subtype,
                label: label,
            }
        },
        VariantType::FixedPoint => {
            let unknown0 = p.read_lv()?;
            let total_bits: u16 = p.read_lv()?;
            let int_bits: u32 = p.read_lv()?;
            let is_signed: u16 = p.read_lv()?;
            if is_signed > 1 {
                Err(LVError::ParseError(
                        format!("Unable to determine whether number \
                                 is signed, {:04x} > 1",
                                is_signed)))?;
            }
            if total_bits > 64 {
                Err(LVError::ParseError(
                        format!("Too many total bits: 0x{:04x} > 64",
                                total_bits)))?;
            }
            if int_bits > total_bits as u32 {
                Err(LVError::ParseError(
                        format!(
                            "Too many int bits: 0x{:08x} > 0x{:02x}",
                            int_bits, total_bits)))?;
            }
            let total_bits2: u16 = p.read_lv()?;
            if total_bits2 != total_bits {
                Err(LVError::ParseError(
                        format!(
                            "Expected duplicate total bits (0x{:02x}), \
                             got 0x{:04x}",
                            total_bits, total_bits2)))?;
            }
            let int_bits2: u32 = p.read_lv()?;
            if int_bits2 != int_bits {
                Err(LVError::ParseError(
                        format!(
                            "Expected duplicate int bits (0x{:02x}), \
                             got 0x{:08x}",
                            int_bits, int_bits2)))?;
            }
            let sign_bit_mask = p.read_lv()?;
            let is_signed2: u16 = p.read_lv()?;
            if is_signed2 != is_signed {
                Err(LVError::ParseError(
                        format!(
                            "Expected duplicate signed flag ({}), \
                             got 0x{:04x}",
                            is_signed, is_signed2)))?;
            }
            let total_bits3: u16 = p.read_lv()?;
            if total_bits3 != total_bits {
                Err(LVError::ParseError(
                        format!(
                            "Expected triplicate total bits \
                             (0x{:02x}), got 0x{:04x}",
                            total_bits, total_bits3)))?;
            }
            let int_bits3: u32 = p.read_lv()?;
            if int_bits3 != int_bits {
                Err(LVError::ParseError(
                        format!(
                            "Expected triplicate int bits (0x{:02x}), \
                             got 0x{:08x}",
                            int_bits, int_bits3)))?;
            }
            let bit_mask = p.read_lv()?;
            let unknown1 = p.read_lv()?;
            let unknown2 = p.read_lv()?;
            let label = p.read_label(flags, false)?;
            VariantTypeDescr::FixedPoint {
                unknown0: unknown0,
                options: FixedPointOptions {
                    signed: is_signed != 0,
                    total_bits: total_bits as u8,
                    int_bits: int_bits as u8,
                },
                sign_bit_mask: sign_bit_mask,
                bit_mask: bit_mask,
                unknown1: unknown1,
                unknown2: unknown2,
                label: label,
            }
        },
        VariantType::Nothing => {
            VariantTypeDescr::Nothing
        },
        VariantType::Array => {
            let size_bounds = p.read_lv_1d_array_sized::<u16, u32>()?;
            let num_dimensions = size_bounds.len();
            let root_type_idx = p.read_lv()?;
            let label = p.read_label(flags, false)?;
            VariantTypeDescr::Array {
                num_dimensions: num_dimensions,
                size_bounds: size_bounds,
                label: label,
                root_type_idx: root_type_idx,
            }
        },
        VariantType::Unknown37 => {
            let unknown0 = p.read_lv()?;
            let unknown1 = p.read_lv()?;
            let unknown2 = p.read_lv()?;
            VariantTypeDescr::Unknown37 {
                unknown0: unknown0,
                unknown1: unknown1,
                unknown2: Box::new(unknown2),
            }
        },
        VariantType::Typedef => {
            let unknown_at_start = p.read_lv()?;
            let names_len: u32 = p.read_lv()?;
            let mut typedef_name = Vec::with_capacity(
                names_len as usize);
            for _ in 0 .. names_len {
                typedef_name.push(p.read_string_u8()?);
            }
            dbg!("    typedef_name: {:?}", typedef_name);
            let unknown0 = p.read_lv()?;
            dbg!("    u0: {:04x}", unknown0);
            let type_header = read_type_header(p)?;
            dbg!("    t:{:?}", type_header);
            VariantTypeDescr::Typedef {
                unknown_at_start: unknown_at_start,
                typedef_name: BinaryStrings(typedef_name),
                unknown0: unknown0,
                type_header: Box::new(type_header),
            }
        },
        VariantType::Extended => {
            match p.read_lv() {
                Err(e) => Err(e)?,
                Ok(subtype @ VariantExtendedType::Occurrence)
                | Ok(subtype @ VariantExtendedType::InvalidRefnum) => {
                    let label = p.read_label(flags, false)?;
                    VariantTypeDescr::PrimitiveRefnum {
                        subtype: subtype,
                        label: label,
                    }
                },
                Ok(subtype @ VariantExtendedType::Queue)
                | Ok(subtype @ VariantExtendedType::Notifier)
                | Ok(subtype @ VariantExtendedType::DataValueReference)
                | Ok(subtype @ VariantExtendedType::UserEvent) => {
                    let root_type_idxes
                        = p.read_lv_1d_array_sized::<u16, u16>()?;
                    // FIXME: Check whether filler byte may appear with other 
                    //        references.
                    let label = p.read_label(
                        flags,
                        subtype == VariantExtendedType::DataValueReference)?;
                    VariantTypeDescr::Synchronization {
                        subtype: subtype,
                        root_type_idxes: root_type_idxes,
                        label: label,
                    }
                },
                Ok(VariantExtendedType::InstrumentDescriptor) => {
                    let ivi_class = p.read_string_u8()?;
                    let root_type_idxes
                        = p.read_lv_1d_array_sized::<u16, u16>()?;
                    let unknown0 = p.read_lv()?;
                    let unknown1 = p.read_lv()?;
                    let label = p.read_label(flags, false)?;
                    VariantTypeDescr::InstrumentDescriptor {
                        ivi_class: ivi_class,
                        root_type_idxes: root_type_idxes,
                        unknown0: unknown0,
                        unknown1: Box::new(unknown1),
                        label: label,
                    }
                },
                Ok(VariantExtendedType::DAQmxTask) => {
                    let type_name = p.read_string_u8()?;
                    let root_type_idx = p.read_lv()?;
                    let unknown0 = p.read_lv()?;
                    let class_name = p.read_string_u16()?;
                    let unknown1 = p.read_lv()?;
                    let label = p.read_label(flags, false)?;
                    VariantTypeDescr::DAQmxTask {
                        type_name: type_name,
                        root_type_idx: root_type_idx,
                        unknown0: unknown0,
                        class_name: class_name,
                        unknown1: Box::new(unknown1),
                        label: label,
                    }
                },
                Ok(VariantExtendedType::Class) => {
                    let unknown0 = p.read_lv()?;
                    dbg!("    Class u0: 0x{:04x}", unknown0);
                    let class_name: ClassName = p.read_lv()?;
                    dbg!("    Class name: {:?}", class_name);
                    let label = p.read_label(flags, !class_name.is_array())?;
                    dbg!("    Label: {}", label);
                    VariantTypeDescr::Class {
                        unknown0: unknown0,
                        class_name: class_name,
                        label: label,
                    }
                },
            }
        },
        VariantType::Cluster => {
            let number_of_elements: u16 = p.read_lv()?;
            let mut root_type_idxes
                = Vec::with_capacity(number_of_elements as usize);
            for _ in 0 .. number_of_elements {
                root_type_idxes.push(p.read_lv()?);
            }
            let label = p.read_label(flags, false)?;
            VariantTypeDescr::Cluster {
                root_type_idxes: root_type_idxes,
                label: label,
            }
        },
    };
    Ok(VariantTypeHeader {
        flags: flags,
        vtype: vtype,
        descr: descr,
    })
}

impl LVValue for Variant {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        let version: u32 = p.read_lv()?;
        if version != 0x1500_8000 && version != 0x1800_8000 {
            Err(LVError::ParseError(format!("Unknown version number: 0x{:08x}",
                                            version)))?;
        }
        let num_types: u32 = p.read_lv()?;
        let mut descr_length: usize = p.read_lv::<u16>()?.into();
        let mut types = Vec::with_capacity(num_types as usize);

        for _ in 0 .. num_types {
            let mut whole_entry = vec![0; descr_length];
            p.read_bytes(whole_entry.as_mut_slice())?;
            let mut header_bytes = io::Cursor::new(whole_entry);
            let header = read_type_header(&mut header_bytes)?;
            let next_descr_length = header_bytes.read_rest_as_usize()?;
            let type_element = VariantTypeElement {
                descr_length: descr_length,
                header: header,
                next_descr_length: next_descr_length,
            };
            dbg!("  Got entry {:?}", type_element);
            types.push(type_element);
            descr_length = next_descr_length;
        }

        let root_type_idx = p.read_lv()?;

        let data = read_data_by_type(p, &types, root_type_idx)?;
        dbg!("  Read data {:?}", data);

        let attributes = p.read_lv()?;
        dbg!("  Read attributes {:?}", attributes);

        Ok(Variant {
            version: version,
            types: types,
            data: data,
            root_type_idx: root_type_idx,
            attributes: attributes,
        })
    }
}

impl LVValue for Attributes {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        let len: u32 = p.read_lv()?;
        let mut attributes = vec![];
        for _ in 0 .. len {
            let key = p.read_lv_1d_array()?;
            let val = p.read_lv()?;
            attributes.push((BinaryString(key), val));
        }
        Ok(Attributes(attributes))
    }
}

/// Floating-point equals implementation that compares NaN equal.
fn float_eq<T: PartialEq>(a: T, b: T) -> bool {
    if a == a {
        a == b
    } else {
        !(b == b)
    }
}

/// Complex floating point equals implementation that compares NaN equal.
fn complex_eq<T: PartialEq>(a: Complex<T>, b: Complex<T>) -> bool {
    let Complex { re: are, im: aim } = a;
    let Complex { re: bre, im: bim } = b;
    float_eq(are, bre) && float_eq(aim, bim)
}

impl cmp::PartialEq for VariantData {
    fn eq(&self, other: &Self) -> bool {
        macro_rules! check {
            ($self:ident, $other:ident {
                $($v0:ident,)*
            } {
                $($v1:ident,)*
            } {
                $($vx:ident => $cmp:expr,)*
            }) => {{
                match $self {
                    $(
                        VariantData::$v0 => if let VariantData::$v0 = $other {
                            true
                        } else {
                            false
                        },
                    )*
                    $(
                        VariantData::$v1(sdata) => {
                            if let VariantData::$v1(odata) = $other {
                                sdata == odata
                            } else {
                                false
                            }
                        },
                    )*
                    $(
                        VariantData::$vx(sdata) => {
                            if let VariantData::$vx(odata) = $other {
                                $cmp(sdata.clone(), odata.clone())
                            } else {
                                false
                            }
                        },
                    )*
                }
            }}
        }
        check!(self, other {
            Nothing,
        } {
            I8,
            I16,
            I32,
            I64,
            U8,
            U16,
            U32,
            U64,
            Boolean,
            String,
            Path,
            Array1D,
            ArrayMDim,
            Cluster,
            Variant,
            Timestamp,
            FixedPoint,
            Class,
            DigitalData,
            // TODO: Make NaNs also equal when comparing F* waveforms
            I16Waveform,
            F64Waveform,
            F32Waveform,
            F128Waveform,
            U8Waveform,
            U16Waveform,
            U32Waveform,
            I8Waveform,
            I32Waveform,
            Complex32Waveform,
            Complex64Waveform,
            Complex128Waveform,
            I64Waveform,
            U64Waveform,
            DigitalWaveform,
        } {
            F32 => float_eq,
            F64 => float_eq,
            F128 => float_eq,
            Complex32 => complex_eq,
            Complex64 => complex_eq,
            Complex128 => complex_eq,
        })
    }
}

impl cmp::Eq for VariantData {}

impl LVValue for VariantType {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        p.read_byte().and_then(|n| {
            VariantType::from_u8(n).ok_or_else(
                || LVError::ParseError(format!("Unknown type byte: {:02x}", n)))
        })
    }
}

impl LVValue for VariantExtendedType {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        p.read_lv_u16().and_then(|n| {
            VariantExtendedType::from_u16(n).ok_or_else(
                || LVError::ParseError(format!(
                        "Unknown extended type number: {:04x}", n)))
        })
    }
}

impl LVValue for VariantTimestampedType {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        p.read_lv_u16().and_then(|n| {
            VariantTimestampedType::from_u16(n).ok_or_else(
                || LVError::ParseError(format!(
                        "Unknown timestamped type number: {:04x}", n)))
        })
    }
}

impl VariantTypeHeader {
    fn vtype(&self) -> VariantType {
        match self {
            VariantTypeHeader {
                descr: VariantTypeDescr::Typedef {
                    type_header,
                    ..
                },
                ..
            } => type_header.vtype(),
            VariantTypeHeader { vtype, .. } => *vtype,
        }
    }

    fn header<'a>(&'a self) -> &'a VariantTypeHeader {
        match self {
            VariantTypeHeader {
                descr: VariantTypeDescr::Typedef { type_header, .. },
                ..
            } => type_header.header(),
            header => header,
        }
    }
}

impl Attributes {
    pub fn len(&self) -> usize {
        self.0.len()
    }
}

impl IntoIterator for Attributes {
    type Item = (BinaryString, Variant);
    type IntoIter = vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<'a> IntoIterator for &'a Attributes {
    type Item = &'a (BinaryString, Variant);
    type IntoIter = slice::Iter<'a, (BinaryString, Variant)>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl<'a> IntoIterator for &'a mut Attributes {
    type Item = &'a mut (BinaryString, Variant);
    type IntoIter = slice::IterMut<'a, (BinaryString, Variant)>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter_mut()
    }
}

impl fmt::Debug for VariantTypeHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "VariantTypeHeader {{ \
                       flags: 0x{:02x}, \
                       vtype: VariantType::{:?}, \
                       descr: {:?} }}",
               self.flags, self.vtype, self.descr)
    }
}

impl fmt::Debug for Variant {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Variant {{ \
                       version: 0x{:08x}, \
                       types: vec!{:?}, \
                       data: {:?}, \
                       root_type_idx: 0x{:04x}, \
                       attributes: {:?} }}",
               self.version, self.types, self.data, self.root_type_idx,
               self.attributes)
    }
}

impl fmt::Debug for VariantTypeElement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "VariantTypeElement {{ \
                       descr_length: 0x{:02x}, \
                       header: {:?}, \
                       next_descr_length: 0x{:02x} }}",
               self.descr_length, self.header, self.next_descr_length)
    }
}

impl fmt::Debug for VariantData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        macro_rules! dump {
            ($f:expr, $self:expr => {
                $(($id:ident, $inside_parens:expr),)*
            }) => (
                match $self {
                    VariantData::Nothing => write!($f, "VariantData::Nothing"),
                    $(VariantData::$id(d)
                      => write!($f, concat!("VariantData::",
                                            stringify!($id),
                                            "(",
                                            $inside_parens,
                                            ")"),
                                d),)*
                }
            );
        }
        dump!(f, self => {
            (I8, "0x{:02x}"),
            (I16, "0x{:04x}"),
            (I32, "0x{:08x}"),
            (I64, "0x{:016x}"),
            (U8, "0x{:02x}"),
            (U16, "0x{:04x}"),
            (U32, "0x{:08x}"),
            (U64, "0x{:016x}"),
            (F32, "{}"),
            (F64, "{}"),
            (F128, "{:?}"),
            (Complex32, "{}"),
            (Complex64, "{}"),
            (Complex128, "{:?}"),
            (Boolean, "{}"),
            (String, "{:?}"),
            (Path, "{:?}"),
            (Array1D, "{:?}"),
            (ArrayMDim, "{:?}"),
            (Cluster, "{:?}"),
            (Variant, "{:?}"),
            (Timestamp, "{:?}"),
            (I16Waveform, "{:?}"),
            (F64Waveform, "{:?}"),
            (F32Waveform, "{:?}"),
            (F128Waveform, "{:?}"),
            (U8Waveform, "{:?}"),
            (U16Waveform, "{:?}"),
            (U32Waveform, "{:?}"),
            (I8Waveform, "{:?}"),
            (I32Waveform, "{:?}"),
            (Complex32Waveform, "{:?}"),
            (Complex64Waveform, "{:?}"),
            (Complex128Waveform, "{:?}"),
            (I64Waveform, "{:?}"),
            (U64Waveform, "{:?}"),
            (FixedPoint, "{:?}"),
            (Class, "{:?}"),
            (DigitalData, "{:?}"),
            (DigitalWaveform, "{:?}"),
        })
    }
}

impl fmt::Debug for VariantTypeDescr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            VariantTypeDescr::Scalar { label } => {
                write!(f, "VariantTypeDescr::Scalar {{ label: {:?} }}", label)
            },
            VariantTypeDescr::Enum { label, items } => {
                write!(f, "VariantTypeDescr::Enum {{ \
                               label: {:?}, items: {:?} }}", label, items)
            },
            VariantTypeDescr::Nothing => {
                write!(f, "VariantTypeDescr::Nothing")
            },
            VariantTypeDescr::Typedef { unknown_at_start, typedef_name,
                                        unknown0, type_header } => {
                write!(f, "VariantTypeDescr::Typedef {{ \
                               unknown_at_start: 0x{:08x}, \
                               typedef_name: vec!{:?}, \
                               unknown0: 0x{:04x}, \
                               type_header: {:?} }}",
                       unknown_at_start, typedef_name, unknown0, type_header)
            },
            VariantTypeDescr::PrimitiveRefnum { subtype, label } => {
                write!(f, "VariantTypeDescr::PrimitiveRefnum {{ \
                               subtype: VariantExtendedType::{:?}, \
                               label: {:?} }}",
                       subtype, label)
            },
            VariantTypeDescr::Synchronization {
                subtype, root_type_idxes, label
            } => {
                write!(f, "VariantTypeDescr::Synchronization {{ \
                               subtype: VariantExtendedType::{:?},
                               root_type_idxes: {:?}, \
                               label: {:?} }}",
                       subtype, root_type_idxes, label)
            },
            VariantTypeDescr::FixedPoint { unknown0, options, sign_bit_mask,
                                           bit_mask, unknown1, unknown2,
                                           label } => {
                write!(f, "VariantTypeDescr::FixedPoint {{ \
                               unknown0: 0x{:04x}, \
                               options: {:?}, \
                               sign_bit_mask: 0x{:016x}, \
                               bit_mask: 0x{:016x}, \
                               unknown1: 0x{:016x}, \
                               unknown2: 0x{:016x}, \
                               label: {:?} }}",
                       unknown0, options, sign_bit_mask, bit_mask, unknown1,
                       unknown2, label)
            },
            VariantTypeDescr::Array { num_dimensions, size_bounds, label,
                                      root_type_idx } => {
                write!(f, "VariantTypeDescr::Array {{ \
                               num_dimensions: 0x{:04x}, \
                               size_bounds: {:?}, \
                               label: {:?}, \
                               root_type_idx: {} \
                           }}",
                       num_dimensions,
                       size_bounds,
                       label,
                       root_type_idx)
            },
            VariantTypeDescr::Cluster { root_type_idxes, label } => {
                write!(f, "VariantTypeDescr::Cluster {{ \
                               root_type_idxes: {:?}, \
                               label: {:?} }}",
                       root_type_idxes, label)
            },
            VariantTypeDescr::String { size_bound, label } => {
                write!(f, "VariantTypeDescr::String {{ \
                               size_bound: 0x{:08x}, \
                               label: {:?} }}",
                       size_bound, label)
            },
            VariantTypeDescr::Timestamped { subtype, label } => {
                write!(f, "VariantTypeDescr::Timestamped {{ \
                               subtype: VariantTimestampedType::{:?}, \
                               label: {:?} }}",
                       subtype, label)
            },
            VariantTypeDescr::Class { unknown0, class_name, label } => {
                write!(f, "VariantTypeDescr::Class {{ \
                               unknown0: {:?}, \
                               class_name: {:?}, \
                               label: {:?} }}",
                       unknown0, class_name, label)
            },
            VariantTypeDescr::Unknown37 { unknown0, unknown1, unknown2 } => {
                write!(f, "VariantTypeDescr::Unknown37 {{ \
                               unknown0: 0x{:08x}, \
                               unknown1: 0x{:04x}, \
                               unknown2: {:?} }}",
                       unknown0, unknown1, unknown2)
            },
            VariantTypeDescr::InstrumentDescriptor {
                ivi_class, root_type_idxes, unknown0, unknown1, label,
            } => {
                write!(f, "VariantTypeDescr::InstrumentDescriptor {{ \
                               ivi_class: {:?}, \
                               root_type_idxes: {:?}, \
                               unknown0: 0x{:04x}, \
                               unknown1: {:?}, \
                               label: {:?} }}",
                       ivi_class, root_type_idxes, unknown0, unknown1, label)
            },
            VariantTypeDescr::DAQmxTask {
                type_name, root_type_idx, unknown0, class_name, unknown1, label
            } => {
                write!(f, "VariantTypeDescr::DAQmxTask {{ \
                               type_name: {:?}, \
                               root_type_idx: {}, \
                               unknown0: 0x{:04x}, \
                               class_name: {:?}, \
                               unknown1: {:?}, \
                               label: {:?} }}",
                       type_name, root_type_idx, unknown0, class_name, unknown1,
                       label)
            },
        }
    }
}

// vim: tw=80
