/*!
This module provides implementations of [`serde::de::Deserializer`] and friends.
 */

use std::fmt;

use serde::de;
use serde::de::Deserializer;
use serde::de::DeserializeSeed;
use serde::de::Visitor;
use serde::de::SeqAccess;

use super::LVParse;
use super::LVError;
use super::LVResult;

/// `LVDeserializer` describes serde deserializer.
struct LVDeserializer<P> {
    /// Some data reader for which parsing was implemented.
    parser: P,
}

macro_rules! create_deserialize {
    ($fname:ident, $de:tt => $self:ident, $name:ident,
                             $v2:ident : $v2t:ty, $visitor:ident
            $code:block) => {
        fn $fname<V: Visitor<$de>>($self, $name: &'static str, $v2: $v2t,
                                   $visitor: V)
            -> LVResult<V::Value>
            $code
    };
    ($fname:ident, $de:tt => +named +sized $impl_fname:ident) => {
        create_deserialize!(
            $fname, $de => self, _name, size: usize, visitor {
                self.$impl_fname(size, visitor)
            });
    };

    ($fname:ident, $de:tt => $self:ident, $v:ident : $vt:ty, $visitor:ident
                             $code:block) => {
        fn $fname<V: Visitor<$de>>($self, $v: $vt, $visitor: V)
            -> LVResult<V::Value>
            $code
    };
    ($fname:ident, $de:tt => $self:ident, $name:ident, $visitor:ident
                             $code:block) => {
        create_deserialize!($fname, $de => $self, $name: &str, $visitor $code);
    };
    ($fname:ident, $de:tt => +named $impl_fname:ident) => {
        create_deserialize!($fname, $de => self, _name, visitor {
            self.$impl_fname(visitor)
        });
    };

    ($fname:ident, $de:tt => $self:ident, $visitor:ident $code:block) => {
        fn $fname<V: Visitor<$de>>($self, $visitor: V) -> LVResult<V::Value>
            $code
    };
    ($fname:ident, $de:tt, $visit_fname:ident) => {
        create_deserialize!($fname, $de => self, visitor {
            visitor.$visit_fname(self.parser.read_lv()?)
        });
    };
    ($fname:ident, $de:tt, error $error:expr) => {
        create_deserialize!($fname, $de => self, _visitor {
            Err(LVError::ParseError($error.to_string()))
        });
    };
    ($fname:ident, $de:tt => $impl_fname:ident) => {
        create_deserialize!($fname, $de => self, visitor {
            self.$impl_fname(visitor)
        });
    };
}

impl<'der, 'de, P: LVParse> Deserializer<'de> for &'der mut LVDeserializer<P> {
    type Error = LVError;

    create_deserialize!(deserialize_any, 'de,
                        error "Unable to deserialize data without type hints");
    create_deserialize!(deserialize_ignored_any, 'de,
                        error "Unable to deserialize data without type hints");
    create_deserialize!(deserialize_char, 'de,
                        error "LabVIEW has no character type");
    create_deserialize!(deserialize_str, 'de,
                        error "LabVIEW is using locale encoding, anything \
                               stringy is thus not supported now");
    create_deserialize!(deserialize_string, 'de,
                        error "LabVIEW is using locale encoding, anything \
                               stringy is thus not supported now");
    create_deserialize!(deserialize_option, 'de,
                        error "Unable to determine how optional type should be \
                               encoded");
    create_deserialize!(deserialize_map, 'de,
                        error "LabVIEW does not have built-in dictionary \
                               types");
    create_deserialize!(deserialize_identifier, 'de,
                        error "LabVIEW hardly ever dumps identifiers unless \
                               inside dumped variants");

    create_deserialize!(deserialize_bool, 'de, visit_bool);

    create_deserialize!(deserialize_i8, 'de, visit_i8);
    create_deserialize!(deserialize_i16, 'de, visit_i16);
    create_deserialize!(deserialize_i32, 'de, visit_i32);
    create_deserialize!(deserialize_i64, 'de, visit_i64);

    create_deserialize!(deserialize_u8, 'de, visit_u8);
    create_deserialize!(deserialize_u16, 'de, visit_u16);
    create_deserialize!(deserialize_u32, 'de, visit_u32);
    create_deserialize!(deserialize_u64, 'de, visit_u64);

    create_deserialize!(deserialize_f32, 'de, visit_f32);
    create_deserialize!(deserialize_f64, 'de, visit_f64);

    create_deserialize!(deserialize_bytes, 'de => self, visitor {
        let bytes = self.parser.read_lv_1d_array::<u8>()?;
        visitor.visit_bytes(bytes.as_slice())
    });

    create_deserialize!(deserialize_byte_buf, 'de => deserialize_bytes);

    create_deserialize!(deserialize_unit, 'de => self, visitor {
        visitor.visit_unit()
    });

    create_deserialize!(deserialize_unit_struct, 'de
                        => +named deserialize_bytes);

    create_deserialize!(deserialize_newtype_struct, 'de => self, _n, visitor {
        visitor.visit_newtype_struct(self)
    });

    create_deserialize!(deserialize_seq, 'de => self, visitor {
        let size: u32 = self.parser.read_lv()?;
        self.deserialize_tuple(size as usize, visitor)
    });

    create_deserialize!(deserialize_tuple, 'de => self, size: usize, visitor {
        /// `LVArray` describes serde sequence.
        struct LVArray<'ader, P: 'ader + LVParse> {
            deserializer: &'ader mut LVDeserializer<P>,
            size: u32,
        }

        impl<'de, 'ader, P: LVParse> SeqAccess<'de> for LVArray<'ader, P>
        {
            type Error = LVError;

            fn next_element_seed<T>(&mut self, seed: T)
                -> LVResult<Option<T::Value>>
                where T: DeserializeSeed<'de>
            {
                if self.size == 0 {
                    Ok(None)
                } else {
                    self.size -= 1;
                    seed.deserialize(&mut *self.deserializer).map(Some)
                }
            }

            fn size_hint(&self) -> Option<usize> {
                Some(self.size as usize)
            }
        }

        visitor.visit_seq(LVArray {
            deserializer: self,
            size: size as u32,
        })
    });

    create_deserialize!(deserialize_tuple_struct, 'de
                        => +named +sized deserialize_tuple);

    create_deserialize!(
        deserialize_struct, 'de => self, name, fields: &[&str], visitor {
            let size = fields.len();
            self.deserialize_tuple_struct(name, size, visitor)
        });

    create_deserialize!(
        deserialize_enum, 'de
        => self, _name, _variants: &[&str], _visitor {
            Err(LVError::ParseError(
                    "Parsing enums is currently not supported".to_string()))
        });
}

impl de::Error for LVError {
    fn custom<T: fmt::Display>(msg: T) -> Self {
        LVError::ParseError(format!("Serde parse error: {}", msg))
    }
}
