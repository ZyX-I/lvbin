use super::LVParse;
use super::LVValue;
use super::LVResult;
use super::LVError;
use super::variant::Variant;
use super::timestamp::Timestamp;
use super::mdimarray::MDimArray;


/// Structure which contains LabVIEW waveform.
#[derive(Debug, Clone, PartialEq)]
pub struct Waveform<R> {
    /// Start time.
    pub timestamp: Timestamp,
    /// Time passed between entries, in seconds.
    pub time_increment: f64,
    /// Values stored in Y array.
    pub values: Vec<R>,
    /// Some additional attributes.
    pub attributes: Variant,
}

impl<R: LVValue> LVValue for Waveform<R> {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        let timestamp = p.read_lv()?;
        let time_increment = p.read_lv()?;
        let values = p.read_lv_1d_array()?;
        for idx in 0 .. 9 {
            if p.read_byte()? != 0 {
                Err(LVError::ParseError(
                    format!("Expected zero at byte {} after waveform values",
                            idx + 1)))?;
            }
        }
        let attributes = p.read_lv()?;
        Ok(Waveform {
            timestamp: timestamp,
            time_increment: time_increment,
            values: values,
            attributes: attributes,
        })
    }
}

/// Structure which contains data for digital waveform.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct DigitalData {
    /// Transitions array.
    pub transitions: Vec<u32>,
    /// Two-dimensional data array.
    pub data: MDimArray<u8>,
}

impl LVValue for DigitalData {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        let transitions = p.read_lv_1d_array()?;
        let data = p.read_lv_array(2)?;
        Ok(DigitalData {
            transitions: transitions,
            data: data,
        })
    }
}

/// Structure which contains LabVIEW digital waveform.
#[derive(Debug, Clone, PartialEq)]
pub struct DigitalWaveform {
    /// Start time.
    pub timestamp: Timestamp,
    /// Time passed between entries, in seconds.
    pub time_increment: f64,
    /// Waveform data.
    pub data: DigitalData,
    /// Unknown constant.
    pub unknown0: u64,
    /// Some additional attributes.
    pub attributes: Variant,
}

impl LVValue for DigitalWaveform {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        let timestamp = p.read_lv()?;
        let time_increment = p.read_lv()?;
        let data = p.read_lv()?;
        let unknown0 = p.read_lv()?;
        if p.read_byte()? != 0 {
            Err(LVError::ParseError(
                format!("Expected zero at byte after unknown number")))?;
        }
        let attributes = p.read_lv()?;
        Ok(DigitalWaveform {
            timestamp: timestamp,
            time_increment: time_increment,
            data: data,
            unknown0: unknown0,
            attributes: attributes,
        })
    }
}

// vim: tw=80
