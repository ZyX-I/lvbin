use super::LVParse;
use super::LVValue;
use super::LVResult;

/// Timestamp represented exactly like LabVIEW saved it.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Timestamp {
    /// Number of seconds since 1904-01-01 00:00:00 UTC.
    ///
    /// This value may be negative for dates before this one.
    pub seconds: i64,
    /// Number of 1/(2**64) fractions of seconds.
    pub fractions: u64,
}

impl LVValue for Timestamp {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        let seconds = p.read_lv()?;
        let fractions = p.read_lv()?;
        Ok(Timestamp {
            seconds: seconds,
            fractions: fractions,
        })
    }
}
