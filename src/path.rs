use std::io;

use num_traits::FromPrimitive;

use super::LVParse;
use super::LVValue;
use super::LVResult;
use super::LVError;
use super::binstr::BinaryStrings;
use super::binstr::BinaryString;

/// Enum representing possible LabVIEW path types.
#[derive(Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum PathType {
    Absolute = 0x0000,
    Relative = 0x0001,
    Invalid = 0x0002,
}

/// Structure representing LabVIEW path.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Path {
    pub path_type: PathType,
    pub components: BinaryStrings,
}

impl LVValue for Path {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        // All path constants start with a header.
        const EXPECTED_HDR: [u8; 4] = *b"PTH0";
        let mut hdr = [0; 4];
        p.read_bytes(&mut hdr)?;
        if hdr != EXPECTED_HDR {
            Err(LVError::ParseError(format!(
                "Expected path header {:?}, got {:?}",
                EXPECTED_HDR, hdr)))?;
        }
        let total_len: u32 = p.read_lv()?;
        let mut data = vec![0; total_len as usize];
        p.read_bytes(&mut data)?;
        let mut data_cursor = io::Cursor::new(data);

        let path_type = data_cursor.read_lv()?;
        let num_components: u16 = data_cursor.read_lv()?;
        let mut components = Vec::with_capacity(num_components as usize);
        for _ in 0 .. num_components {
            components.push(BinaryString(
                data_cursor.read_lv_1d_array_sized::<u8, u8>()?));
        }

        Ok(Path {
            path_type: path_type,
            components: BinaryStrings(components),
        })
    }
}

impl LVValue for PathType {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        p.read_lv_u16().and_then(|n| {
            PathType::from_u16(n).ok_or_else(
                || LVError::ParseError(format!("Unknown type number: {:04x}",
                                               n)))
        })
    }
}

// vim: tw=80
