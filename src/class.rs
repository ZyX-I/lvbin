use std::io;
use std::fmt;

use super::LVParse;
use super::LVValue;
use super::LVResult;
use super::binstr::BinaryStrings;
use super::binstr::BinaryString;

/// Structure which contains LabVIEW class version.
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct ClassVersion {
    pub major: u16,
    pub minor: u16,
    pub fix: u16,
    pub build: u16,
}

/// Structure which represents full class name.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ClassName(pub BinaryStrings);

/// Structure which contains LabVIEW class data.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Class {
    /// Class name, including owning library names.
    pub class_name: ClassName,
    /// Class versions.
    ///
    /// That is, version of the actual class then version of the parent then 
    /// etc. LVObject class while being the top-level in class hierarchy does 
    /// not actually participate here.
    pub versions: Vec<ClassVersion>,
    /// Class private data clusters’ data.
    ///
    /// That is, private data of the actual class then data of the parent then 
    /// etc. LVObject class while being the top-level in class hierarchy does 
    /// not actually participate here.
    pub data: Vec<Vec<u8>>,
}

pub const ZERO_VERSION: ClassVersion = ClassVersion {
    major: 0,
    minor: 0,
    fix: 0,
    build: 0,
};

impl LVValue for ClassVersion {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        let major = match p.read_lv()? {
            0 => p.read_lv()?,
            n => n,
        };
        // FIXME Try getting ZERO_VERSION using different means.
        // When got two 0x0000 assume it is empty class.
        if major == 0 {
            Ok(ZERO_VERSION)
        } else {
            let minor = p.read_lv()?;
            let fix = p.read_lv()?;
            let build = p.read_lv()?;
            Ok(ClassVersion {
                major: major,
                minor: minor,
                fix: fix,
                build: build,
            })
        }
    }
}

impl fmt::Debug for ClassVersion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ClassVersion {{ major: {}, minor: {}, fix: {}, build: {} }}",
               self.major, self.minor, self.fix, self.build)
    }
}

impl LVValue for ClassName {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        let full_name = p.read_lv_1d_array_sized::<u8, u8>()?;
        let full_name_len = full_name.len();
        let is_array = full_name_len != 0 && *full_name.last().unwrap() == 0;
        let mut class_name = vec![];
        if is_array {
            let mut full_name_cursor = io::Cursor::new(full_name);
            loop {
                let name = full_name_cursor.read_lv_1d_array_sized::<u8, u8>()?;
                if name.len() == 0 {
                    break;
                }
                class_name.push(BinaryString(name));
            }
            class_name.push(BinaryString(vec![]));
            if full_name_len % 2 == 0 {
                p.read_byte()?;
            }
        } else {
            class_name.push(BinaryString(full_name));
        }
        Ok(ClassName(BinaryStrings(class_name)))
    }
}

impl ClassName {
    /// True if class name contains was parsed using array method.
    ///
    /// “Array method” is used when parser finds that end of the class name 
    /// contains zero byte. If it does parser assumes that class name is an 
    /// array of pascal strings (1-byte length + actual string) terminated by 
    /// zero byte. If it does not parser assumes that class name is one string, 
    /// not terminated by anything.
    ///
    /// “Array method” is used for almost all classes except for the top level 
    /// “LabVIEW Object” class which each class inherits from.
    pub fn is_array(&self) -> bool {
        let ClassName(BinaryStrings(arr)) = self;
        arr.last().map_or(false, |bs| bs.as_bytes().is_empty())
    }
}

impl LVValue for Class {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        let num_classes = p.read_lv()?;
        let (class_name, versions, data) = if num_classes > 0 {
            let class_name = p.read_lv()?;
            let mut versions = vec![];
            for _ in 0 .. num_classes {
                versions.push(p.read_lv()?);
            }
            let mut data = vec![];
            for _ in 0 .. num_classes {
                data.push(p.read_lv_1d_array()?);
            }
            (class_name, versions, data)
        } else {
            (ClassName(BinaryStrings(vec![])), vec![], vec![])
        };
        Ok(Class {
            class_name: class_name,
            versions: versions,
            data: data,
        })
    }
}

// vim: tw=80
