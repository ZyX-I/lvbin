use super::LVParse;
use super::LVValue;
use super::LVResult;

use std::fmt;
use std::vec::Vec;

/// BinaryString type, defined mostly to get nicer display.
#[derive(Clone, Eq, PartialEq)]
pub struct BinaryString(pub Vec<u8>);

pub fn read_lv_string<S, T>(p: &mut T) -> LVResult<BinaryString>
    where S: LVValue + Into<usize>,
          T: LVParse + ?Sized
{
    Ok(BinaryString(p.read_lv_1d_array_sized::<S, u8>()?))
}

impl BinaryString {
    #[inline]
    pub fn as_bytes(&self) -> &[u8] {
        let BinaryString(s) = self;
        s.as_slice()
    }
}

fn format_byte_string(f: &mut fmt::Formatter, bytes: &[u8]) -> fmt::Result {
    write!(f, "b\"")?;
    for byte in bytes {
        match byte {
            byte @ 0x20 ..= 0x7E => {
                f.write_str(&String::from_utf8_lossy(&[*byte]))?;
            },
            byte => {
                write!(f, "\\x{:02x}", byte)?;
            },
        }
    }
    write!(f, "\"")
}

impl fmt::Debug for BinaryString {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let BinaryString(name) = self;
        write!(f, "BinaryString(")?;
        format_byte_string(f, name.as_slice())?;
        write!(f, ".to_vec())")
    }
}

impl fmt::Display for BinaryString {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let BinaryString(name) = self;
        format_byte_string(f, name.as_slice())
    }
}

/// BinaryStrings type, defined mostly to get nicer display.
#[derive(Clone, Eq, PartialEq)]
pub struct BinaryStrings(pub Vec<BinaryString>);

impl fmt::Display for BinaryStrings {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let BinaryStrings(names) = self;
        write!(f, "[")?;
        let mut first = true;
        for BinaryString(name) in names {
            if first {
                first = false;
            } else {
                write!(f, ", ")?;
            }
            format_byte_string(f, name.as_slice())?;
        }
        write!(f, "]")
    }
}

impl fmt::Debug for BinaryStrings {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let BinaryStrings(names) = self;
        write!(f, "BinaryStrings(vec!{:?})", names)
    }
}

impl LVValue for BinaryString {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        p.read_lv_1d_array().map(Self)
    }
}
