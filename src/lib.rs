/*!
This crate provides methods for parsing LabVIEW binary files.

The organization of the crate is the following:

- Top-level module ([`lvbin`]) contains general-purpose traits [`LVValue`] and 
  [`LVParse`] as well as [`LVResult`] type definition with friends.

  [`LVValue`] trait describes types which may be found in LabVIEW. If you need 
  to parse your own cluster you may find it useful to implement it. This crate 
  provides implementation of this trait for all numeric types.

  [`LVParse`] trait describes types which may be parsed into [`LVValue`]s. 
  That is, any type from which you may get data byte by byte. Most useful is 
  obviously implementation for [`std::io::Read`]-implementing types.

- Submodules [`lvbin::timestamp`], [`lvbin::fixedpoint`], [`lvbin::path`], 
  [`lvbin::bigfloat`], [`lvbin::class`] and [`lvbin::waveform`] describe types 
  specific to LabVIEW which have no (good enough) rust equivalents.

- Submodule [`lvbin::binstr`] defines type which represents binary strings. Most 
  useful parts of the module are [`std::fmt::Debug`] and [`std::fmt::Display`] 
  implementations.

- Submodule [`lvbin::mdimarray`] provides the rust type which represents LabVIEW 
  multidimensional array.

- And the most complicated submodule [`lvbin::variant`] provides the rust type 
  which represents LabVIEW variant. Since LabVIEW variants may hold any LabVIEW 
  type [`lvbin::variant`] submodule is using every other submodule in this 
  crate.

 */

extern crate byteorder;
#[macro_use]
extern crate num_derive;
extern crate num_traits;
extern crate num_complex;
extern crate serde;

pub mod timestamp;
pub mod mdimarray;
pub mod variant;
pub mod binstr;
pub mod fixedpoint;
pub mod path;
pub mod bigfloat;
pub mod class;
pub mod waveform;
mod deserialize;

use std::io;
use std::fmt;
use std::usize;
use std::error::Error;
use std::mem::size_of;
use std::convert::From;
use std::convert::Into;
use std::boxed::Box;
use std::any::type_name;

use byteorder::ReadBytesExt;
use byteorder::BigEndian;
use num_complex::Complex;
use num_traits::FromPrimitive;

use mdimarray::MDimArray;

/// `LVError` describes error which may occur during parsing LabVIEW binary 
/// files.
#[derive(Debug, Clone, PartialEq)]
pub enum LVError {
    /// IO error occurred during reading from [`std::io::Read`] object.
    IOError(io::ErrorKind, String),
    /// Parsing error used when reading succeeded, but read data does not make 
    /// sense.
    ParseError(String),
}

/// Size type used by 32-bit LabVIEW 2015.
///
/// This LabVIEW uses 32-bit sizes as array prefixes, but there is no 
/// `From<u32>` implementation for [`usize`].
struct U32Size(u32);

/// A type alias used for return values in most public functions that may fail
pub type LVResult<T> = Result<T, LVError>;

impl From<io::Error> for LVError {
    fn from(error: io::Error) -> Self {
        LVError::IOError(error.kind(), format!("{}", error))
    }
}

impl Error for LVError {}

impl fmt::Display for LVError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            LVError::IOError(kind, msg)
                => write!(
                    f,
                    "I/O error {:?} occurred while trying to read LVValue: {}",
                    kind, msg),
            LVError::ParseError(s)
                => write!(f, "Failed to parse LVValue: {}", s),
        }
    }
}

/// `LVParse` describes types that can be parsed into [`LVValue`] types.
///
/// The only function which is required to be implemented here is 
/// [`LVParse::read_byte`]. But for performance reasons it is suggested to 
/// implement at least all `LVParse::read_lv_u*` functions otherwise other 
/// functions will read data by calling [`LVParse::read_byte`] for each byte 
/// they need.
pub trait LVParse {
    #[inline]
    /// Reads one u8 value.
    fn read_lv_u8(&mut self) -> LVResult<u8> {
        self.read_byte()
    }

    /// Reads u16 value.
    fn read_lv_u16(&mut self) -> LVResult<u16> {
        Ok(((self.read_byte()? as u16) << 8)
            | (self.read_byte()? as u16))
    }

    /// Reads u32 value.
    fn read_lv_u32(&mut self) -> LVResult<u32> {
        Ok(((self.read_byte()? as u32) << 24)
            | ((self.read_byte()? as u32) << 16)
            | ((self.read_byte()? as u32) << 8)
            | (self.read_byte()? as u32))
    }

    /// Reads u64 value.
    fn read_lv_u64(&mut self) -> LVResult<u64> {
        Ok(((self.read_byte()? as u64) << 56)
            | ((self.read_byte()? as u64) << 48)
            | ((self.read_byte()? as u64) << 40)
            | ((self.read_byte()? as u64) << 32)
            | ((self.read_byte()? as u64) << 24)
            | ((self.read_byte()? as u64) << 16)
            | ((self.read_byte()? as u64) << 8)
            | ((self.read_byte()? as u64)))
    }

    /// Reads u128 value.
    fn read_lv_u128(&mut self) -> LVResult<u128> {
        Ok(((self.read_lv_u64()? as u128) << 64)
           | (self.read_lv_u64()? as u128))
    }

    /// Reads i8 value.
    fn read_lv_i8(&mut self) -> LVResult<i8> {
        Ok(self.read_lv_u8()? as i8)
    }

    /// Reads i16 value.
    fn read_lv_i16(&mut self) -> LVResult<i16> {
        Ok(self.read_lv_u16()? as i16)
    }

    /// Reads i32 value.
    fn read_lv_i32(&mut self) -> LVResult<i32> {
        Ok(self.read_lv_u32()? as i32)
    }

    /// Reads i64 value.
    fn read_lv_i64(&mut self) -> LVResult<i64> {
        Ok(self.read_lv_u64()? as i64)
    }

    /// Reads i128 value.
    fn read_lv_i128(&mut self) -> LVResult<i128> {
        Ok(self.read_lv_u128()? as i128)
    }

    /// Reads f32 value.
    fn read_lv_f32(&mut self) -> LVResult<f32> {
        let n = self.read_lv_u32()?;
        // TODO: Actually parse float by default, leaving below as an 
        // optimization for implementors.
        Ok(unsafe { *(&n as *const u32 as *const f32) })
    }

    /// Reads f64 value.
    fn read_lv_f64(&mut self) -> LVResult<f64> {
        let n = self.read_lv_u64()?;
        // TODO: Actually parse float by default, leaving below as an 
        // optimization for implementors.
        Ok(unsafe { *(&n as *const u64 as *const f64) })
    }

    /// Fetch LabVIEW multidimentional array with `num_dimensions` number of 
    /// dimensions containing values with type `V` where size uses type `S`.
    fn read_lv_array_sized<S, V>(&mut self, num_dimensions: usize)
        -> LVResult<MDimArray<V>>
        where S: LVValue + Into<usize>,
              V: LVValue
    {
        let mut sizes = Vec::with_capacity(num_dimensions);
        let mut num_elements: usize = 1;
        for _ in 0 .. num_dimensions {
            let size = Into::<usize>::into(self.read_lv::<S>()?);
            sizes.push(size);
            num_elements = num_elements.checked_mul(size).ok_or(
                LVError::ParseError(format!(
                        "Size {} * {} is too big to fit into usize",
                        size, num_elements)))?;
        }
        let mut data = Vec::<V>::with_capacity(num_elements);
        for _ in 0 .. num_elements {
            data.push(self.read_lv()?);
        }
        Ok(MDimArray {
            sizes: sizes,
            data: data,
        })
    }

    /// Reads array of elements `V` with single dimension, but with sizes of 
    /// type `S`.
    fn read_lv_1d_array_sized<S, V>(&mut self) -> LVResult<Vec<V>>
        where S: LVValue + Into<usize>,
              V: LVValue
    {
        Ok(self.read_lv_array_sized::<S, V>(1)?.data)
    }

    #[inline]
    /// Reads array of elements `V` with single dimension with four-byte sizes.
    fn read_lv_1d_array<V: LVValue>(&mut self) -> LVResult<Vec<V>> {
        self.read_lv_1d_array_sized::<U32Size, V>()
    }

    #[inline]
    /// Reads multidimensional array of elements `V` with `num_dimensions` 
    /// dimensions and four-byte sizes.
    fn read_lv_array<V: LVValue>(&mut self, num_dimensions: usize)
        -> LVResult<MDimArray<V>>
    {
        self.read_lv_array_sized::<U32Size, V>(num_dimensions)
    }

    /// Reads exact number of bytes defined by slice length.
    fn read_bytes(&mut self, buf: &mut [u8]) -> LVResult<()> {
        for byte in buf {
            *byte = self.read_byte()?;
        }
        Ok(())
    }

    /// Reads one byte.
    fn read_byte(&mut self) -> LVResult<u8>;

    /// Reads [`LVValue`].
    ///
    /// This is a convenience method that just calls [`LVValue::read_lv`] 
    /// method.
    #[inline]
    fn read_lv<T: LVValue>(&mut self) -> LVResult<T> {
        LVValue::read_lv(self)
    }
}

/// `LVValue` describes types that can be found in LabVIEW binary file.
pub trait LVValue : Sized {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self>;
}

macro_rules! make_lvvalue {
    ($type:ty, $p:ident, $body:expr) => {
        impl LVValue for $type {
            fn read_lv<T: LVParse + ?Sized>($p: &mut T) -> LVResult<Self> {
                $body
            }
        }
    };
}

make_lvvalue!(bool, p, match p.read_byte() {
    Ok(0) => Ok(false),
    Ok(1) => Ok(true),
    Ok(n) => Err(LVError::ParseError(format!(
            "Byte 0x{:02x} is not a valid boolean value", n))),
    Err(e) => Err(e),
});

make_lvvalue!(u8, p, p.read_lv_u8());
make_lvvalue!(u16, p, p.read_lv_u16());
make_lvvalue!(u32, p, p.read_lv_u32());
make_lvvalue!(u64, p, p.read_lv_u64());
make_lvvalue!(u128, p, p.read_lv_u128());

make_lvvalue!(i8, p, p.read_lv_i8());
make_lvvalue!(i16, p, p.read_lv_i16());
make_lvvalue!(i32, p, p.read_lv_i32());
make_lvvalue!(i64, p, p.read_lv_i64());
make_lvvalue!(i128, p, p.read_lv_i128());

make_lvvalue!(f32, p, p.read_lv_f32());
make_lvvalue!(f64, p, p.read_lv_f64());

make_lvvalue!(U32Size, p, Ok(U32Size(p.read_lv_u32()?)));

impl<V: LVValue> LVValue for Box<V> {
    fn read_lv<T: LVParse + ?Sized>(p: &mut T) -> LVResult<Self> {
        Ok(Box::new(p.read_lv()?))
    }
}

impl<T: LVValue> LVValue for Complex<T> {
    fn read_lv<P: LVParse + ?Sized>(p: &mut P) -> LVResult<Self> {
        let re = p.read_lv()?;
        let im = p.read_lv()?;
        Ok(Complex { re: re, im: im })
    }
}

macro_rules! create_read {
    ($fname:ident, $type:ident, $read_fn:ident) => {
        /// Fetch big endian number with type $type from stream
        #[inline]
        fn $fname(&mut self) -> LVResult<$type> {
            Ok(self.$read_fn::<BigEndian>()?)
        }
    };
}

impl<T: io::Read> LVParse for T {
    fn read_byte(&mut self) -> LVResult<u8> {
        Ok(self.read_u8()?)
    }

    // Assuming byteorder crate has more optimal implementations.

    fn read_lv_i8(&mut self) -> LVResult<i8> {
        Ok(self.read_i8()?)
    }

    create_read!(read_lv_u16, u16, read_u16);
    create_read!(read_lv_u32, u32, read_u32);
    create_read!(read_lv_u64, u64, read_u64);
    create_read!(read_lv_u128, u128, read_u128);

    create_read!(read_lv_i16, i16, read_i16);
    create_read!(read_lv_i32, i32, read_i32);
    create_read!(read_lv_i64, i64, read_i64);
    create_read!(read_lv_i128, i128, read_i128);

    create_read!(read_lv_f32, f32, read_f32);
    create_read!(read_lv_f64, f64, read_f64);
}

impl From<u32> for U32Size {
    fn from(n: u32) -> U32Size {
        U32Size(n)
    }
}

impl From<U32Size> for usize {
    fn from(size: U32Size) -> usize {
        let U32Size(n) = size;
        if size_of::<u32>() >= size_of::<usize>() {
            n as usize
        } else {
            if n > usize::MAX as u32 {
                panic!("Number {} is greater then usize::MAX ({})",
                       n, usize::MAX);
            } else {
                n as usize
            }
        }
    }
}

/// Trait which restricts representing numbers to whatever is defined here and 
/// allows for actually creating [[LVEnum::read_lv_enum]] below.
///
/// Note that you are not supposed to create your own implementations of this 
/// trait, it is only public because making it private produces warning.
pub unsafe trait RepresentingNumber : fmt::Display + LVValue + Copy {
    /// Convert RepresentingNumber to some type (normally enum) which implements 
    /// [[FromPrimitive]].
    fn from_representing_number<T: FromPrimitive>(self) -> Option<T>;
}

macro_rules! gen_representing_number {
    ($type:ty, $func:ident) => {
        unsafe impl RepresentingNumber for $type {
            fn from_representing_number<T: FromPrimitive>(self) -> Option<T> {
                T::$func(self)
            }
        }
    };
}

gen_representing_number!(u8, from_u8);
gen_representing_number!(u16, from_u16);
gen_representing_number!(u32, from_u32);

/// Trait which makes it easier to read enums from a LabVIEW binary files.
pub trait LVEnum : FromPrimitive {
    /// Underlying representation type. Check out what type is used by a given 
    /// enum by opening enum typedef (`.ctl`) file, opening properties window of 
    /// the contained enum control and checking out “Data Type” tab there.
    type Representation: RepresentingNumber;

    /// Read LabVIEW enum dumped into a binary file.
    fn read_lv_enum<R: LVParse + ?Sized>(p: &mut R) -> LVResult<Self> {
        (p.read_lv() as LVResult<Self::Representation>).and_then(|v| {
            Self::Representation::from_representing_number(v).ok_or_else(|| {
                LVError::ParseError(
                    format!("Unable to cast {} to {}", v, type_name::<Self>()))
            })
        })
    }
}

/// Generate LVValue implementation for a given enum.
#[macro_export]
macro_rules! gen_lvvalue_for_enum {
    ($enum:ty, $repr:ty) => {
        impl $crate::LVEnum for $enum {
            type Representation = $repr;
        }
        impl $crate::LVValue for $enum {
            fn read_lv<R>(p: &mut R) -> $crate::LVResult<Self>
                where R: $crate::LVParse + ?Sized
            {
                $crate::LVEnum::read_lv_enum(p)
            }
        }
    };
}

#[cfg(test)]
mod tests {
}

// vim: tw=80
