extern crate lvbin;
extern crate num_complex;

use std::io;
use std::io::Read;
use std::fmt;
use std::f64;
use std::f32;

use num_complex::Complex;
use num_complex::Complex32;
use num_complex::Complex64;

use lvbin::LVValue;
use lvbin::LVParse;
use lvbin::variant::VariantType;
use lvbin::variant::VariantExtendedType;
use lvbin::variant::VariantTimestampedType;
use lvbin::variant::VariantTypeDescr;
use lvbin::variant::VariantData;
use lvbin::variant::VariantTypeHeader;
use lvbin::variant::VariantTypeElement;
use lvbin::variant::Variant;
use lvbin::variant::UNBOUND_SIZE;
use lvbin::variant::Complex128;
use lvbin::variant::Attributes;
use lvbin::path::PathType;
use lvbin::path::Path;
use lvbin::bigfloat::F128;
use lvbin::binstr::BinaryString;
use lvbin::binstr::BinaryStrings;
use lvbin::fixedpoint::FixedPoint;
use lvbin::fixedpoint::FixedPointOptions;
use lvbin::timestamp::Timestamp;
use lvbin::class::Class;
use lvbin::class::ClassName;
use lvbin::class::ClassVersion;
use lvbin::mdimarray::MDimArray;
use lvbin::waveform::Waveform;
use lvbin::waveform::DigitalWaveform;
use lvbin::waveform::DigitalData;

trait DSLFormat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result;
}

impl DSLFormat for VariantTypeHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let VariantTypeHeader { flags, vtype, descr } = self;
        write!(f, "h!(0x{:02X}, {:?}, ", flags, vtype)?;
        descr.fmt(f)?;
        write!(f, ")")
    }
}

impl DSLFormat for Variant {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Variant {
            version,
            types,
            data,
            root_type_idx,
            attributes,
        } = self;
        let print_version = *version != 0x1500_8000;
        let print_attributes = attributes.len() != 0 || print_version ;
        let print_root_type_idx
            = (*root_type_idx as usize != types.len() - 1) || print_version;
        write!(f, "var!([")?;
        for typ in types {
            typ.fmt(f)?;
        }
        write!(f, "] => ")?;
        data.fmt(f)?;
        if print_root_type_idx {
            write!(f, ", root {}", root_type_idx)?;
        }
        if print_attributes {
            write!(f, ", attributes [", )?;
            let mut is_first = true;
            for (k, v) in attributes {
                if is_first {
                    is_first = false;
                } else {
                    write!(f, ", ")?;
                }
                write!(f, "{} => ", k)?;
                v.fmt(f)?;
            }
            write!(f, "]")?;
        }
        if print_version {
            write!(f, ", version 0x{:08X}", version)?;
        }
        write!(f, ")")
    }
}

impl DSLFormat for VariantTypeElement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let VariantTypeElement {
            next_descr_length,
            descr_length,
            header
        } = self;
        write!(f, "e!(0x{:02X}, ", descr_length)?;
        header.fmt(f)?;
        if *next_descr_length != 1 {
            write!(f, "0x{:02X}", next_descr_length)?;
        }
        write!(f, ")")
    }
}

macro_rules! dsl_format_from_str {
    ($type:ty, $str:expr) => {
        impl DSLFormat for $type {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                write!(f, $str, self)
            }
        }
    };
}

dsl_format_from_str!(f32, "{}");
dsl_format_from_str!(f64, "{}");
dsl_format_from_str!(i8, "0x{:02X}");
dsl_format_from_str!(i16, "0x{:04X}");
dsl_format_from_str!(i32, "0x{:08X}");
dsl_format_from_str!(i64, "0x{:016X}");
dsl_format_from_str!(u8, "0x{:02X}");
dsl_format_from_str!(u16, "0x{:04X}");
dsl_format_from_str!(u32, "0x{:08X}");
dsl_format_from_str!(u64, "0x{:016X}");

impl DSLFormat for F128 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "f128!({:?})", self.to_f64())
    }
}

impl DSLFormat for Complex32 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Complex { re, im } = self;
        write!(f, "c!(")?;
        re.fmt(f)?;
        write!(f, ", i ")?;
        im.fmt(f)?;
        write!(f, ")")
    }
}

impl DSLFormat for Complex64 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Complex { re, im } = self;
        write!(f, "c!(")?;
        re.fmt(f)?;
        write!(f, ", i ")?;
        im.fmt(f)?;
        write!(f, ")")
    }
}

impl DSLFormat for Complex128 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Complex { re, im } = self;
        write!(f, "c128!(")?;
        re.to_f64().fmt(f)?;
        write!(f, ", i ")?;
        im.to_f64().fmt(f)?;
        write!(f, ")")
    }
}

fn fmt_waveform<V: DSLFormat>(f: &mut fmt::Formatter, typ: &str,
                              wf: &Waveform<V>) -> fmt::Result {
    let Waveform {
        timestamp: Timestamp { seconds, fractions },
        time_increment,
        values,
        attributes,
    } = wf;
    write!(f, "v!({}wf [", typ)?;
    let mut is_first = true;
    for value in values {
        if is_first {
            is_first = false;
        } else {
            write!(f, ", ")?;
        }
        value.fmt(f)?;
    }
    write!(f, "] ts: (0x{:016X}, 0x{:016X}), incr: {}, attrs: ",
           seconds, fractions, time_increment)?;
    attributes.fmt(f)?;
    write!(f, ")")
}

impl DSLFormat for VariantData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            VariantData::Nothing => write!(f, "v!(nothing)"),
            VariantData::I8(n) => write!(f, "v!(i8 0x{:02X})", n),
            VariantData::I16(n) => write!(f, "v!(i16 0x{:04X})", n),
            VariantData::I32(n) => write!(f, "v!(i32 0x{:08X})", n),
            VariantData::I64(n) => write!(f, "v!(i64 0x{:016X})", n),
            VariantData::U8(n) => write!(f, "v!(u8 0x{:02X})", n),
            VariantData::U16(n) => write!(f, "v!(u16 0x{:04X})", n),
            VariantData::U32(n) => write!(f, "v!(u32 0x{:08X})", n),
            VariantData::U64(n) => write!(f, "v!(u64 0x{:016X})", n),
            VariantData::F32(n) => write!(f, "v!(f32 {})", n),
            VariantData::F64(n) => write!(f, "v!(f64 {})", n),
            VariantData::F128(n) => {
                write!(f, "v!(f128 ")?;
                n.fmt(f)?;
                write!(f, ")")
            },
            VariantData::Complex32(Complex32 { re, im })
                => write!(f, "v!(c32 {}, i {})", re, im),
            VariantData::Complex64(Complex64 { re, im })
                => write!(f, "v!(c64 {}, i {})", re, im),
            VariantData::Complex128(Complex { re, im }) => {
                write!(f, "v!(c128 ")?;
                re.fmt(f)?;
                write!(f, ", i ")?;
                im.fmt(f)?;
                write!(f, ")")
            },
            VariantData::Boolean(b) => write!(f, "v!(b {})", b),
            VariantData::String(s) => write!(f, "v!(str {:?})", s),
            VariantData::Path(Path { path_type, components }) => {
                match path_type {
                    PathType::Absolute
                        => write!(f, "v!(pth abs {})", components),
                    PathType::Relative
                        => write!(f, "v!(pth rel {})", components),
                    PathType::Invalid
                        => if components.0.len() == 0 {
                            write!(f, "v!(pth invalid)")
                        } else {
                            write!(f, "v!(pth Invalid {})", components)
                        },
                }
            },
            VariantData::Array1D(a) => write!(f, "v!(arr {:?})", a),
            VariantData::ArrayMDim(MDimArray { sizes, data })
                => write!(f, "v!(arr {:?} => {:?})", sizes, data),
            VariantData::Cluster(a) => write!(f, "v!(cluster {:?})", a),
            VariantData::Variant(v) => write!(f, "v!(var {:?})", v),
            VariantData::Timestamp(Timestamp { seconds, fractions })
                => write!(f, "v!(ts 0x{:016X}, 0x{:016X})",
                          seconds, fractions),
            VariantData::FixedPoint(FixedPoint {
                data,
                options: FixedPointOptions {
                    signed,
                    total_bits,
                    int_bits,
                },
            }) => write!(f, "v!(fp [sgn: {}, w: {}, int: {}] 0x{:016X})",
                         signed, total_bits, int_bits, data),
            VariantData::Class(Class {
                class_name: ClassName(names),
                versions,
                data,
            }) => {
                write!(f, "v!(cls [")?;
                let mut is_first = true;
                for ClassVersion { major, minor, fix, build } in versions {
                    if is_first {
                        is_first = false;
                    } else {
                        write!(f, ", ")?;
                    }
                    write!(f, "{} . {} . {} . {}", major, minor, fix, build)?;
                }
                write!(f, "] {} = [", names)?;
                for (i, d) in data.iter().enumerate() {
                    if i > 0 {
                        write!(f, ", ")?;
                    }
                    fmt::Display::fmt(&BinaryString(d.clone()), f)?;
                }
                write!(f, "])")
            }
            VariantData::F32Waveform(wf) => fmt_waveform(f, "f32", &**wf),
            VariantData::F64Waveform(wf) => fmt_waveform(f, "f64", &**wf),
            VariantData::F128Waveform(wf) => fmt_waveform(f, "f128", &**wf),
            VariantData::I8Waveform(wf) => fmt_waveform(f, "i8", &**wf),
            VariantData::I16Waveform(wf) => fmt_waveform(f, "i16", &**wf),
            VariantData::I32Waveform(wf) => fmt_waveform(f, "i32", &**wf),
            VariantData::I64Waveform(wf) => fmt_waveform(f, "i64", &**wf),
            VariantData::U8Waveform(wf) => fmt_waveform(f, "u8", &**wf),
            VariantData::U16Waveform(wf) => fmt_waveform(f, "u16", &**wf),
            VariantData::U32Waveform(wf) => fmt_waveform(f, "u32", &**wf),
            VariantData::U64Waveform(wf) => fmt_waveform(f, "u64", &**wf),
            VariantData::Complex32Waveform(wf) => fmt_waveform(f, "c32", &**wf),
            VariantData::Complex64Waveform(wf) => fmt_waveform(f, "c64", &**wf),
            VariantData::Complex128Waveform(wf) => fmt_waveform(f, "c128",
                                                                &**wf),
            VariantData::DigitalWaveform(dwf) => {
                let DigitalWaveform {
                    timestamp: Timestamp { seconds, fractions },
                    time_increment,
                    data,
                    unknown0,
                    attributes,
                } = &**dwf;
                write!(f, "v!(dwf ts: (0x{:016X}, 0x{:016X}), \
                                  incr: {}, \
                                  data: &[",
                       seconds, fractions, time_increment)?;
                let row_size = data.data.sizes[1];
                for (i, element) in data.data.data.iter().enumerate() {
                    if i % row_size == 0 {
                        if i == 0 {
                            write!(f, "&[")?;
                        } else {
                            write!(f, "], &[")?;
                        }
                    } else {
                        write!(f, ", ")?;
                    }
                    write!(f, "0x{:02X}", element)?;
                }
                write!(f, "]], transitions: [")?;
                let mut is_first = true;
                for tv in &data.transitions {
                    if is_first {
                        is_first = false;
                    } else {
                        write!(f, ", ")?;
                    }
                    tv.fmt(f)?;
                }
                write!(f, "], unknown0: 0x{:016X}, attrs: ", unknown0)?;
                attributes.fmt(f)?;
                write!(f, ")")
            },
            VariantData::DigitalData(data) => {
                write!(f, "v!(dd data: &[")?;
                let row_size = data.data.sizes[1];
                for (i, element) in data.data.data.iter().enumerate() {
                    if i % row_size == 0 {
                        if i == 0 {
                            write!(f, "&[")?;
                        } else {
                            write!(f, "], &[")?;
                        }
                    } else {
                        write!(f, ", ")?;
                    }
                    write!(f, "0x{:02X}", element)?;
                }
                write!(f, "]], transitions: [")?;
                let mut is_first = true;
                for tv in &data.transitions {
                    if is_first {
                        is_first = false;
                    } else {
                        write!(f, ", ")?;
                    }
                    tv.fmt(f)?;
                }
                write!(f, "])")
            },
        }
    }
}

impl DSLFormat for VariantTypeDescr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            VariantTypeDescr::Scalar { label } => {
                write!(f, "d!(scalar {})", label)
            },
            VariantTypeDescr::Enum { label, items } => {
                write!(f, "d!(enum {} => )", label)?;
                let mut first = true;
                for item in items {
                    if first {
                        first = false;
                    } else {
                        write!(f, ", ")?;
                    }
                    write!(f, "{}", item)?;
                }
                write!(f, ")")
            },
            VariantTypeDescr::Nothing => {
                write!(f, "d!(nothing)")
            },
            VariantTypeDescr::Typedef { unknown_at_start, typedef_name,
                                        unknown0, type_header } => {
                write!(f, "d!(typedef {} = {:?}, us: 0x{:08X}, u0: 0x{:04X})",
                       typedef_name, type_header, unknown_at_start, unknown0)
            },
            VariantTypeDescr::PrimitiveRefnum { subtype, label } => {
                write!(f, "d!({} {})",
                       match subtype {
                           VariantExtendedType::Occurrence => "occurrence",
                           VariantExtendedType::InvalidRefnum => "invrefnum",
                           _ => unreachable!(),
                       },
                       label)
            },
            VariantTypeDescr::Synchronization {
                subtype, root_type_idxes, label
            } => {
                write!(f, "d!({} {} => {:?}",
                       match subtype {
                           VariantExtendedType::Queue => "queue",
                           VariantExtendedType::Notifier => "notifier",
                           VariantExtendedType::DataValueReference => "dvr",
                           VariantExtendedType::UserEvent => "uevent",
                           _ => unreachable!(),
                       }, label, root_type_idxes)
            },
            VariantTypeDescr::FixedPoint {
                unknown0,
                options: FixedPointOptions { signed, total_bits, int_bits },
                sign_bit_mask,
                bit_mask,
                unknown1,
                unknown2,
                label,
            } => {
                write!(f, "d!(fp {} => [sgn: {}, w: 0x{:02X}, int: 0x{:02X}], \
                                 sbmsk: 0x{:016X},\
                                 bmsk: 0x{:016X}, \
                                 u0: 0x{:04X}, \
                                 u1: 0x{:016X}, \
                                 u2: 0x{:016X})",
                       label, signed, total_bits, int_bits,
                       sign_bit_mask,
                       bit_mask,
                       unknown0,
                       unknown1,
                       unknown2)
            },
            VariantTypeDescr::Array { num_dimensions, size_bounds, label,
                                      root_type_idx } => {
                if size_bounds.iter().all(|e| e == &UNBOUND_SIZE) {
                    write!(f, "d!(arr {} => 0x{:04X}, 0x{:04X})",
                           label, num_dimensions, root_type_idx)
                } else {
                    write!(f, "d!(arr {} => {:?}, 0x{:04X})",
                           label, size_bounds, root_type_idx)
                }
            },
            VariantTypeDescr::Cluster { root_type_idxes, label } => {
                write!(f, "d!(cluster {} => ", label)?;
                let mut first = true;
                for idx in root_type_idxes {
                    if first {
                        first = false;
                    } else {
                        write!(f, ", ")?;
                    }
                    write!(f, "{}", idx)?;
                }
                write!(f, ")")
            },
            VariantTypeDescr::String { size_bound, label } => {
                if size_bound == &UNBOUND_SIZE {
                    write!(f, "d!(str {})", label)
                } else {
                    write!(f, "d!(str {} => {})", label, size_bound)
                }
            },
            VariantTypeDescr::Timestamped { subtype, label } => {
                macro_rules! match_subtype {
                    ($f:expr, $label:expr, $subtype:expr => {
                        $($type:ident => $s:expr,)*
                    }) => (
                        match $subtype {
                            $(VariantTimestampedType::$type
                              => write!($f, concat!("d!(", $s, " {})"),
                                        $label),)*
                        }
                    );
                }
                match_subtype!(
                    f, label, subtype => {
                        I16Waveform => "i16wf",
                        F64Waveform => "f64wf",
                        F32Waveform => "f32wf",
                        Timestamp => "ts",
                        DigitalWaveform => "dwf",
                        DigitalData => "dd",
                        F128Waveform => "f128wf",
                        U8Waveform => "u8wf",
                        U16Waveform => "u16wf",
                        U32Waveform => "u32wf",
                        I8Waveform => "i8wf",
                        I32Waveform => "i32wf",
                        Complex32Waveform => "c32wf",
                        Complex64Waveform => "c64wf",
                        Complex128Waveform => "c128wf",
                        I64Waveform => "i64wf",
                        U64Waveform => "u64wf",
                    }
                )
            },
            VariantTypeDescr::Class {
                unknown0,
                class_name: ClassName(names),
                label,
            } => {
                write!(f, "d!(cls {} => {}, u0: 0x{:04X})",
                       label, names, unknown0)
            },
            VariantTypeDescr::Unknown37 { unknown0, unknown1, unknown2 } => {
                write!(f, "d!(unknown37 0x{:08X}, 0x{:04X}, {:?})",
                       unknown0, unknown1, unknown2)
            },
            VariantTypeDescr::InstrumentDescriptor {
                ivi_class, root_type_idxes, unknown0, unknown1, label,
            } => {
                write!(f, "d!(instrd {} => {}, {:?}, u0: 0x{:04X}, u1: ",
                       label, ivi_class, root_type_idxes, unknown0)?;
                unknown1.fmt(f)?;
                write!(f, ")")
            },
            VariantTypeDescr::DAQmxTask {
                type_name, root_type_idx, unknown0, class_name, unknown1, label
            } => {
                write!(f, "d!(daqmxtask {} => {}, {} => {}, u0: 0x{:04X}, u1: ",
                       label, type_name, class_name, root_type_idx, unknown0)?;
                unknown1.fmt(f)?;
                write!(f, ")")
            },
        }
    }
}

impl fmt::Display for DSLFormat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        DSLFormat::fmt(self, f)
    }
}

fn parse_string<T>(s: &[u8]) -> Result<T, String>
    where T: LVValue + fmt::Debug
{
    let slen = s.len();
    let mut f = io::Cursor::new(s);
    f.read_lv().or_else(|e| Err(format!("{:?}", e))).and_then(|r| {
        if f.position() == slen as u64 {
            Ok(r)
        } else {
            let mut rest = Vec::new();
            f.read_to_end(&mut rest).unwrap();
            Err(format!("Bytes remaining in buffer: {:?}. \
                         Result got so far: {:?}",
                        rest, r))
        }
    })
}

impl<R: DSLFormat> DSLFormat for Result<R, String> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Ok(d) => {
                write!(f, "Ok(")?;
                d.fmt(f)?;
                write!(f, ")")
            },
            Err(e) => {
                write!(f, "Err({:?})", e)
            },
        }
    }
}

fn push_halfbyte(is_first: &mut bool, tgt: &mut Vec<u8>, halfbyte: u8) {
    if *is_first {
        tgt.push(halfbyte << 4);
    } else {
        if let Some(last) = tgt.last_mut() {
            *last |= halfbyte;
        } else {
            unreachable!();
        }
    }
    *is_first = !*is_first;
}

fn from_hex(s: &str) -> Vec<u8> {
    let mut is_first = true;
    let mut ret = vec![];
    for c in s.bytes() {
        match c {
            b'0' ... b'9' => {
                push_halfbyte(&mut is_first, &mut ret, c - b'0');
            },
            b'A' ... b'F' => {
                push_halfbyte(&mut is_first, &mut ret, c - b'A' + 10);
            },
            b'a' ... b'f' => {
                push_halfbyte(&mut is_first, &mut ret, c - b'a' + 10);
            },
            _ => {},
        }
    }
    ret
}

macro_rules! test_variant_body {
    ($variant:expr, $data:expr) => {{
        let expected = $variant;
        let parsed = parse_string($data);
        assert_eq!(expected, parsed,
                   "\nexpected data does not match parsed one\n  \
                        expected: {},\n    \
                          parsed: {}.",
                   &expected as &DSLFormat, &parsed as &DSLFormat);
    }};
}

macro_rules! test_variant {
    ($func:ident, $variant:expr, $data:expr) => {
        #[test]
        fn $func() {
            test_variant_body!($variant, $data);
        }
    };
}

macro_rules! replace_expr {
    ($e:expr, $r:expr) => ($r)
}

macro_rules! count_exprs {
    ($($e:expr,)*) => (0u16 $(+ replace_expr!($e, 1))*);
}

macro_rules! var {
    ([$($type:expr),* $(,)*] => $data:expr, root $root_type_idx:expr,
     attributes [$($k:expr => $v:expr),* $(,)*],
     version $version:expr) => (
        Variant {
            version: $version,
            types: vec![
                $($type,)*
            ],
            data: $data,
            root_type_idx: $root_type_idx,
            attributes: Attributes(
                vec![$((BinaryString($k.to_vec()), $v)),*]),
        }
    );
    ([$($type:expr),* $(,)*] => $data:expr, root $root_type_idx:expr,
     attributes [$($k:expr => $v:expr),* $(,)*]) => (
        var!([$($type,)*] => $data,
             root $root_type_idx,
             attributes [$($k => $v),*],
             version 0x1500_8000)
    );
    ([$($type:expr),* $(,)*] => $data:expr, root $root_type_idx:expr) => (
        var!([$($type,)*] => $data,
             root $root_type_idx,
             attributes [])
    );
    ([$($type:expr),* $(,)*] => $data:expr) => (
        var!([$($type,)*] => $data, root count_exprs!($($type,)*) - 1)
    )
}

macro_rules! test_variant_positive {
    ($func:ident, [$($type:expr,)*], $data:expr, $root_type_idx:expr,
     $lvdata:expr) => {
        test_variant!($func, Ok(
                var!([$($type,)*] => $data, root $root_type_idx)), $lvdata);
    };
    ($func:ident, [$($type:expr),* $(,)*], $data:expr, $lvdata:expr) => {
        test_variant_positive!(
            $func, [$($type,)*], $data, count_exprs!($($type,)*) - 1,
            $lvdata);
    };
}

macro_rules! test_variant_positive_multiple {
    ($func:ident, [$($type:expr),* $(,)*], $data:expr,
     [$($version:expr => $lvdata:expr),* $(,)*], $root_type_idx:expr) => {
         #[test]
         fn $func() {
             let types = vec![$($type),*];
             let attributes = Attributes(vec![]);
             $(
                 {
                     let variant = Variant {
                         version: $version,
                         types: types.clone(),
                         data: $data,
                         root_type_idx: $root_type_idx,
                         attributes: attributes.clone(),
                     };
                     test_variant_body!(Ok(variant), $lvdata);
                 }
             )*
         }
    };
    ($func:ident, [$($type:expr),* $(,)*], $data:expr,
     [$($version:expr => $lvdata:expr),* $(,)*]) => {
         test_variant_positive_multiple!(
             $func, [$($type),*], $data,
             [$($version => $lvdata),*], count_exprs!($($type,)*) - 1);
    };
}

macro_rules! test_variant_positive_hex {
    ($func:ident, [$($type:expr,)*], $data:expr, $root_type_idx:expr,
     $hex:expr) => {
        test_variant_positive!($func, [$($type,)*], $data, $root_type_idx,
                               &from_hex($hex));
    };
    ($func:ident, [$($type:expr),* $(,)*], $data:expr, $hex:expr) => {
        test_variant_positive!($func, [$($type,)*], $data, &from_hex($hex));
    };
}

macro_rules! e {
    ($descr_length:expr, $header:expr, $next_descr_length:expr) => (
        VariantTypeElement {
            descr_length: $descr_length,
            header: $header,
            next_descr_length: $next_descr_length,
       }
    );
    ($descr_length:expr, $header:expr) => (
        VariantTypeElement {
            descr_length: $descr_length,
            header: $header,
            next_descr_length: 1,
        }
    )
}

macro_rules! h {
    ($flags:expr, $vtype:ident, $descr:expr) => (
        VariantTypeHeader {
            flags: $flags,
            vtype: VariantType::$vtype,
            descr: $descr,
        }
    )
}

macro_rules! d {
    (nothing) => (VariantTypeDescr::Nothing);
    (scalar $label:expr) => (VariantTypeDescr::Scalar {
        label: BinaryString($label.to_vec()),
    });
    (enum $label:expr => $($item:expr),* $(,)*) => (VariantTypeDescr::Enum {
        label: BinaryString($label.to_vec()),
        items: vec![$(BinaryString($item.to_vec())),*],
    });
    (str $label:expr => $size_bound:expr) => (VariantTypeDescr::String {
        size_bound: $size_bound,
        label: BinaryString($label.to_vec()),
    });
    (str $label:expr) => (d!(str $label => UNBOUND_SIZE));
    (ts/$subtype:ident $label:expr) => (
        VariantTypeDescr::Timestamped {
            subtype: VariantTimestampedType::$subtype,
            label: BinaryString($label.to_vec()),
        }
    );
    (f32wf $label:expr) => (d!(ts/F32Waveform $label));
    (f64wf $label:expr) => (d!(ts/F64Waveform $label));
    (f128wf $label:expr) => (d!(ts/F128Waveform $label));
    (ts $label:expr) => (d!(ts/Timestamp $label));
    (dwf $label:expr) => (d!(ts/DigitalWaveform $label));
    (dd $label:expr) => (d!(ts/DigitalData $label));
    (i8wf $label:expr) => (d!(ts/I8Waveform $label));
    (i16wf $label:expr) => (d!(ts/I16Waveform $label));
    (i32wf $label:expr) => (d!(ts/I32Waveform $label));
    (i64wf $label:expr) => (d!(ts/I64Waveform $label));
    (u8wf $label:expr) => (d!(ts/U8Waveform $label));
    (u16wf $label:expr) => (d!(ts/U16Waveform $label));
    (u32wf $label:expr) => (d!(ts/U32Waveform $label));
    (u64wf $label:expr) => (d!(ts/U64Waveform $label));
    (c32wf $label:expr) => (d!(ts/Complex32Waveform $label));
    (c64wf $label:expr) => (d!(ts/Complex64Waveform $label));
    (c128wf $label:expr) => (d!(ts/Complex128Waveform $label));
    (typedef [$($label:expr),*] = $type_header:expr,
     us: $unknown_at_start:expr, u0: $unknown0:expr) => (
         VariantTypeDescr::Typedef {
             unknown_at_start: $unknown_at_start,
             typedef_name: BinaryStrings(
                 vec![$(BinaryString($label.to_vec()),)*]),
             unknown0: $unknown0,
             type_header: Box::new($type_header),
         }
    );
    (invrefnum $label:expr) => (
        VariantTypeDescr::PrimitiveRefnum {
            subtype: VariantExtendedType::InvalidRefnum,
            label: BinaryString($label.to_vec()),
        }
    );
    (occurrence $label:expr) => (
        VariantTypeDescr::PrimitiveRefnum {
            subtype: VariantExtendedType::Occurrence,
            label: BinaryString($label.to_vec()),
        }
    );
    (queue $label:expr => [$($root_type_idx:expr),*]) => (
        VariantTypeDescr::Synchronization {
            subtype: VariantExtendedType::Queue,
            root_type_idxes: vec![$($root_type_idx,)*],
            label: BinaryString($label.to_vec()),
        }
    );
    (notifier $label:expr => [$($root_type_idx:expr),*]) => (
        VariantTypeDescr::Synchronization {
            subtype: VariantExtendedType::Notifier,
            root_type_idxes: vec![$($root_type_idx,)*],
            label: BinaryString($label.to_vec()),
        }
    );
    (dvr $label:expr => [$($root_type_idx:expr),*]) => (
        VariantTypeDescr::Synchronization {
            subtype: VariantExtendedType::DataValueReference,
            root_type_idxes: vec![$($root_type_idx,)*],
            label: BinaryString($label.to_vec()),
        }
    );
    (uevent $label:expr => [$($root_type_idx:expr),*]) => (
        VariantTypeDescr::Synchronization {
            subtype: VariantExtendedType::UserEvent,
            root_type_idxes: vec![$($root_type_idx,)*],
            label: BinaryString($label.to_vec()),
        }
    );
    (arr $label:expr => $num_dimensions:expr, $root_type_idx:expr) => (
        VariantTypeDescr::Array {
            num_dimensions: $num_dimensions,
            size_bounds: vec![UNBOUND_SIZE; $num_dimensions],
            root_type_idx: $root_type_idx,
            label: BinaryString($label.to_vec()),
        }
    );
    (arr $label:expr => [$($sb:expr),*], $root_type_idx:expr) => (
        VariantTypeDescr::Array {
            num_dimensions: count_exprs!($($sb,)*),
            size_bounds: vec![$($sb,)*],
            root_type_idx: $root_type_idx,
            label: BinaryString($label.to_vec()),
        }
    );
    (fp $label:expr => [sgn: $signed:expr, w: $total_bits:expr,
                        int: $int_bits:expr],
                       sbmsk: $sign_bit_mask:expr,
                       bmsk: $bit_mask:expr,
                       u0: $unknown0:expr,
                       u1: $unknown1:expr,
                       u2: $unknown2:expr) => (
        VariantTypeDescr::FixedPoint {
            unknown0: $unknown0,
            options: FixedPointOptions {
                signed: $signed,
                total_bits: $total_bits,
                int_bits: $int_bits,
            },
            sign_bit_mask: $sign_bit_mask,
            bit_mask: $bit_mask,
            unknown1: $unknown1,
            unknown2: $unknown2,
            label: BinaryString($label.to_vec()),
        }
    );
    (cluster $label:expr => $($root_type_idx:expr),*) => (
        VariantTypeDescr::Cluster {
            root_type_idxes: vec![$($root_type_idx,)*],
            label: BinaryString($label.to_vec()),
        }
    );
    (cls $label:expr => [$($cname:expr),* $(,)*], u0: $unknown0:expr) => (
        VariantTypeDescr::Class {
            unknown0: $unknown0,
            class_name: ClassName(BinaryStrings(
                vec![$(BinaryString($cname.to_vec()),)*])),
            label: BinaryString($label.to_vec()),
        }
    );
    (unknown37 $unknown0:expr, $unknown1:expr, $unknown2:expr) => (
        VariantTypeDescr::Unknown37 {
            unknown0: $unknown0,
            unknown1: $unknown1,
            unknown2: Box::new($unknown2),
        }
    );
    (instrd $label:expr => $ivi_class:expr, [$($root_type_idx:expr),*],
            u0: $unknown0:expr, u1: $unknown1:expr) => (
        VariantTypeDescr::InstrumentDescriptor {
            label: BinaryString($label.to_vec()),
            ivi_class: BinaryString($ivi_class.to_vec()),
            root_type_idxes: vec![$($root_type_idx,)*],
            unknown0: $unknown0,
            unknown1: Box::new($unknown1),
        }
    );
    (daqmxtask $label:expr => $type_name:expr, $class_name:expr
               => $root_type_idx:expr,
               u0: $unknown0:expr, u1: $unknown1:expr) => (
        VariantTypeDescr::DAQmxTask {
            label: BinaryString($label.to_vec()),
            type_name: BinaryString($type_name.to_vec()),
            class_name: BinaryString($class_name.to_vec()),
            root_type_idx: $root_type_idx,
            unknown0: $unknown0,
            unknown1: Box::new($unknown1),
        }
    );
}

macro_rules! c {
    ($re:expr, i $im:expr) => (
        Complex { re: $re, im: $im }
    );
}

macro_rules! f128 {
    ($v:ident) => (F128::new_from_f64(f64::$v));
    ($v:expr) => (F128::new_from_f64($v));
}

macro_rules! c128 {
    ($re:expr, i $im:expr) => (
        Complex128 { re: f128!($re), im: f128!($im) }
    );
}

macro_rules! v {
    (nothing) => (VariantData::Nothing);
    (i8 $v:expr) => (VariantData::I8($v));
    (i16 $v:expr) => (VariantData::I16($v));
    (i32 $v:expr) => (VariantData::I32($v));
    (i64 $v:expr) => (VariantData::I64($v));
    (u8 $v:expr) => (VariantData::U8($v));
    (u16 $v:expr) => (VariantData::U16($v));
    (u32 $v:expr) => (VariantData::U32($v));
    (u64 $v:expr) => (VariantData::U64($v));
    (f32 $v:ident) => (VariantData::F32(f32::$v));
    (f64 $v:ident) => (VariantData::F64(f64::$v));
    (f128 $v:ident) => (VariantData::F128(f128!($v)));
    (f32 $v:expr) => (VariantData::F32($v));
    (f64 $v:expr) => (VariantData::F64($v));
    (f128 $v:expr) => (VariantData::F128(f128!($v)));
    (c32 $re:expr, i $im:expr) => (VariantData::Complex32(c!($re, i $im)));
    (c64 $re:expr, i $im:expr) => (VariantData::Complex64(c!($re, i $im)));
    (c128 $re:expr, i $im:expr) => (VariantData::Complex128(c128!($re, i $im)));
    (b $v:expr) => (VariantData::Boolean($v));
    (fp [sgn: $signed:expr, w: $total_bits:expr, int: $int_bits:expr]
        $data:expr)
        => (VariantData::FixedPoint(FixedPoint {
            data: $data,
            options: FixedPointOptions {
                signed: $signed,
                total_bits: $total_bits,
                int_bits: $int_bits,
            },
        }));
    (var $v:expr) => (VariantData::Variant(Box::new($v)));
    (str $v:expr) => (VariantData::String(BinaryString($v.to_vec())));
    (ts $seconds:expr, $fractions:expr)
        => (VariantData::Timestamp(Timestamp {
            seconds: $seconds,
            fractions: $fractions,
        }));
    (pth [$($s:expr),*]) => (
        v!(pth Absolute [$($s,)*])
    );
    (pth rel [$($s:expr),*]) => (
        v!(pth Relative [$($s,)*])
    );
    (pth abs [$($s:expr),*]) => (
        v!(pth Absolute [$($s,)*])
    );
    (pth invalid) => (
        v!(pth Invalid [])
    );
    (pth $t:ident [$($s:expr),* $(,)*]) => (
        VariantData::Path(Path {
            path_type: PathType::$t,
            components: BinaryStrings(vec![$(BinaryString($s.to_vec()),)*])
        })
    );
    (arr [$($s:expr),*] => [$($v:expr),*]) => (
        VariantData::ArrayMDim(MDimArray {
            sizes: vec![$($s,)*],
            data: vec![$($v,)*],
        }));
    (arr [$($s:expr),+] => [$v:expr;]) => (
        VariantData::ArrayMDim(MDimArray {
            sizes: vec![$($s,)*],
            data: vec![$v; 1$(*$s)+],
        }));
    (arr [$($s:expr),*] => [$v:expr; $r:expr]) => (
        VariantData::ArrayMDim(MDimArray {
            sizes: vec![$($s,)*],
            data: vec![$v; $r],
        }));
    (arr [$v:expr; $r:expr]) => (VariantData::Array1D(vec![$v; $r]));
    (arr [$($v:expr),* $(,)*]) => (VariantData::Array1D(vec![$($v,)*]));
    (cluster [$($v:expr),* $(,)*]) => (VariantData::Cluster(vec![$($v,)*]));
    (cls $major:tt.$minor:tt.$fix:tt.$build:tt [$($c:expr),* $(,)*]
         = $data:expr) => (
        VariantData::Class(Class {
            class_name: ClassName(BinaryStrings(vec![
                $(BinaryString($c.to_vec()),)*])),
            versions: vec![ClassVersion {
                major: $major,
                minor: $minor,
                fix: $fix,
                build: $build,
            }],
            data: vec![$data.to_vec()],
        }));
    (cls [$($major:tt.$minor:tt.$fix:tt.$build:tt),*] [$($c:expr),* $(,)*]
         = [$($data:expr),* $(,)*]) => (
        VariantData::Class(Class {
            class_name: ClassName(BinaryStrings(vec![
                $(BinaryString($c.to_vec()),)*])),
            versions: vec![$(ClassVersion {
                major: $major,
                minor: $minor,
                fix: $fix,
                build: $build,
            },)*],
            data: vec![$($data.to_vec()),*],
        }));
    (wf/$wf:ident [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                                  incr: $time_increment:expr,
                                  attrs: $attributes:expr) => (
        VariantData::$wf(Box::new(Waveform {
            timestamp: Timestamp {
                seconds: $seconds,
                fractions: $fractions,
            },
            time_increment: $time_increment,
            values: vec![$($v,)*],
            attributes: $attributes,
        }))
    );
    (dwf ts: ($seconds:expr, $fractions:expr),
         incr: $time_increment:expr,
         data: $data:expr,
         transitions: [$($tv:expr),* $(,)*],
         unknown0: $unknown0:expr,
         attrs: $attributes:expr) => (
        VariantData::DigitalWaveform(Box::new(DigitalWaveform {
            timestamp: Timestamp {
                seconds: $seconds,
                fractions: $fractions,
            },
            time_increment: $time_increment,
            data: DigitalData {
                data: MDimArray::new_from_nested_2d_array($data).unwrap(),
                transitions: vec![$($tv,)*],
            },
            unknown0: $unknown0,
            attributes: $attributes,
        }))
    );
    (dd data: $data:expr,
        transitions: [$($tv:expr),* $(,)*]) => (
        VariantData::DigitalData(DigitalData {
            data: MDimArray::new_from_nested_2d_array($data).unwrap(),
            transitions: vec![$($tv,)*],
        })
    );
    (f32wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/F32Waveform [$($v),*], ts: ($seconds, $fractions),
                                     incr: $time_increment,
                                     attrs: $attributes)
    );
    (f64wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/F64Waveform [$($v),*], ts: ($seconds, $fractions),
                                     incr: $time_increment,
                                     attrs: $attributes)
    );
    (f128wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                            incr: $time_increment:expr,
                            attrs: $attributes:expr) => (
        v!(wf/F128Waveform [$($v),*], ts: ($seconds, $fractions),
                                      incr: $time_increment,
                                      attrs: $attributes)
    );
    (i8wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                          incr: $time_increment:expr,
                          attrs: $attributes:expr) => (
        v!(wf/I8Waveform [$($v),*], ts: ($seconds, $fractions),
                                    incr: $time_increment,
                                    attrs: $attributes)
    );
    (i16wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/I16Waveform [$($v),*], ts: ($seconds, $fractions),
                                     incr: $time_increment,
                                     attrs: $attributes)
    );
    (i32wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/I32Waveform [$($v),*], ts: ($seconds, $fractions),
                                     incr: $time_increment,
                                     attrs: $attributes)
    );
    (i64wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/I64Waveform [$($v),*], ts: ($seconds, $fractions),
                                     incr: $time_increment,
                                     attrs: $attributes)
    );
    (u8wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                          incr: $time_increment:expr,
                          attrs: $attributes:expr) => (
        v!(wf/U8Waveform [$($v),*], ts: ($seconds, $fractions),
                                    incr: $time_increment,
                                    attrs: $attributes)
    );
    (u16wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/U16Waveform [$($v),*], ts: ($seconds, $fractions),
                                     incr: $time_increment,
                                     attrs: $attributes)
    );
    (u32wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/U32Waveform [$($v),*], ts: ($seconds, $fractions),
                                     incr: $time_increment,
                                     attrs: $attributes)
    );
    (u64wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/U64Waveform [$($v),*], ts: ($seconds, $fractions),
                                     incr: $time_increment,
                                     attrs: $attributes)
    );
    (c32wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/Complex32Waveform [$($v),*], ts: ($seconds, $fractions),
                                           incr: $time_increment,
                                           attrs: $attributes)
    );
    (c64wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                           incr: $time_increment:expr,
                           attrs: $attributes:expr) => (
        v!(wf/Complex64Waveform [$($v),*], ts: ($seconds, $fractions),
                                           incr: $time_increment,
                                           attrs: $attributes)
    );
    (c128wf [$($v:expr),*], ts: ($seconds:expr, $fractions:expr),
                            incr: $time_increment:expr,
                            attrs: $attributes:expr) => (
        v!(wf/Complex128Waveform [$($v),*], ts: ($seconds, $fractions),
                                            incr: $time_increment,
                                            attrs: $attributes)
    );
}

test_variant_positive_hex!(
    test_variant_named_i32,
    [e!(0x1D, h!(0x40, I32, d!(scalar b"max_samples_to_average")))],
    v!(i32 0x80CD_7FA5u32 as i32),
    "
        1500 8000 0000 0001 001D 4003 0016 6D61 785F 7361 6D70 6C65 735F
        746F 5F61 7665 7261 6765 0000 0100 0080 CD7F A500 0000 00
    ");
test_variant_positive_hex!(
    test_variant_named_u32,
    [e!(0x1D, h!(0x40, U32, d!(scalar b"max_samples_to_average")))],
    v!(u32 0x80CD_7FA5u32),
    "
        1500 8000 0000 0001 001D 4007 0016 6D61 785F 7361 6D70 6C65 735F
        746F 5F61 7665 7261 6765 0000 0100 0080 CD7F A500 0000 00
    ");

test_variant_positive_hex!(
    test_variant_typedefed_i32,
    [
        e!(0x46, h!(0x00, Typedef, d!(
            typedef [b"test_variant_i32_typedef.ctl"] = h!(
                0x40, I32, d!(scalar b"max_samples_to_average")),
            us: 0x0000_0000,
            u0: 0x0021))),
    ],
    v!(i32 0x80CD_7FA5u32 as i32),
    "
        1500 8000 0000 0001 0046 00F1 0000 0000 0000 0001 1C74 6573 745F
        7661 7269 616E 745F 6933 325F 7479 7065 6465 662E 6374 6C00 2140
        0300 166D 6178 5F73 616D 706C 6573 5F74 6F5F 6176 6572 6167 6500
        0001 0000 80CD 7FA5 0000 0000
    ");

test_variant_positive_hex!(
    test_variant_unnamed_boolean,
    [e!(0x04, h!(0x00, Boolean, d!(scalar b"")))],
    v!(b true),
    "
        1500 8000 0000 0001 0004 0021 0001 0000 0100 0000 00
    ");

test_variant_positive_hex!(
    test_variant_named_boolean,
    [e!(0x0A, h!(0x40, Boolean, d!(scalar b"test")))],
    v!(b true),
    "
        1500 8000 0000 0001 000A 4021 0474 6573 7400 0001 0000 0100 0000 00
    ");

test_variant_positive_hex!(
    test_variant_typedefed_boolean,
    [
        e!(0x51, h!(0x00, Typedef, d!(
            typedef [b"lvpad_controller.lvclass",
                     b"host_ctl_reset_report.ctl"]
                    = h!(0x40, Boolean, d!(scalar b"reset_report")),
                    us: 0x0000_0000,
                    u0: 0x0016))),
    ],
    v!(b false),
    "
        1500 8000 0000 0001 0051 00F1 0000 0000 0000 0002 186C 7670 6164
        5F63 6F6E 7472 6F6C 6C65 722E 6C76 636C 6173 7319 686F 7374 5F63
        746C 5F72 6573 6574 5F72 6570 6F72 742E 6374 6C00 1640 210C 7265
        7365 745F 7265 706F 7274 0000 0100 0000 0000 0000
    ");

test_variant_positive_hex!(
    test_variant_typedefed_boolean_queue,
    [
        e!(0x51, h!(0x00, Typedef, d!(
            typedef [b"lvpad_controller.lvclass",
                     b"host_ctl_reset_report.ctl"]
                    = h!(0x40, Boolean, d!(scalar b"reset_report")),
                    us: 0x0000_0000,
                    u0: 0x0016)), 0x14),
        e!(0x14, h!(0x40, Extended, d!(queue b"queue out" => [0]))),
    ],
    v!(u32 0xAF50_0024),
    1,
    "
        1500 8000 0000 0002 0051 00F1 0000 0000 0000 0002 186C 7670 6164
        5F63 6F6E 7472 6F6C 6C65 722E 6C76 636C 6173 7319 686F 7374 5F63
        746C 5F72 6573 6574 5F72 6570 6F72 742E 6374 6C00 1640 210C 7265
        7365 745F 7265 706F 7274 0000 1440 7000 1200 0100 0009 7175 6575
        6520 6F75 7400 0100 01AF 5000 2400 0000 00
    ");

test_variant_positive_hex!(
    test_variant_typedefed_typedefed_boolean_queue,
    [
        e!(0x51, h!(0x00, Typedef, d!(
            typedef [b"lvpad_controller.lvclass",
                     b"host_ctl_reset_report.ctl"]
                    = h!(0x40, Boolean, d!(scalar b"reset_report")),
                    us: 0x0000_0000,
                    u0: 0x0016)), 0x63),
        e!(0x63, h!(0x00, Typedef, d!(
            typedef [b"lvpad_controller.lvclass",
                     b"host_ctl_reset_report_queue.ctl"]
                    = h!(0x40, Extended, d!(
                        queue b"reset_report_queue" => [0])),
                    us: 0x0000_0000,
                    u0: 0x0022))),
    ],
    v!(u32 0),
    1,
    "
        1500 8000 0000 0002 0051 00F1 0000 0000 0000 0002 186C 7670 6164
        5F63 6F6E 7472 6F6C 6C65 722E 6C76 636C 6173 7319 686F 7374 5F63
        746C 5F72 6573 6574 5F72 6570 6F72 742E 6374 6C00 1640 210C 7265
        7365 745F 7265 706F 7274 0000 6300 F100 0000 0000 0000 0218 6C76
        7061 645F 636F 6E74 726F 6C6C 6572 2E6C 7663 6C61 7373 1F68 6F73
        745F 6374 6C5F 7265 7365 745F 7265 706F 7274 5F71 7565 7565 2E63
        746C 0022 4070 0012 0001 0000 1272 6573 6574 5F72 6570 6F72 745F
        7175 6575 6500 0001 0001 0000 0000 0000 0000
    ");

test_variant_positive_hex!(
    test_variant_named_named_f64_zero,
    [e!(0x09, h!(0x40, F64, d!(scalar b"AO0")))],
    v!(f64 0.0),
    "
        1500 8000 0000 0001 0009 400A 0003 414F 3000 0100 0000 0000 0000
        0000 0000 0000 00
    ");

test_variant_positive_hex!(
    test_variant_named_named_f64_nan,
    [e!(0x09, h!(0x40, F64, d!(scalar b"AO0")))],
    v!(f64 NAN),
    "
        1500 8000 0000 0001 0009 400A 0003 414F 3000 0100 007F FFFF FFFF
        FFFF FF00 0000 00
    ");

test_variant_positive_hex!(
    test_variant_named_named_f64_array,
    [
        e!(0x09, h!(0x40, F64, d!(scalar b"AO0")), 0x10),
        e!(0x10, h!(0x40, Array, d!(arr b"ao" => 0x0001, 0x0000))),
    ],
    v!(arr [v!(f64 0.0); 8]),
    1,
    "
        1500 8000 0000 0002 0009 400A 0003 414F 3000 1040 4000 01FF FFFF
        FF00 0002 616F 0000 0100 0100 0000 0800 0000 0000 0000 0000 0000
        0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
        0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
        0000 0000 00
    ");

test_variant_positive_hex!(
    test_variant_named_u64,
    [e!(0x09, h!(0x40, U64, d!(scalar b"AO0")))],
    v!(u64 0),
    "
        1500 8000 0000 0001 0009 4008 0003 414F 3000 0100 0000 0000 0000
        0000 0000 0000 00
    ");

test_variant_positive!(
    test_variant_named_u64_2,
    [e!(0x1D, h!(0x40, U64, d!(scalar b"max_samples_to_average")))],
    v!(u64 0x80CD_7FA5),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x1D\x40\x08\x00\
      \x16max_samples_to_average\x00\x00\x01\
      \x00\x00\x00\x00\x00\x00\x80\xCD\x7F\xA5\x00\x00\x00\x00");

test_variant_positive_hex!(
    test_variant_named_i64,
    [e!(0x1D, h!(0x40, I64, d!(scalar b"max_samples_to_average")))],
    v!(i64 0x80CD_7FA5),
    "
        1500 8000 0000 0001 001D 4004 0016 6D61 785F 7361 6D70 6C65 735F
        746F 5F61 7665 7261 6765 0000 0100 0000 0000 0080 CD7F A500 0000
        00
    ");

test_variant_positive_multiple!(
    test_variant_named_i16,
    [e!(0x1D, h!(0x40, I16, d!(scalar b"max_samples_to_average")))],
    v!(i16 0x7FA5),
    [
        0x1500_8000
        => b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x1D\x40\x02\x00\
             \x16max_samples_to_average\x00\x00\x01\x00\x00\
             \x7F\xA5\x00\x00\x00\x00",
        0x1800_8000
        => b"\x18\x00\x80\x00\
             \x00\x00\x00\x01\
               \x00\x1D\
                 \x40\x02\
                 \x00\x16max_samples_to_average\
               \x00\x00\x01\
             \x00\x00\
             \x7F\xA5\
             \x00\x00\x00\x00"
    ]);

test_variant_positive!(
    test_variant_named_i8,
    [e!(0x1D, h!(0x40, I8, d!(scalar b"max_samples_to_average")))],
    v!(i8 0xA5u8 as i8),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x1D\x40\x01\x00\
      \x16max_samples_to_average\x00\x00\x01\x00\x00\xA5\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_u8,
    [e!(0x1D, h!(0x40, U8, d!(scalar b"max_samples_to_average")))],
    v!(u8 0xA5),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x1D\x40\x05\x00\
      \x16max_samples_to_average\x00\x00\x01\x00\x00\xA5\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_u16,
    [e!(0x1D, h!(0x40, U16, d!(scalar b"max_samples_to_average")))],
    v!(u16 0x7FA5),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x1D\x40\x06\x00\
      \x16max_samples_to_average\x00\x00\x01\x00\x00\
      \x7F\xA5\x00\x00\x00\x00");

test_variant_positive_hex!(
    test_variant_named_named_u64_array,
    [
        e!(0x09, h!(0x40, U64, d!(scalar b"AO0")), 0x10),
        e!(0x10, h!(0x40, Array, d!(arr b"ao" => 0x0001, 0x0000))),
    ],
    v!(arr [v!(u64 0xAA55_FF5A_00A5_7733)]),
    "
        1500 8000 0000 0002 0009 4008 0003 414F 3000 1040 4000 01FF FFFF
        FF00 0002 616F 0000 0100 0100 0000 01AA 55FF 5A00 A577 3300 0000
        00
    ");

test_variant_positive_hex!(
    test_variant_named_named_u64_array_2,
    [
        e!(0x11, h!(0x40, U64, d!(scalar b"fifo_entry")), 0x1A),
        e!(0x1A, h!(0x40, Array, d!(arr b"fifo_entries" => 0x0001, 0x0000))),
    ],
    v!(arr [v!(u64 0xAA55_FF5A_00A5_7733)]),
    1,
    "
        1500 8000 0000 0002 0011 4008 000A 6669 666F 5F65 6E74 7279 0000
        1A40 4000 01FF FFFF FF00 000C 6669 666F 5F65 6E74 7269 6573 0000
        0100 0100 0000 01AA 55FF 5A00 A577 3300 0000 00
    ");

test_variant_positive_hex!(
    test_variant_variant_nothing,
    [e!(0x08, h!(0x40, Variant, d!(scalar b"foo")))],
    v!(var var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))] => v!(nothing))),
    "
        1500 8000 0000 0001 0008 4053 0366 6F6F 0001 0000 1500 8000 0000
        0001 0004 0000 0001 0000 0000 0000 0000 0000
    ");

test_variant_positive_hex!(
    test_variant_named_named_boolean_array,
    [
        e!(0x0E, h!(0x40, Boolean, d!(scalar b"local_oe")), 0x16),
        e!(0x16, h!(0x40, Array, d!(arr b"local_oe" => 0x0001, 0x0000))),
    ],
    v!(arr [v!(b true)]),
    "
        1500 8000 0000 0002 000E 4021 086C 6F63 616C 5F6F 6500 0016 4040
        0001 FFFF FFFF 0000 086C 6F63 616C 5F6F 6500 0001 0001 0000 0001
        0100 0000 00
    ");

test_variant_positive_hex!(
    test_variant_private_u32_cluster,
    [
        e!(0x13, h!(0x40, U32, d!(scalar b"irdw_timeout")), 0x11),
        e!(0x11, h!(0x40, U32, d!(scalar b"wrw_timeout")), 0x11),
        e!(0x11, h!(0x40, U32, d!(scalar b"rdw_timeout")), 0x11),
        e!(0x11, h!(0x40, U32, d!(scalar b"pdw_timeout")), 0x13),
        e!(0x13, h!(0x40, U32, d!(scalar b"idle_timeout")), 0x0F),
        e!(0x0F, h!(0x40, U32, d!(scalar b"wr_cycles")), 0x4F),
        e!(0x4F, h!(0x00, Typedef, d!(
            typedef [b"FM33256-G_host.lvclass", b"ic_seq_config.ctl"]
                    = h!(0x40, Cluster, d!(
                        cluster b"config" => 0, 1, 2, 3, 4, 5)),
                    us: 0xD824_9F1B,
                    u0: 0x001E))),
    ],
    v!(cluster [
       v!(u32 0x0000_012c),
       v!(u32 0x0000_03e8),
       v!(u32 0x0000_0064),
       v!(u32 0x0000_0001),
       v!(u32 0x0000_2710),
       v!(u32 0x0000_000a),
    ]),
    "
        1500 8000 0000 0007 0013 4007 000C 6972 6477 5F74 696D 656F 7574
        0000 1140 0700 0B77 7277 5F74 696D 656F 7574 0011 4007 000B 7264
        775F 7469 6D65 6F75 7400 1140 0700 0B70 6477 5F74 696D 656F 7574
        0013 4007 000C 6964 6C65 5F74 696D 656F 7574 0000 0F40 0700 0977
        725F 6379 636C 6573 004F 00F1 D824 9F1B 0000 0002 1646 4D33 3332
        3536 2D47 5F68 6F73 742E 6C76 636C 6173 7311 6963 5F73 6571 5F63
        6F6E 6669 672E 6374 6C00 1E40 5000 0600 0000 0100 0200 0300 0400
        0506 636F 6E66 6967 0000 0100 0600 0001 2C00 0003 E800 0000 6400
        0000 0100 0027 1000 0000 0A00 0000 00
    ");

test_variant_positive!(
    test_variant_nothing,
    [e!(0x04, h!(0x00, Nothing, d!(nothing)))],
    v!(nothing),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x04\
          \x00\x00\
        \x00\x01\
      \x00\x00\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_f64,
    [e!(0x1D, h!(0x40, F64, d!(scalar b"max_samples_to_average")))],
    v!(f64 0xA5 as f64),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x1D\x40\x0A\x00\
      \x16max_samples_to_average\x00\x00\x01\x00\x00\
      \x40\x64\xA0\x00\x00\x00\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_f32,
    [e!(0x1D, h!(0x40, F32, d!(scalar b"max_samples_to_average")))],
    v!(f32 0xA5 as f32),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x1D\x40\x09\x00\
      \x16max_samples_to_average\x00\x00\x01\x00\x00\
      \x43\x25\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_f32_nan,
    [e!(0x1D, h!(0x40, F32, d!(scalar b"max_samples_to_average")))],
    v!(f32 NAN),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x1D\x40\x09\x00\
      \x16max_samples_to_average\x00\x00\x01\x00\x00\
      \x7F\xFF\xFF\xFF\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_f64_nan,
    [e!(0x05, h!(0x00, F64, d!(scalar b"")))],
    v!(f64 NAN),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0A\x00\
      \x00\x01\x00\x00\
      \x7F\xFF\xFF\xFF\xE0\x00\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_f64_inf,
    [e!(0x05, h!(0x00, F64, d!(scalar b"")))],
    v!(f64 INFINITY),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0A\x00\
      \x00\x01\x00\x00\
      \x7F\xF0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_f32_inf,
    [e!(0x05, h!(0x00, F32, d!(scalar b"")))],
    v!(f32 INFINITY),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x09\x00\
      \x00\x01\x00\x00\x7F\x80\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_f32_ninf,
    [e!(0x05, h!(0x00, F32, d!(scalar b"")))],
    v!(f32 NEG_INFINITY),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x09\x00\
      \x00\x01\x00\x00\xFF\x80\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_f64_ninf,
    [e!(0x05, h!(0x00, F64, d!(scalar b"")))],
    v!(f64 NEG_INFINITY),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0A\x00\
      \x00\x01\x00\x00\
      \xFF\xF0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_fxp_signed_64_32_5d5,
    [
        e!(0x3C, h!(0x00, FixedPoint, d!(
            fp b"" => [sgn: true, w: 0x40, int: 0x20],
               sbmsk: 0x8000_0000_0000_0000,
               bmsk: 0x7FFF_FFFF_FFFF_FFFF,
               u0: 0x0000_0351,
               u1: 0x0000_0001_FFFF_FFE1,
               u2: 0x0000_0000_0000_0001))),
    ],
    v!(fp [sgn: true, w: 0x40, int: 0x20] 0x0000_0005_8000_0000),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x3C\x00\x5F\
      \x03\x51\
      \x00\x40\
      \x00\x00\x00\x20\
      \x00\x01\
      \x00\x40\
      \x00\x00\x00\x20\
      \x80\x00\x00\x00\x00\x00\x00\x00\
      \x00\x01\
      \x00\x40\
      \x00\x00\x00\x20\
      \x7F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\
      \x00\x00\x00\x01\xFF\xFF\xFF\xE1\
      \x00\x00\x00\x00\x00\x00\x00\x01\
      \x00\
      \x01\x00\x00\
      \x00\x00\x00\x05\x80\x00\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_fxp_signed_64_32_n5d5_named,
    [
        e!(0x44, h!(0x40, FixedPoint, d!(
            fp b"number" => [sgn: true, w: 0x40, int: 0x20],
               sbmsk: 0x8000_0000_0000_0000,
               bmsk: 0x7FFF_FFFF_FFFF_FFFF,
               u0: 0x0000_0351,
               u1: 0x0000_0001_FFFF_FFE1,
               u2: 0x0000_0000_0000_0001))),
    ],
    v!(fp [sgn: true, w: 0x40, int: 0x20] 0xFFFF_FFFA_8000_0000),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x44\x40\x5F\
      \x03\x51\
      \x00\x40\
      \x00\x00\x00\x20\
      \x00\x01\
      \x00\x40\
      \x00\x00\x00\x20\
      \x80\x00\x00\x00\x00\x00\x00\x00\
      \x00\x01\
      \x00\x40\
      \x00\x00\x00\x20\
      \x7F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\
      \x00\x00\x00\x01\xFF\xFF\xFF\xE1\
      \x00\x00\x00\x00\x00\x00\x00\x01\
      \x06number\
      \x00\x00\x01\
      \x00\x00\
      \xFF\xFF\xFF\xFA\x80\x00\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_fxp_unsigned_64_32_5d5_named,
    [
        e!(0x44, h!(0x40, FixedPoint, d!(
            fp b"number" => [sgn: false, w: 0x40, int: 0x20],
               sbmsk: 0x0000_0000_0000_0000,
               bmsk: 0xFFFF_FFFF_FFFF_FFFF,
               u0: 0x0000_0311,
               u1: 0x0000_0001_FFFF_FFE1,
               u2: 0x0000_0000_0000_0001))),
    ],
    v!(fp [sgn: false, w: 0x40, int: 0x20] 0x0000_0005_8000_0000),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x44\x40\x5F\
      \x03\x11\
      \x00\x40\
      \x00\x00\x00\x20\
      \x00\x00\
      \x00\x40\
      \x00\x00\x00\x20\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\
      \x00\x40\
      \x00\x00\x00\x20\
      \xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\
      \x00\x00\x00\x01\xFF\xFF\xFF\xE1\
      \x00\x00\x00\x00\x00\x00\x00\x01\
      \x06number\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x05\x80\x00\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_fxp_unsigned_8_8_64_named,
    [
        e!(0x44, h!(0x40, FixedPoint, d!(
            fp b"number" => [sgn: false, w: 0x08, int: 0x08],
               sbmsk: 0x0000_0000_0000_0000,
               bmsk: 0x0000_0000_0000_00FF,
               u0: 0x0000_0311,
               u1: 0x0000_0001_0000_0001,
               u2: 0x0000_0000_0000_0001))),
    ],
    v!(fp [sgn: false, w: 0x08, int: 0x08] 0x0000_0000_0000_0040),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x44\x40\x5F\
      \x03\x11\
      \x00\x08\
      \x00\x00\x00\x08\
      \x00\x00\
      \x00\x08\
      \x00\x00\x00\x08\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\
      \x00\x08\
      \x00\x00\x00\x08\
      \x00\x00\x00\x00\x00\x00\x00\xFF\
      \x00\x00\x00\x01\x00\x00\x00\x01\
      \x00\x00\x00\x00\x00\x00\x00\x01\
      \x06number\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_variant_nothing_2,
    [e!(0x10, h!(0x40, Variant, d!(scalar b"dumped_data")))],
    v!(var var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))] => v!(nothing))),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x10\x40\x53\
      \x0Bdumped_data\x00\x01\x00\x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\
      \x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_fxp_unsigned_8_0_0d5_named,
    [
        e!(0x44, h!(0x40, FixedPoint, d!(
            fp b"number" => [sgn: false, w: 0x08, int: 0x00],
               sbmsk: 0x0000_0000_0000_0000,
               bmsk: 0x0000_0000_0000_00FF,
               u0: 0x0000_0311,
               u1: 0x0000_0001_FFFF_FFF9,
               u2: 0x0000_0000_0000_0001))),
    ],
    v!(fp [sgn: false, w: 0x08, int: 0x00] 0x0000_0000_0000_0080),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x44\x40\x5F\
      \x03\x11\
      \x00\x08\
      \x00\x00\x00\x00\
      \x00\x00\
      \x00\x08\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\
      \x00\x08\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\xFF\
      \x00\x00\x00\x01\xFF\xFF\xFF\xF9\
      \x00\x00\x00\x00\x00\x00\x00\x01\
      \x06number\
      \x00\x00\x01\
      \x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_fxp_signed_8_0_n0d25_named,
    [
        e!(0x44, h!(0x40, FixedPoint, d!(
            fp b"number" => [sgn: true, w: 0x08, int: 0x00],
               sbmsk: 0xFFFF_FFFF_FFFF_FF80,
               bmsk: 0x0000_0000_0000_007F,
               u0: 0x0000_0351,
               u1: 0x0000_0001_FFFF_FFF9,
               u2: 0x0000_0000_0000_0001))),
    ],
    v!(fp [sgn: true, w: 0x08, int: 0x00] 0xFFFF_FFFF_FFFF_FFC0),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x44\x40\x5F\
      \x03\x51\
      \x00\x08\
      \x00\x00\x00\x00\
      \x00\x01\
      \x00\x08\
      \x00\x00\x00\x00\
      \xFF\xFF\xFF\xFF\xFF\xFF\xFF\x80\
      \x00\x01\
      \x00\x08\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x7F\
      \x00\x00\x00\x01\xFF\xFF\xFF\xF9\
      \x00\x00\x00\x00\x00\x00\x00\x01\
      \x06number\
      \x00\x00\x01\
      \x00\x00\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xC0\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_cf32_n0d25_1d25i_named,
    [e!(0x0D, h!(0x40, Complex32, d!(scalar b"number")))],
    v!(c32 -0.25, i 1.25),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x0D\x40\x0C\
      \x00\x06number\
      \x00\x00\x01\
      \x00\x00\xBE\x80\x00\x00\x3F\xA0\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_cf64_n0d25_1d25i_named,
    [e!(0x0D, h!(0x40, Complex64, d!(scalar b"number")))],
    v!(c64 -0.25, i 1.25),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x0D\x40\x0D\
      \x00\x06number\
      \x00\x00\x01\
      \x00\x00\xBF\xD0\x00\x00\x00\x00\x00\x00\x3F\xF4\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_typedefed_named_i32_array,
    [
        e!(0x1D, h!(0x40, I32, d!(scalar b"max_samples_to_average")), 0x59),
        e!(0x59, h!(0x00, Typedef, d!(
            typedef [b"test_variant_i32_array_typedef.ctl"]
                    = h!(0x40, Array, d!(
                        arr b"max_samples_to_average_array"
                            => 0x0001, 0x0000)),
                    us: 0x0000_0000,
                    u0: 0x002E))),
    ],
    v!(arr [v!(i32 0x80CD7FA5u32 as i32)]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x02\
      \x00\x1D\
        \x40\x03\
        \x00\x16max_samples_to_average\
      \x00\x00\x59\
        \x00\xF1\
        \x00\x00\x00\x00\
        \x00\x00\x00\x01\
          \x22test_variant_i32_array_typedef.ctl\
        \x00\x2E\
          \x40\x40\
          \x00\x01\
          \xFF\xFF\xFF\xFF\
          \x00\x00\
          \x1Cmax_samples_to_average_array\
      \x00\x00\x01\
      \x00\x01\
      \x00\x00\x00\x01\
      \x80\xCD\x7F\xA5\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_typedefed_named_typedefed_named_i32_array,
    [
        e!(0x46, h!(0x00, Typedef, d!(
            typedef [b"test_variant_i32_typedef.ctl"]
                    = h!(0x40, I32, d!(scalar b"max_samples_to_average")),
                    us: 0x0000_0000,
                    u0: 0x0021)), 0x63),
        e!(0x63, h!(0x00, Typedef, d!(
            typedef [b"test_variant_typedefed_i32_array_typedef.ctl"]
                    = h!(0x40, Array, d!(
                        arr b"max_samples_to_average_array"
                            => 0x0001, 0x0000)),
                    us: 0x0000_0000,
                    u0: 0x002E))),
    ],
    v!(arr [v!(i32 -10)]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x02\
      \x00\x46\
        \x00\xF1\
        \x00\x00\x00\x00\
        \x00\x00\x00\x01\
          \x1Ctest_variant_i32_typedef.ctl\
        \x00\x21\
          \x40\x03\
          \x00\x16max_samples_to_average\
      \x00\x00\x63\
        \x00\xF1\
        \x00\x00\x00\x00\
        \x00\x00\x00\x01\
          \x2Ctest_variant_typedefed_i32_array_typedef.ctl\
        \x00\x2E\
          \x40\x40\
          \x00\x01\
          \xFF\xFF\xFF\xFF\
          \x00\x00\
          \x1Cmax_samples_to_average_array\
      \x00\x00\x01\
      \x00\x01\
      \x00\x00\x00\x01\
      \xFF\xFF\xFF\xF6\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_2xi32_cluster,
    [
        e!(0x05, h!(0x00, I32, d!(scalar b"")), 0x0A),
        e!(0x0A, h!(0x00, Cluster, d!(cluster b"" => 0, 0))),
    ],
    v!(cluster [v!(i32 0xAA55), v!(i32 0x77EE)]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x02\
      \x00\x05\
        \x00\x03\
        \x00\x00\
      \x0A\
        \x00\x50\
        \x00\x02\
          \x00\x00\
          \x00\x00\
        \x00\
        \x01\
      \x00\x01\
      \x00\x00\xAA\x55\
      \x00\x00\x77\xEE\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_i32_i32_cluster,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x05),
        e!(0x05, h!(0x00, I32, d!(scalar b"")), 0x0A),
        e!(0x0A, h!(0x00, Cluster, d!(cluster b"" => 0, 1))),
    ],
    v!(cluster [v!(i32 0xAA55), v!(i32 0x77EE)]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x03\
      \x00\x07\
        \x40\x03\
        \x00\x01x\
      \x00\x05\
        \x00\x03\
        \x00\x00\
      \x0A\
        \x00\x50\
        \x00\x02\
          \x00\x00\
          \x00\x01\
        \x00\
        \x01\
      \x00\x02\
      \x00\x00\xAA\x55\
      \x00\x00\x77\xEE\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_string,
    [e!(0x0C, h!(0x40, String, d!(str b"str")))],
    v!(str b"foobar"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x0C\
        \x40\x30\
        \xFF\xFF\xFF\xFF\
        \x03str\
      \x00\x01\
      \x00\x00\
      \x00\x00\x00\x06foobar\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_2xi32_cluster,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x07),
        e!(0x07, h!(0x40, I32, d!(scalar b"y")), 0x0A),
        e!(0x0A, h!(0x00, Cluster, d!(cluster b"" => 0, 1))),
    ],
    v!(cluster [v!(i32 0xAA55), v!(i32 0x77EE)]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x03\
      \x00\x07\
        \x40\x03\
        \x00\x01x\
      \x00\x07\
        \x40\x03\
        \x00\x01y\
      \x00\x0A\
        \x00\x50\
        \x00\x02\
          \x00\x00\
          \x00\x01\
        \x00\x01\
      \x00\x02\
      \x00\x00\xAA\x55\
      \x00\x00\x77\xEE\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_named_2xi32_cluster,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x07),
        e!(0x07, h!(0x40, I32, d!(scalar b"y")), 0x10),
        e!(0x10, h!(0x40, Cluster, d!(cluster b"clust" => 0, 1))),
    ],
    v!(cluster [v!(i32 0xAA55), v!(i32 0x77EE)]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x03\
      \x00\x07\
        \x40\x03\
        \x00\x01x\
      \x00\x07\
        \x40\x03\
        \x00\x01y\
      \x00\x10\
        \x40\x50\
        \x00\x02\
          \x00\x00\
          \x00\x01\
        \x05clust\
      \x00\x01\
      \x00\x02\
      \x00\x00\xAA\x55\
      \x00\x00\x77\xEE\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_variant_variant_nothing,
    [e!(0x0C, h!(0x40, Variant, d!(scalar b"Variant")))],
    v!(var var!([e!(0x10, h!(0x40, Variant, d!(scalar b"dumped_data")))]
                => v!(var var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                               => v!(nothing))))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x0C\
        \x40\x53\
        \x07Variant\
      \x00\x01\
      \x00\x00\
      \x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x10\
          \x40\x53\
          \x0Bdumped_data\
        \x00\x01\
        \x00\x00\
        \x15\x00\x80\x00\
        \x00\x00\x00\x01\
          \x00\x04\
        \x00\x00\x00\x01\
        \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_i32_named_named_i32_array_cluster,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x07),
        e!(0x07, h!(0x40, I32, d!(scalar b"y")), 0x10),
        e!(0x10, h!(0x40, Array, d!(arr b"ys" => 0x0001, 0x0001)), 0x10),
        e!(0x10, h!(0x40, Cluster, d!(cluster b"clust" => 0, 2))),
    ],
    v!(cluster [v!(i32 0x0000AA55), v!(arr [v!(i32 0x000077EE)])]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x04\
      \x00\x07\
        \x40\x03\
        \x00\x01x\
      \x00\x07\
        \x40\x03\
        \x00\x01y\
      \x00\x10\
        \x40\x40\
        \x00\x01\
        \xFF\xFF\xFF\xFF\
        \x00\x01\
        \x02ys\
      \x00\x00\x10\
        \x40\x50\
        \x00\x02\
          \x00\x00\
          \x00\x02\
        \x05clust\
      \x00\x01\
      \x00\x03\
      \x00\x00\xAA\x55\
      \x00\x00\x00\x01\
        \x00\x00\x77\xEE\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_i32_named_named_typedefed_i32_array_cluster,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x30),
        e!(0x30, h!(0x00, Typedef, d!(
            typedef [b"test_variant_i32_typedef.ctl"]
                    = h!(0x40, I32, d!(scalar b"y")),
                    us: 0x00000000,
                    u0: 0x000B)), 0x10),
        e!(0x10, h!(0x40, Array, d!(arr b"ys" => 0x0001, 0x0001)), 0x10),
        e!(0x10, h!(0x40, Cluster, d!(cluster b"clust" => 0, 2)))
    ],
    v!(cluster [v!(i32 0x0000AA55), v!(arr [v!(i32 0x000077EE)])]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x04\
      \x00\x07\
        \x40\x03\
        \x00\x01x\
      \x00\x30\
        \x00\xF1\
        \x00\x00\x00\x00\
        \x00\x00\x00\x01\
          \x1Ctest_variant_i32_typedef.ctl\
        \x00\x0B\
          \x40\x03\
          \x00\x01y\
      \x00\x10\
        \x40\x40\
        \x00\x01\
        \xFF\xFF\xFF\xFF\
        \x00\x01\
        \x02ys\
      \x00\x00\x10\
        \x40\x50\
        \x00\x02\
          \x00\x00\
          \x00\x02\
        \x05clust\
      \x00\x01\
      \x00\x03\
      \x00\x00\xAA\x55\
      \x00\x00\x00\x01\
        \x00\x00\x77\xEE\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_tdefed_named_i32_named_named_typedefed_i32_array_cluster,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x30),
        e!(0x30, h!(0x00, Typedef, d!(
            typedef [b"test_variant_i32_typedef.ctl"]
                    = h!(0x40, I32, d!(scalar b"y")),
                    us: 0x00000000,
                    u0: 0x000B)), 0x10),
        e!(0x10, h!(0x40, Array, d!(arr b"ys" => 0x0001, 0x0001)), 0x55),
        e!(0x55, h!(0x00, Typedef, d!(
            typedef [
                b"test_variant_i32_typedefed_i32_array_cluster_typedef.ctl"
            ] = h!(0x40, Cluster, d!(cluster b"clust" => 0, 2)),
            us: 0x00000000,
            u0: 0x0014))),
    ],
    v!(cluster [v!(i32 0x0000AA55), v!(arr [v!(i32 0x000077EE)])]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x04\
      \x00\x07\
        \x40\x03\
        \x00\x01x\
      \x00\x30\
        \x00\xF1\
        \x00\x00\x00\x00\
        \x00\x00\x00\x01\
          \x1Ctest_variant_i32_typedef.ctl\
        \x00\x0B\
          \x40\x03\
          \x00\x01y\
        \x00\x10\
          \x40\x40\
          \x00\x01\
          \xFF\xFF\xFF\xFF\
          \x00\x01\
          \x02ys\
        \x00\x00\x55\
          \x00\xF1\
          \x00\x00\x00\x00\
          \x00\x00\x00\x01\
            \x38test_variant_i32_typedefed_i32_array_cluster_typedef.ctl\
          \x00\x14\
            \x40\x50\
            \x00\x02\
              \x00\x00\
              \x00\x02\
            \x05clust\
        \x00\x01\
        \x00\x03\
        \x00\x00\xAA\x55\
        \x00\x00\x00\x01\
          \x00\x00\x77\xEE\
        \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_string,
    [e!(0x08, h!(0x00, String, d!(str b"")))],
    v!(str b"foobar"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x08\
        \x00\x30\
        \xFF\xFF\xFF\xFF\
        \x00\
      \x01\
      \x00\x00\
      \x00\x00\x00\x06foobar\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_relative_path,
    [e!(0x0C, h!(0x40, Path, d!(str b"pth")))],
    v!(pth rel [b"foo"]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x0C\
        \x40\x32\
        \xFF\xFF\xFF\xFF\
        \x03pth\
      \x00\x01\
      \x00\x00\
      PTH0\
        \x00\x00\x00\x08\
        \x00\x01\
        \x00\x01\
          \x03foo\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_absolute_path,
    [e!(0x08, h!(0x00, Path, d!(str b"")))],
    v!(pth abs [b"C", b"foo"]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x08\
        \x00\x32\
        \xFF\xFF\xFF\xFF\
        \x00\
      \x01\
      \x00\x00\
      PTH0\
        \x00\x00\x00\x0A\
        \x00\x00\
        \x00\x02\
          \x01C\
          \x03foo\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_invalid_path,
    [e!(0x08, h!(0x00, Path, d!(str b"")))],
    v!(pth invalid),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x08\
        \x00\x32\
        \xFF\xFF\xFF\xFF\
        \x00\
      \x01\
      \x00\x00\
      PTH0\
        \x00\x00\x00\x04\
        \x00\x02\
        \x00\x00\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_empty_path,
    [e!(0x08, h!(0x00, Path, d!(str b"")))],
    v!(pth []),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x08\
        \x00\x32\
        \xFF\xFF\xFF\xFF\
        \x00\
      \x01\
      \x00\x00\
      PTH0\
        \x00\x00\x00\x04\
        \x00\x00\
        \x00\x00\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_true_boolean,
    [e!(0x04, h!(0x00, Boolean, d!(scalar b"")))],
    v!(b true),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x04\
        \x00\x21\
        \x00\
      \x01\
      \x00\x00\
      \x01\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_false_boolean,
    [e!(0x0A, h!(0x40, Boolean, d!(scalar b"false")))],
    v!(b false),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x0A\
        \x40\x21\
        \x05false\
      \x00\x01\
      \x00\x00\
      \x00\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_timestamp,
    [e!(0x14, h!(0x40, Timestamped, d!(ts b"current time")))],
    v!(ts 0x0000_0000_0000_0000, 0x0041_8937_4BC6_A7F0),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x14\
        \x40T\
        \x00\x06\
        \x0Ccurrent time\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_timestamp,
    [e!(0x06, h!(0x00, Timestamped, d!(ts b"")))],
    v!(ts 0x0000_0000_0000_0000, 0x0041_8937_4BC6_A7F0),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x06\
        \x00T\
        \x00\x06\
        \x00\
      \x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_variant_named_ext,
    [e!(0x1D, h!(0x40, F128, d!(scalar b"max_samples_to_average")))],
    v!(f128 0xA5u8 as f64),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x1D\
        \x40\x0B\
        \x00\x16max_samples_to_average\
      \x00\x00\x01\
      \x00\x00\
      \x40\x06\x4A\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Complex ext number -0.25+1.25i, named `number`.
test_variant_positive!(
    test_variant_cext_n0d25_1d25i_named,
    [e!(0x0D, h!(0x40, Complex128, d!(scalar b"number")))],
    v!(c128 -0.25, i 1.25),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x0D\
        \x40\x0E\
        \x00\x06number\
      \x00\x00\x01\
      \x00\x00\
      \xBF\xFD\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x3F\xFF\x40\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_lib_lib_cls_public_typedefed_bool,
    [e!(0x6D, h!(0x00, Typedef, d!(
        typedef [b"test_lib.lvlib",
                 b"test_lib_nested.lvlib",
                 b"test_lib_nested_class.lvclass",
                 b"cls_public_bool.ctl"]
                = h!(0x40, Boolean, d!(scalar b"bool")),
                us: 0x0000_0000,
                u0: 0x000E)))],
    v!(b false),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x6D\
        \x00\xF1\
        \x00\x00\x00\x00\
        \x00\x00\x00\x04\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_class.lvclass\
          \x13cls_public_bool.ctl\
        \x00\x0E\
          \x40\x21\
          \x04bool\
      \x00\x00\x01\
      \x00\x00\
      \x00\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_lib_lib_cls_protected_typedefed_bool,
    [e!(0x70, h!(0x00, Typedef, d!(
        typedef [b"test_lib.lvlib",
                 b"test_lib_nested.lvlib",
                 b"test_lib_nested_class.lvclass",
                 b"cls_protected_bool.ctl"]
                = h!(0x40, Boolean, d!(scalar b"bool")),
                us: 0x0000_0000,
                u0: 0x000E)))],
    v!(b false),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x70\
        \x00\xF1\
        \x00\x00\x00\x00\
        \x00\x00\x00\x04\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_class.lvclass\
          \x16cls_protected_bool.ctl\
        \x00\x0E\
          \x40\x21\
          \x04bool\
      \x00\x00\x01\
      \x00\x00\
      \x00\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_lib_lib_cls_private_typedefed_bool,
    [e!(0x6E, h!(0x00, Typedef, d!(
        typedef [b"test_lib.lvlib",
                 b"test_lib_nested.lvlib",
                 b"test_lib_nested_class.lvclass",
                 b"cls_private_bool.ctl"]
                = h!(0x40, Boolean, d!(scalar b"bool")),
                us: 0x0000_0000,
                u0: 0x000E)))],
    v!(b false),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x6E\
        \x00\xF1\
        \x00\x00\x00\x00\
        \x00\x00\x00\x04\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_class.lvclass\
          \x14cls_private_bool.ctl\
        \x00\x0E\
          \x40\x21\
          \x04bool\
      \x00\x00\x01\
      \x00\x00\
      \x00\
      \x00\x00\x00\x00");

test_variant_positive!(
    test_lib_lib_cls_community_typedefed_bool,
    [e!(0x70, h!(0x00, Typedef, d!(
        typedef [b"test_lib.lvlib",
                 b"test_lib_nested.lvlib",
                 b"test_lib_nested_class.lvclass",
                 b"cls_community_bool.ctl"]
                = h!(0x40, Boolean, d!(scalar b"bool")),
                us: 0x0000_0000,
                u0: 0x000E)))],
    v!(b false),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x70\
        \x00\xF1\
        \x00\x00\x00\x00\
        \x00\x00\x00\x04\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_class.lvclass\
          \x16cls_community_bool.ctl\
        \x00\x0E\
          \x40\x21\
          \x04bool\
      \x00\x00\x01\
      \x00\x00\
      \x00\
      \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl" named `foobar` 
// belonging to class `test_lib_clas2.lvclass` inside library 
// `test_lib.lvlib`. Owning class has version 1.2.3b5, test_lib has version 
// 1.0.0b0.
test_variant_positive!(
    test_lib_cls2_named_nonempty_nonempty,
    [e!(0x38, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_clas2.lvclass",
                          b""], u0: 0)))],
    v!(cls 1 . 2 . 3 . 5 [b"test_lib.lvlib", b"test_lib_clas2.lvclass", b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x38\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x27\
            \x0Etest_lib.lvlib\
            \x16test_lib_clas2.lvclass\
            \x00\
          \x06foobar\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x27\
          \x0Etest_lib.lvlib\
          \x16test_lib_clas2.lvclass\
          \x00\
        \x00\x01\
        \x00\x02\
        \x00\x03\
        \x00\x05\
        \x00\x00\x00\x0E\
          \x00\x00\x00\x03ghi\
          \x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data bool=true named foobar belonging to 
// class test_lib_nested_class.lvclass inside library test_lib_nested.lvlib 
// inside library test_lib.lvlib. Owning class has version 1.0.0b1, 
// test_lib_nested lib has version 1.0.0b0, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_lib_cls_named_true,
    [e!(0x56, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_nested.lvlib",
                          b"test_lib_nested_class.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 1 . 0 . 0 . 1 [b"test_lib.lvlib",
                          b"test_lib_nested.lvlib",
                          b"test_lib_nested_class.lvclass",
                          b""]
           = b"\x01"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x56\
        \x40\x70\
        \x00\x1E\
        \x00\x00\
        \x44\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_class.lvclass\
          \x00\x00\
        \x06foobar\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x44\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_class.lvclass\
          \x00\x00\
        \x00\x00\
        \x00\x01\
        \x00\x00\
        \x00\x00\
        \x00\x01\
        \x00\x00\x00\x01\
          \x01\
      \x00\x00\x00\x00");

// Class constant with private data bool=false named foobar belonging to 
// class test_lib_nested_class.lvclass inside library test_lib_nested.lvlib 
// inside library test_lib.lvlib. Owning class has version 1.0.0b1, 
// test_lib_nested lib has version 1.0.0b0, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_lib_cls_named_false,
    [e!(0x56, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_nested.lvlib",
                          b"test_lib_nested_class.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 1 . 0 . 0 . 1 [b"test_lib.lvlib",
                          b"test_lib_nested.lvlib",
                          b"test_lib_nested_class.lvclass",
                          b""]
           = b""),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x56\
        \x40\x70\
        \x00\x1E\
        \x00\x00\
        \x44\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_class.lvclass\
          \x00\x00\
        \x06foobar\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x44\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_class.lvclass\
          \x00\x00\
        \x00\x00\
        \x00\x01\
        \x00\x00\
        \x00\x00\
        \x00\x01\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data str="foobarbaz" named `foobar` belonging 
// to class `test_lib_class.lvclass` inside library `test_lib.lvlib`. Owning 
// class has version 21845.43690.30583b17477, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_cls_named_nonempty,
    [e!(0x38, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_class.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 0x5555 . 0xAAAA . 0x7777 . 0x4444 [b"test_lib.lvlib",
                                              b"test_lib_class.lvclass",
                                              b""]
           = b"\x00\x00\x00\x09foobarbaz"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x00\x38\
        \x40\x70\
        \x00\x1E\x00\
        \x00\x27\
          \x0Etest_lib.lvlib\
          \x16test_lib_class.lvclass\
          \x00\
        \x06foobar\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x27\
          \x0Etest_lib.lvlib\
          \x16test_lib_class.lvclass\
          \x00\
        \x55\x55\
        \xAA\xAA\
        \x77\x77\
        \x44\x44\
        \x00\x00\x00\x0D\
          \x00\x00\x00\x09\
            foobarbaz\
      \x00\x00\x00\x00");

// Class constant with private data bool=true named foobar belonging to 
// class test_lib_nested_clas2.lvclass inside library test_lib_nested.lvlib 
// inside library test_lib.lvlib. Owning class has version 
// 65535.21845.43690b48059, test_lib_nested lib has version 1.0.0b0, 
// test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_lib_cls2_named_true,
    [e!(0x56, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_nested.lvlib",
                          b"test_lib_nested_clas2.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 0xFFFF . 0x5555 . 0xAAAA . 0xBBBB
           [b"test_lib.lvlib",
            b"test_lib_nested.lvlib",
            b"test_lib_nested_clas2.lvclass",
            b""]
           = b"\x01"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x56\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x44\
            \x0Etest_lib.lvlib\
            \x15test_lib_nested.lvlib\
            \x1Dtest_lib_nested_clas2.lvclass\
            \x00\x00\
          \x06foobar\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x44\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_clas2.lvclass\
          \x00\x00\
        \x00\x00\
        \xFF\xFF\
        \x55\x55\
        \xAA\xAA\
        \xBB\xBB\
        \x00\x00\x00\x01\
          \x01\
      \x00\x00\x00\x00");

// Class constant with private data bool=false named foobar belonging to 
// class test_lib_nested_clas2.lvclass inside library test_lib_nested.lvlib 
// inside library test_lib.lvlib. Owning class has version 
// 65535.21845.43690b48059, test_lib_nested lib has version 1.0.0b0, 
// test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_lib_cls2_named_false,
    [e!(0x56, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_nested.lvlib",
                          b"test_lib_nested_clas2.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 0xFFFF . 0x5555 . 0xAAAA . 0xBBBB
           [b"test_lib.lvlib",
            b"test_lib_nested.lvlib",
            b"test_lib_nested_clas2.lvclass",
            b""]
           = b""),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x56\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x44\
            \x0Etest_lib.lvlib\
            \x15test_lib_nested.lvlib\
            \x1Dtest_lib_nested_clas2.lvclass\
            \x00\x00\
          \x06foobar\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x44\
          \x0Etest_lib.lvlib\
          \x15test_lib_nested.lvlib\
          \x1Dtest_lib_nested_clas2.lvclass\
          \x00\x00\
        \x00\x00\
        \xFF\xFF\
        \x55\x55\
        \xAA\xAA\
        \xBB\xBB\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data str="" named `foobar` belonging to class 
// `test_lib_class.lvclass` inside library `test_lib.lvlib`. Owning class 
// has version 21845.43690.30583b17477, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_cls_named_empty,
    [e!(0x38, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_class.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 0x5555 . 0xAAAA . 0x7777 . 0x4445
           [b"test_lib.lvlib",
            b"test_lib_class.lvclass",
            b""]
           = b""),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x38\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x27\
            \x0Etest_lib.lvlib\
            \x16test_lib_class.lvclass\
            \x00\
          \x06foobar\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x27\
          \x0Etest_lib.lvlib\
          \x16test_lib_class.lvclass\
          \x00\
        \x55\x55\
        \xAA\xAA\
        \x77\x77\
        \x44\x45\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data str="", st2="" named `foobar` belonging 
// to class `test_lib_clas2.lvclass` inside library `test_lib.lvlib`. Owning 
// class has version 1.2.3b5, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_cls2_named_empty_empty,
    [e!(0x38, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_clas2.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 1 . 2 . 3 . 5
           [b"test_lib.lvlib",
            b"test_lib_clas2.lvclass",
            b""]
           = b""),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x38\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x27\
            \x0Etest_lib.lvlib\
            \x16test_lib_clas2.lvclass\
            \x00\
          \x06foobar\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x27\
          \x0Etest_lib.lvlib\
          \x16test_lib_clas2.lvclass\
          \x00\
        \x00\x01\
        \x00\x02\
        \x00\x03\
        \x00\x05\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data str="abc", st2="" named `foobar` 
// belonging to class `test_lib_clas2.lvclass` inside library 
// `test_lib.lvlib`. Owning class has version 1.2.3b5, test_lib has version 
// 1.0.0b0.
test_variant_positive!(
    test_lib_cls2_named_nonempty_empty,
    [e!(0x38, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_clas2.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 1 . 2 . 3 . 5
           [b"test_lib.lvlib",
            b"test_lib_clas2.lvclass",
            b""]
           = b"\x00\x00\x00\x03abc\x00\x00\x00\x00"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x38\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x27\
            \x0Etest_lib.lvlib\
            \x16test_lib_clas2.lvclass\
            \x00\
          \x06foobar\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x27\
          \x0Etest_lib.lvlib\
          \x16test_lib_clas2.lvclass\
          \x00\
        \x00\x01\
        \x00\x02\
        \x00\x03\
        \x00\x05\
        \x00\x00\x00\x0B\
          \x00\x00\x00\x03abc\
          \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data str="", st2="def" named `foobar` 
// belonging to class `test_lib_clas2.lvclass` inside library 
// `test_lib.lvlib`. Owning class has version 1.2.3b5, test_lib has version 
// 1.0.0b0.
test_variant_positive!(
    test_lib_cls2_named_empty_nonempty,
    [e!(0x38, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_clas2.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 1 . 2 . 3 . 5
           [b"test_lib.lvlib",
            b"test_lib_clas2.lvclass",
            b""]
           = b"\x00\x00\x00\x00\x00\x00\x00\x03def"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x38\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x27\
            \x0Etest_lib.lvlib\
            \x16test_lib_clas2.lvclass\
            \x00\
          \x06foobar\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x27\
          \x0Etest_lib.lvlib\
          \x16test_lib_clas2.lvclass\
          \x00\
        \x00\x01\
        \x00\x02\
        \x00\x03\
        \x00\x05\
        \x00\x00\x00\x0B\
          \x00\x00\x00\x00\x00\x00\x00\x03def\
      \x00\x00\x00\x00");

// 4D array named `falses` containing boolean values named `false`. Number 
// of values is so that first dimension has size two, second dimension has 
// size three, third dimension has size four and fourth dimension has size 
// five. All values are false.
test_variant_positive!(
    test_variant_named_named_boolean_4d_array,
    [
        e!(0x0A, h!(0x40, Boolean, d!(scalar b"false")), 0x20),
        e!(0x20, h!(0x40, Array, d!(arr b"falses" => 0x00004, 0x0000))),
    ],
    v!(arr [2, 3, 4, 5] => [v!(b false);]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x02\
        \x00\x0A\
          \x40\x21\
          \x05false\
        \x00\x20\
          \x40\x40\
          \x00\x04\
            \xFF\xFF\xFF\xFF\
            \xFF\xFF\xFF\xFF\
            \xFF\xFF\xFF\xFF\
            \xFF\xFF\xFF\xFF\
          \x00\x00\
          \x06falses\
        \x00\x00\x01\
      \x00\x01\
      \x00\x00\x00\x02\
      \x00\x00\x00\x03\
      \x00\x00\x00\x04\
      \x00\x00\x00\x05\
        \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// 4D array named `falses` containing boolean values named `false`. Array is 
// empty.
test_variant_positive!(
    test_variant_named_named_boolean_4d_empty_array,
    [
        e!(0x0A, h!(0x40, Boolean, d!(scalar b"false")), 0x20),
        e!(0x20, h!(0x40, Array, d!(arr b"falses" => 0x00004, 0x0000))),
    ],
    v!(arr [0, 0, 0, 0] => []),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x02\
        \x00\x0A\
          \x40\x21\
          \x05false\
        \x00\x20\
          \x40\x40\
          \x00\x04\
            \xFF\xFF\xFF\xFF\
            \xFF\xFF\xFF\xFF\
            \xFF\xFF\xFF\xFF\
            \xFF\xFF\xFF\xFF\
          \x00\x00\
          \x06falses\
        \x00\x00\x01\
      \x00\x01\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl" named `foobar` 
// belonging to class `test_lib_class3.lvclass` inside library 
// `test_lib.lvlib`. Owning class has version 1.3.5b7, test_lib has version 
// 1.0.0b0.
test_variant_positive!(
    test_lib_cls3_named_nonempty_nonempty,
    [e!(0x3A, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_class3.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 1 . 3 . 5 . 7
           [b"test_lib.lvlib",
            b"test_lib_class3.lvclass",
            b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x3A\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x28\
            \x0Etest_lib.lvlib\
            \x17test_lib_class3.lvclass\
            \x00\x00\
          \x06foobar\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x28\
          \x0Etest_lib.lvlib\
          \x17test_lib_class3.lvclass\
          \x00\x00\
          \x00\x00\
          \x00\x01\
          \x00\x03\
          \x00\x05\
          \x00\x07\
          \x00\x00\x00\x0E\
            \x00\x00\x00\x03ghi\
            \x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl" named `foobar` 
// belonging to class `test_lib_cla4.lvclass` inside library 
// `test_lib.lvlib`. Owning class has version 127.45.44b43, test_lib has 
// version 1.0.0b0.
test_variant_positive!(
    test_lib_cls4_named_nonempty_nonempty,
    [e!(0x38, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_cla4.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 127 . 45 . 44 . 43
           [b"test_lib.lvlib",
            b"test_lib_cla4.lvclass",
            b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x38\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x26\
            \x0Etest_lib.lvlib\
            \x15test_lib_cla4.lvclass\
            \x00\x00\
          \x06foobar\
        \x00\x00\x01\
        \x00\x00\
        \x00\x00\x00\x01\
          \x26\
            \x0Etest_lib.lvlib\
            \x15test_lib_cla4.lvclass\
            \x00\x00\
          \x00\x7F\
          \x00\x2D\
          \x00\x2C\
          \x00\x2B\
          \x00\x00\x00\x0E\
            \x00\x00\x00\x03ghi\
            \x00\x00\x00\x03jkl\
        \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl" named `foobar` 
// belonging to class `test_lib_cl5.lvclass` inside library 
// `test_lib.lvlib`. Owning class has version 1.5.10b50, test_lib has 
// version 1.0.0b0.
test_variant_positive!(
    test_lib_cls5_named_nonempty_nonempty,
    [e!(0x36, h!(0x40, Extended, d!(
        cls b"foobar" => [b"test_lib.lvlib",
                          b"test_lib_cl5.lvclass",
                          b""],
            u0: 0x0000)))],
    v!(cls 1 . 5 . 10 . 50
           [b"test_lib.lvlib",
            b"test_lib_cl5.lvclass",
            b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x36@p\x00\x1E\x00\x00\
      %\x0Etest_lib.lvlib\x14test_lib_cl5.lvclass\x00\x06foobar\
      \x00\x00\x01\x00\x00\x00\x00\x00\x01\
      %\x0Etest_lib.lvlib\x14test_lib_cl5.lvclass\x00\x00\x00\
      \x00\x01\x00\x05\x00\x0A\x00\x32\
      \x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl", not named, 
// belonging to class `test_lib_c6.lvclass` inside library `test_lib.lvlib`. 
// Owning class has version 1.42.84b126, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_cls6_unnamed_nonempty_nonempty,
    [e!(0x2E, h!(0x00, Extended, d!(
        cls b"" => [b"test_lib.lvlib",
                    b"test_lib_c6.lvclass",
                    b""],
            u0: 0x0000)))],
    v!(cls 1 . 42 . 84 . 126
           [b"test_lib.lvlib",
            b"test_lib_c6.lvclass",
            b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00.\x00p\x00\x1E\x00\x00\
      $\x0Etest_lib.lvlib\x13test_lib_c6.lvclass\x00\
      \x00\x00\x01\x00\x00\x00\x00\x00\x01\
      $\x0Etest_lib.lvlib\x13test_lib_c6.lvclass\x00\x00\x00\x00\
      \x00\x01\x00*\x00T\x00~\
      \x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl", not named, 
// belonging to class `test_lib_7.lvclass` inside library `test_lib.lvlib`. 
// Owning class has version 1.2.3b700, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_cls7_unnamed_nonempty_nonempty,
    [e!(0x2C, h!(0x00, Extended, d!(
        cls b"" => [b"test_lib.lvlib",
                    b"test_lib_7.lvclass",
                    b""],
            u0: 0x0000)))],
    v!(cls 1 . 2 . 3 . 700
           [b"test_lib.lvlib",
            b"test_lib_7.lvclass",
            b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x2C\x00p\x00\x1E\x00\x00\
      #\x0Etest_lib.lvlib\x12test_lib_7.lvclass\x00\
      \x00\x01\x00\x00\x00\x00\x00\x01\
      #\x0Etest_lib.lvlib\x12test_lib_7.lvclass\x00\
      \x00\x01\x00\x02\x00\x03\x02\xBC\
      \x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl", named `a`, 
// belonging to class `test_li8.lvclass` inside library `test_lib.lvlib`. 
// Owning class has version 2.4.8b16, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_cls8_named_nonempty_nonempty,
    [e!(0x2C, h!(0x40, Extended, d!(
        cls b"a" => [b"test_lib.lvlib",
                     b"test_li8.lvclass",
                     b""],
            u0: 0x0000)))],
    v!(cls 2 . 4 . 8 . 16
           [b"test_lib.lvlib",
            b"test_li8.lvclass",
            b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00,@p\x00\x1E\x00\x00\
      !\x0Etest_lib.lvlib\x10test_li8.lvclass\x00\x01a\
      \x00\x01\x00\x00\x00\x00\x00\x01\
      !\x0Etest_lib.lvlib\x10test_li8.lvclass\x00\x00\x00\
      \x00\x02\x00\x04\x00\x08\x00\x10\
      \x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl", named `a`, 
// belonging to class `test9.lvclass` inside library `test_lib.lvlib`. 
// Owning class has version 127.63.31b15, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_cls9_named_nonempty_nonempty,
    [e!(0x2A, h!(0x40, Extended, d!(
        cls b"a" => [b"test_lib.lvlib",
                     b"test9.lvclass",
                     b""],
            u0: 0x0000)))],
    v!(cls 127 . 63 . 31 . 15
           [b"test_lib.lvlib",
            b"test9.lvclass",
            b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00*@p\x00\x1E\x00\x00\
      \x1E\x0Etest_lib.lvlib\x0Dtest9.lvclass\x00\x00\x01a\
      \x00\x01\x00\x00\x00\x00\x00\x01\
      \x1E\x0Etest_lib.lvlib\x0Dtest9.lvclass\x00\x00\
      \x00\x7F\x00?\x00\x1F\x00\x0F\
      \x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl", named `a`, 
// belonging to class `a.lvclass` inside library `test_lib.lvlib`. Owning 
// class has version 1.4.16b64, test_lib has version 1.0.0b0.
test_variant_positive!(
    test_lib_cls10_named_nonempty_nonempty,
    [e!(0x26, h!(0x40, Extended, d!(
        cls b"a" => [b"test_lib.lvlib",
                     b"a.lvclass",
                     b""],
            u0: 0x0000)))],
    v!(cls 1 . 4 . 16 . 64
           [b"test_lib.lvlib",
            b"a.lvclass",
            b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00&@p\x00\x1E\x00\x00\
      \x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\x01a\
      \x00\x01\x00\x00\x00\x00\x00\x01\
      \x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\
      \x00\x01\x00\x04\x00\x10\x00@\
      \x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data str="ghi", st2="jkl", named `a`, 
// belonging to class `b.lvclass` . Owning class has version 64.61.63b62.
test_variant_positive!(
    test_cls11_named_nonempty_nonempty,
    [e!(0x16, h!(0x40, Extended, d!(
        cls b"a" => [b"b.lvclass", b""],
            u0: 0x0000)))],
    v!(cls 64 . 61 . 63 . 62
           [b"b.lvclass", b""]
           = b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x16\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x0B\
            \x09b.lvclass\
            \x00\
          \x01a\
        \x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \x0B\
          \x09b.lvclass\
          \x00\
        \x00\x40\
        \x00\x3D\
        \x00\x3F\
        \x00\x3E\
        \x00\x00\x00\x0E\
          \x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant named 
// `long_name_long_name_long_name_long_name_long_name_long_name_long_name\
//  _long_name_long_name_long_name_long_name_long_name_long_name_long_name\
//  _long_name_long_name_long_name_long_name_long_name_long_name.lvlib:long\
//  _name_long_name_long_name_long_name.lvclass` of a class named
// `long_name_long_name_long_name_long_name.lvclass` with version 
// 10.20.30b40 living in library
// `long_name_long_name_long_name_long_name_long_name_long_name_long_name\
//  _long_name_long_name_long_name_long_name_long_name_long_name_long_name\
//  _long_name_long_name_long_name_long_name_long_name_long_name.lvlib`.
test_variant_positive!(
    test_variant_longnamed_longnamed_longnamed_cls,
    [e!(0x0206, h!(0x40, Extended, d!(
        cls b"long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name.\
              lvlib:long_name_long_name_long_name_long_name.lvclass"
            => [b"long_name_long_name_long_name_long_name_long_name_\
                  long_name_long_name_long_name_long_name_long_name_\
                  long_name_long_name_long_name_long_name_long_name_\
                  long_name_long_name_long_name_long_name_long_name\
                  .lvlib",
                b"long_name_long_name_long_name_long_name.lvclass",
                b""],
            u0: 0x0000)))],
    v!(cls 0 . 0 . 0 . 0
           [b"long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name.lvlib",
            b"long_name_long_name_long_name_long_name.lvclass",
            b""]
           = b""),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
      \x02\x06\
        \x40\x70\
        \x00\x1E\
        \x00\x00\
        \xFF\
          \xCDlong_name_long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name.lvlib\
          \x2Flong_name_long_name_long_name_long_name.lvclass\
          \x00\
        \xFDlong_name_long_name_long_name_long_name_long_name_long_name_\
            long_name_long_name_long_name_long_name_long_name_long_name_\
            long_name_long_name_long_name_long_name_long_name_long_name_\
            long_name_long_name.lvlib:long_name_long_name_long_name_long_\
            name.lvclass\
      \x00\x01\
      \x00\x00\
      \x00\x00\x00\x01\
        \xFF\
          \xCDlong_name_long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name_long_name_long_name_long_name_long_name_\
              long_name_long_name.lvlib\
          \x2Flong_name_long_name_long_name_long_name.lvclass\
          \x00\
        \x00\x00\
        \x00\x00\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// True constant named dumped_data and wrapped in another variant which is 
// twice wrapped in variant.
test_variant_positive!(
    test_variant_variant_variant_variant_true,
    [e!(0x0C, h!(0x40, Variant, d!(scalar b"Variant")))],
    v!(var var!([e!(0x0C, h!(0x40, Variant, d!(scalar b"Variant")))]
                => v!(var var!([e!(0x10,
                                   h!(0x40, Boolean,
                                      d!(scalar b"dumped_data")))]
                               => v!(b true))))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x0C\
          \x40\x53\
          \x07Variant\
        \x00\x01\
      \x00\x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\
          \x00\x0C\
            \x40\x53\
            \x07Variant\
          \x00\x01\
        \x00\x00\
        \x15\x00\x80\x00\
          \x00\x00\x00\x01\
            \x00\x10\
              \x40\x21\
              \x0Bdumped_data\
            \x00\x01\
          \x00\x00\
          \x01\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Boolean false named `OK button` typedefed to control 
// `long_name_long_name.ctl` which is located in library 
// `long_name_long_name_long_name_long_name_long_name_long_name_long_name_\
//  long_name_long_name_long_name_long_name_long_name_long_name_long_name_\
//  long_name_long_name_long_name_long_name_long_name_long_name.lvlib`
// which is located in library
// `long_name_long_name_long_name_long_name_long_name_long_name_long_name_\
//  long_name_long_name_long_name_long_name_long_name_long_name_long_name_\
//  long_name_long_name_long_name_long_name_long_name_long_name.lvlib`.
test_variant_positive!(
    test_variant_longtypedefed_named_bool,
    [e!(0x01CE, h!(0x00, Typedef, d!(
        typedef [b"long_name_long_name_long_name_long_name_long_name_long_\
                   name_long_name_long_name_long_name_long_name_long_name_\
                   long_name_long_name_long_name_long_name_long_name_long_\
                   name_long_name_long_name_long_name.lvlib",
                 b"long_name_long_name_long_name_long_name_long_name_long_\
                   name_long_name_long_name_long_name_long_name_long_name_\
                   long_name_long_name_long_name_long_name_long_name_long_\
                   name_long_name_long_name_long_name.lvlib",
                 b"long_name_long_name.ctl"]
                = h!(0x40, Boolean, d!(scalar b"OK Button")),
                us: 0xD839_B265,
                u0: 0x0012)))],
    v!(b false),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x01\xCE\
          \x00\xF1\
            \xD8\x39\xB2\x65\
            \x00\x00\x00\x03\
              \xCDlong_name_long_name_long_name_long_name_long_name_\
                  long_name_long_name_long_name_long_name_long_name_\
                  long_name_long_name_long_name_long_name_long_name_\
                  long_name_long_name_long_name_long_name_long_name.lvlib\
              \xCDlong_name_long_name_long_name_long_name_long_name_\
                  long_name_long_name_long_name_long_name_long_name_\
                  long_name_long_name_long_name_long_name_long_name_\
                  long_name_long_name_long_name_long_name_long_name.lvlib\
              \x17long_name_long_name.ctl\
            \x00\x12\
              \x40\x21\
                \x09OK Button\
        \x00\x01\
      \x00\x00\
      \x00\
      \x00\x00\x00\x00");

// Boolean false typedefed to community control lib_community_bool.ctl 
// inside library test_lib.lvlib.
test_variant_positive!(
    test_lib_community_typedefed_bool,
    [e!(0x3C, h!(0x00, Typedef, d!(
        typedef [b"test_lib.lvlib",
                 b"lib_community_bool.ctl"]
                = h!(0x40, Boolean, d!(scalar b"bool")),
                us: 0x0000_0000,
                u0: 0x000E)))],
    v!(b false),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\
      \x00\x3C\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x02\
      \x0Etest_lib.lvlib\x16lib_community_bool.ctl\
      \x00\x0E\x40\x21\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00");

// Boolean false typedefed to public control lib_public_bool.ctl inside 
// library test_lib.lvlib.
test_variant_positive!(
    test_lib_public_typedefed_bool,
    [e!(0x39, h!(0x00, Typedef, d!(
        typedef [b"test_lib.lvlib",
                 b"lib_public_bool.ctl"]
                = h!(0x40, Boolean, d!(scalar b"bool")),
                us: 0x0000_0000,
                u0: 0x000E)))],
    v!(b false),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\
      \x00\x39\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x02\
      \x0Etest_lib.lvlib\x13lib_public_bool.ctl\
      \x00\x0E\x40\x21\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00");

// Boolean false typedefed to private control lib_private_bool.ctl inside 
// library test_lib.lvlib. Owning library has version 1.0.0b0
test_variant_positive!(
    test_lib_private_typedefed_bool,
    [e!(0x3A, h!(0x00, Typedef, d!(
        typedef [b"test_lib.lvlib",
                 b"lib_private_bool.ctl"]
                = h!(0x40, Boolean, d!(scalar b"bool")),
                us: 0x0000_0000,
                u0: 0x000E)))],
    v!(b false),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\
      \x00\x3A\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x02\
      \x0Etest_lib.lvlib\x14lib_private_bool.ctl\x00\x0E\
      \x40\x21\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00");

// Nothing named dumped_data and wrapped in another variant which is twice 
// wrapped in variant.
test_variant_positive!(
    test_variant_variant_variant_variant_nothing,
    [e!(0x0C, h!(0x40, Variant, d!(scalar b"Variant")))],
    v!(var var!(
        [e!(0x0C, h!(0x40, Variant, d!(scalar b"Variant")))]
        => v!(var var!(
            [e!(0x10, h!(0x40, Variant, d!(scalar b"dumped_data")))]
            => v!(var var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                           => v!(nothing))))))),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\
      \x00\x0C\x40\x53\x07Variant\x00\x01\x00\x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x10@S\x0Bdumped_data\x00\x01\
      \x00\x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00");

// Error constant named `error in (no error)` and containing no error 
// (status false, code zero, empty message).
test_variant_positive!(
    test_variant_named_error,
    [
        e!(0x0C, h!(0x40, Boolean, d!(scalar b"status")), 0x0B),
        e!(0x0B, h!(0x40, I32, d!(scalar b"code")), 0x10),
        e!(0x10, h!(0x40, String, d!(str b"source")), 0x20),
        e!(0x20, h!(0x40, Cluster, d!(cluster b"error in (no error)"
                                              => 0, 1, 2))),
    ],
    v!(cluster [v!(b false), v!(i32 0), v!(str b"")]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x04\
        \x00\x0C\x40\x21\x06status\x00\
        \x00\x0B\x40\x03\x00\x04code\x00\
        \x00\x10\x40\x30\xFF\xFF\xFF\xFF\x06source\x00\
        \x00\x20\x40\x50\
          \x00\x03\x00\x00\x00\x01\x00\x02\
          \x13error in (no error)\
        \x00\x01\
        \x00\x03\
        \x00\
        \x00\x00\x00\x00\
        \x00\x00\x00\x00\
        \x00\x00\x00\x00");

// Boolean true named 
// `big-name-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//          -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`.
test_variant_positive!(
    test_variant_longnamed_boolean,
    [e!(0x104, h!(0x40, Boolean, d!(
        // Note: name got truncated to 255 characters here.
        scalar b"big-name-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
                         -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
                         -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
                         -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu")))],
    v!(b false),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x01\x04\
          \x40\x21\
          \xFFbig-name-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
                      -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
                      -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
                      -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu\
        \x00\x01\
      \x00\x00\
      \x00\
      \x00\x00\x00\x00");

// I32 0x7FAA5566 named 
// `big-name\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`.
test_variant_positive!(
    test_variant_longnamed_i32,
    [e!(0x105, h!(0x40, I32, d!(
        // Note: name got truncated to 255 characters here.
        scalar b"\
            big-name\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu")))],
    v!(i32 0x7FAA5566),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x01\x05\
          \x40\x03\
          \x00\xFF\
            big-name\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu\
        \x00\x01\
      \x00\x00\
      \x7F\xAA\x55\x66\
      \x00\x00\x00\x00");

// Valid notifier named `notifier out` notifying about elements named 
// `clust` which have type `test_variant_2xi32_typedef.ctl` which is 
// a cluster with two I32 numbers named `x` and `y`.
test_variant_positive!(
    test_variant_named_typedefed_named_2xi32_cluster_notifier,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x07),
        e!(0x07, h!(0x40, I32, d!(scalar b"y")), 0x3B),
        e!(0x3B, h!(0x00, Typedef, d!(
            typedef [b"test_variant_2xi32_typedef.ctl"]
                    = h!(0x40, Cluster, d!(cluster b"clust" => 0, 1)),
                    us: 0x0000_0000,
                    u0: 0x0014)), 0x18),
        e!(0x18, h!(0x40, Extended, d!(notifier b"notifier out" => [2]))),
    ],
    v!(u32 0xB5900008),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x04\
        \x00\x07\
          \x40\x03\
          \x00\x01x\
        \x00\x07\
          \x40\x03\
          \x00\x01y\
        \x00\x3B\
          \x00\xF1\
          \x00\x00\x00\x00\
          \x00\x00\x00\x01\
            \x1Etest_variant_2xi32_typedef.ctl\
          \x00\x14\
            \x40\x50\
            \x00\x02\
              \x00\x00\
              \x00\x01\
            \x05clust\
        \x00\x18\
          \x40\x70\
          \x00\x11\
          \x00\x01\
          \x00\x02\
          \x0Cnotifier out\
        \x00\x00\x01\
      \x00\x03\
      \xB5\x90\x00\x08\
      \x00\x00\x00\x00");

// Valid queue named `queue out` containing elements named `clust` which 
// have type `test_variant_2xi32_typedef.ctl` which is a cluster with two 
// I32 numbers named `x` and `y`.
test_variant_positive!(
    test_variant_named_typedefed_named_2xi32_cluster_queue,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x07),
        e!(0x07, h!(0x40, I32, d!(scalar b"y")), 0x3B),
        e!(0x3B, h!(0x00, Typedef, d!(
            typedef [b"test_variant_2xi32_typedef.ctl"]
                    = h!(0x40, Cluster, d!(cluster b"clust" => 0, 1)),
                    us: 0x0000_0000,
                    u0: 0x0014)), 0x14),
        e!(0x14, h!(0x40, Extended, d!(queue b"queue out" => [2]))),
    ],
    v!(u32 0xD6C00026),
    b"\x15\x00\x80\x00\x00\x00\x00\x04\
      \x00\x07\x40\x03\x00\x01x\
      \x00\x07\x40\x03\x00\x01y\
      \x00\x3B\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\
        \x1Etest_variant_2xi32_typedef.ctl\
        \x00\x14\x40\x50\x00\x02\x00\x00\x00\x01\x05clust\
      \x00\x14\x40\x70\x00\x12\x00\x01\x00\x02\x09queue out\
      \x00\x01\x00\x03\
      \xD6\xC0\x00\x26\
      \x00\x00\x00\x00");

// Valid data value reference named `data value reference` containing 
// element named `clust` which has type `test_variant_2xi32_typedef.ctl` 
// which is a cluster with two I32 numbers named `x` and `y`.
test_variant_positive!(
    test_variant_named_typedefed_named_2xi32_cluster_dvr,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x07),
        e!(0x07, h!(0x40, I32, d!(scalar b"y")), 0x3B),
        e!(0x3B, h!(0x00, Typedef, d!(
            typedef [b"test_variant_2xi32_typedef.ctl"]
                    = h!(0x40, Cluster, d!(cluster b"clust" => 0, 1)),
                    us: 0x0000_0000,
                    u0: 0x0014)), 0x21),
        e!(0x21, h!(0x40, Extended, d!(dvr b"data value reference"
                                           => [2]))),
    ],
    v!(u32 0x09400000),
    b"\x15\x00\x80\x00\x00\x00\x00\x04\
      \x00\x07\x40\x03\x00\x01x\
      \x00\x07\x40\x03\x00\x01y\
      \x00\x3B\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\
        \x1Etest_variant_2xi32_typedef.ctl\
        \x00\x14\x40\x50\x00\x02\x00\x00\x00\x01\x05clust\
      \x00\x21\
        \x40\x70\
        \x00\x20\
        \x00\x01\
        \x00\x02\
        \x00\x14data value reference\
      \x00\x00\x01\x00\x03\
      \x09\x40\x00\x00\
      \x00\x00\x00\x00");

// Valid user event named `clust` containing elements named `clust` which 
// have type `test_variant_2xi32_typedef.ctl` which is a cluster with two 
// I32 numbers named `x` and `y`.
test_variant_positive!(
    test_variant_named_typedefed_named_2xi32_cluster_uevent,
    [
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x07),
        e!(0x07, h!(0x40, I32, d!(scalar b"y")), 0x3B),
        e!(0x3B, h!(0x00, Typedef, d!(
            typedef [b"test_variant_2xi32_typedef.ctl"]
                    = h!(0x40, Cluster, d!(cluster b"clust" => 0, 1)),
                    us: 0x0000_0000,
                    u0: 0x0014)), 0x10),
        e!(0x10, h!(0x40, Extended, d!(uevent b"clust" => [2]))),
    ],
    v!(u32 0xBDC0000B),
    b"\x15\x00\x80\x00\x00\x00\x00\x04\
      \x00\x07\x40\x03\x00\x01x\
      \x00\x07\x40\x03\x00\x01y\
      \x00\x3B\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\
        \x1Etest_variant_2xi32_typedef.ctl\
        \x00\x14\x40\x50\x00\x02\x00\x00\x00\x01\x05clust\
      \x00\x10\
        \x40\x70\
        \x00\x19\
        \x00\x01\
        \x00\x02\
        \x05clust\
      \x00\x01\x00\x03\
      \xBD\xC0\x00\x0B\
      \x00\x00\x00\x00");

// FIXME: Find out WTF is actually stored here.

// Instrument descriptor named `foodesc` pointing to instrument `foo`. 
// Descriptor was not properly configured, but still uses `niDMM` instrument 
// as IVI class.
test_variant_positive!(
    test_variant_named_instrument_descriptor,
    [
        e!(0x1E, h!(0x00, Unknown37, d!(
            unknown37 0xFFFFFFFF,
                      0x0003,
                      var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                           => v!(nothing)))), 0x3C),
        e!(0x3C, h!(0x40, Extended, d!(
            instrd b"Instrument Descriptor"
                   => b"niDMM",
                   [0],
                   u0: 0x0000,
                   u1: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                            => v!(nothing))))),
    ],
    v!(str b"foo"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x02\
        \x00\x1E\
          \x00\x37\
            \xFF\xFF\xFF\xFF\
            \x00\x03\
            \x15\x00\x80\x00\
              \x00\x00\x00\x01\
                \x00\x04\
                  \x00\x00\
                \x00\x01\
                \x00\x00\
              \x00\x00\x00\x00\
        \x00\x3C\
          \x40\x70\
          \x00\x0F\
          \x05niDMM\
          \x00\x01\
            \x00\x00\
          \x00\x00\
          \x15\x00\x80\x00\
            \x00\x00\x00\x01\
              \x00\x04\
                \x00\x00\
              \x00\x01\
            \x00\x00\
            \x00\x00\x00\x00\
          \x15Instrument Descriptor\
        \x00\x01\
      \x00\x01\
      \x00\x00\x00\x03foo\
      \x00\x00\x00\x00");

// DAQmx task named `DAQmx Task Name` pointing to task `bar`. Task was not 
// properly configured.
test_variant_positive!(
    test_variant_named_daqmx_task,
    [
        e!(0x1E, h!(0x00, Unknown37, d!(
            unknown37 0xFFFFFFFF,
                      0x0009,
                      var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                           => v!(nothing)))), 0x3A),
        e!(0x3A, h!(0x40, Extended, d!(
            daqmxtask b"DAQmx Task Name"
                      => b"Task", b"NIDAQ"
                      => 0,
                      u0: 0x0100,
                      u1: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                               => v!(nothing))))),
    ],
    v!(str b"bar"),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x02\
        \x00\x1E\
          \x00\x37\
            \xFF\xFF\xFF\xFF\
            \x00\x09\
            \x15\x00\x80\x00\
              \x00\x00\x00\x01\
                \x00\x04\
                  \x00\x00\
                \x00\x01\
              \x00\x00\
              \x00\x00\x00\x00\
          \x00\x3A\
            \x40\x70\
            \x00\x15\
            \x04Task\
            \x00\x00\
            \x01\x00\
            \x00\x05NIDAQ\
            \x15\x00\x80\x00\
              \x00\x00\x00\x01\
                \x00\x04\
                  \x00\x00\
                \x00\x01\
              \x00\x00\
              \x00\x00\x00\x00\
            \x0FDAQmx Task Name\
        \x00\x01\
      \x00\x01\
      \x00\x00\x00\x03bar\
      \x00\x00\x00\x00");

// Cluster constant with long name and elements 1) cluster named `ab` with 
// two false booleans named `a` and `b`; 2) queue named `queue out` with 
// elements being cluster named `clust` typedefed to 
// `test_variant_2xi32_typedef.ctl` which has two I32 elements named `x` and 
// `y`; 3) queue named `abqueue` with elements being cluster named `ab` with 
// two booleans named `a` and `b` as elements; 4) cluster named `ab` with 
// two false booleans named `a` and `b`. All queue constants do not 
// represent any valid queue. The whole cluster is named
// `big-cluster\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
//  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`
test_variant_positive!(
    test_variant_complex_cluster,
    [
        e!(0x06, h!(0x40, Boolean, d!(scalar b"a")), 0x06),
        e!(0x06, h!(0x40, Boolean, d!(scalar b"b")), 0x0E),
        e!(0x0E, h!(0x40, Cluster, d!(cluster b"ab" => 0, 1)), 0x07),
        e!(0x07, h!(0x40, I32, d!(scalar b"x")), 0x07),
        e!(0x07, h!(0x40, I32, d!(scalar b"y")), 0x3B),
        e!(0x3B, h!(0x00, Typedef, d!(
            typedef [b"test_variant_2xi32_typedef.ctl"]
                    = h!(0x40, Cluster, d!(cluster b"clust" => 3, 4)),
                    us: 0x0000_0000,
                    u0: 0x0014)), 0x14),
        e!(0x14, h!(0x40, Extended, d!(queue b"queue out" => [5])), 0x12),
        e!(0x12, h!(0x40, Extended, d!(queue b"abqueue" => [2])), 0x010E),
        e!(0x010E, h!(0x40, Cluster, d!(
            // Note: name got truncated to 255 characters here.
            cluster
                b"big-cluster\
                  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrst\
                                                                     uvwxyz\
                  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrst\
                                                                     uvwxyz\
                  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrst\
                                                                     uvwxyz\
                  -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqr"
                => 2, 6, 7, 2)))
    ],
    v!(cluster [
       v!(cluster [v!(b false), v!(b false)]),
       v!(u32 0),
       v!(u32 0),
       v!(cluster [v!(b false), v!(b false)]),
    ]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x09\
        \x00\x06\
          \x40\x21\
          \x01a\
        \x00\x06\
          \x40\x21\
          \x01b\
        \x00\x0E\
          \x40\x50\
          \x00\x02\
            \x00\x00\
            \x00\x01\
          \x02ab\
        \x00\x00\x07\
          \x40\x03\
          \x00\x01x\
        \x00\x07\
          \x40\x03\
          \x00\x01y\
        \x00\x3B\
          \x00\xF1\
          \x00\x00\x00\x00\
          \x00\x00\x00\x01\
            \x1Etest_variant_2xi32_typedef.ctl\
          \x00\x14\
            \x40\x50\
            \x00\x02\
              \x00\x03\
              \x00\x04\
            \x05clust\
        \x00\x14\
          \x40\x70\
          \x00\x12\
          \x00\x01\
            \x00\x05\
          \x09queue out\
        \x00\x12\
          \x40\x70\
          \x00\x12\
          \x00\x01\
            \x00\x02\
          \x07abqueue\
        \x01\x0E\
          \x40\x50\
          \x00\x04\
            \x00\x02\
            \x00\x06\
            \x00\x07\
            \x00\x02\
          \xFF\
            big-cluster\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\
            -0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqr\
        \x00\x01\
      \x00\x08\
      \x00\
      \x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\
      \x00\
      \x00\x00\x00\x00");

// EXT number NaN, not named.
test_variant_positive!(
    test_variant_ext_nan,
    [e!(0x05, h!(0x00, F128, d!(scalar b"")))],
    v!(f128 NAN),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\
      \x7F\xFF\xFF\xFF\xFE\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// EXT number +inf, not named.
test_variant_positive!(
    test_variant_ext_inf,
    [e!(0x05, h!(0x00, F128, d!(scalar b"")))],
    v!(f128 INFINITY),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\
      \x7F\xFF\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// EXT number -inf, not named.
test_variant_positive!(
    test_variant_ext_ninf,
    [e!(0x05, h!(0x00, F128, d!(scalar b"")))],
    v!(f128 NEG_INFINITY),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\
      \xFF\xFF\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// EXT number -0, not named.
test_variant_positive!(
    test_variant_ext_nzero,
    [e!(0x05, h!(0x00, F128, d!(scalar b"")))],
    v!(f128 -0.0),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\
      \x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// EXT number 5646, not named.
test_variant_positive!(
    test_variant_ext_5646,
    [e!(0x05, h!(0x00, F128, d!(scalar b"")))],
    v!(f128 5646.0),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\
      \x40\x0B\x60\xE0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data xxx="ghi", yyy="jkl", class `a` private 
// data str="yx", st2="xy", named `foo`, belonging to class 
// `a-child.lvclass` which inherits from class `a.lvclass` inside library 
// `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has 
// version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
test_variant_positive!(
    test_lib_child_named_all_nonempty,
    [e!(0x2E, h!(0x40, Extended, d!(
        cls b"foo" => [b"test_lib.lvlib", b"a-child.lvclass", b""],
            u0: 0x0000)))],
    v!(cls [54 . 87 . 23 . 49, 1 . 4 . 16 . 64]
           [b"test_lib.lvlib", b"a-child.lvclass", b""]
           = [b"\x00\x00\x00\x02yx\x00\x00\x00\x02xy",
              b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x2E\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x20\
            \x0Etest_lib.lvlib\
            \x0Fa-child.lvclass\
            \x00\x00\
          \x03foo\
        \x00\x01\
      \x00\x00\
      \x00\x00\x00\x02\
        \x20\
          \x0Etest_lib.lvlib\
          \x0Fa-child.lvclass\
          \x00\x00\
        \x00\x00\
        \x00\x36\
        \x00\x57\
        \x00\x17\
        \x00\x31\
        \x00\x01\
        \x00\x04\
        \x00\x10\
        \x00\x40\
        \x00\x00\x00\x0C\
          \x00\x00\x00\x02yx\
          \x00\x00\x00\x02xy\
        \x00\x00\x00\x0E\
          \x00\x00\x00\x03ghi\
          \x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data xxx="", yyy="", class `a` private data 
// str="", st2="", not named, belonging to class `a-child.lvclass` which 
// inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning 
// class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` 
// has version 1.4.16b64.
test_variant_positive!(
    test_lib_child_unnamed_all_empty,
    [e!(0x2A, h!(0x00, Extended, d!(
        cls b"" => [b"test_lib.lvlib", b"a-child.lvclass", b""],
            u0: 0x0000)))],
    v!(cls [54 . 87 . 23 . 49, 1 . 4 . 16 . 64]
           [b"test_lib.lvlib", b"a-child.lvclass", b""]
           = [b"",
              b""]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x2A\
          \x00\x70\
          \x00\x1E\
          \x00\x00\x20\
            \x0Etest_lib.lvlib\
            \x0Fa-child.lvclass\
            \x00\x00\
        \x00\x01\
      \x00\x00\
      \x00\x00\x00\x02\
        \x20\
          \x0Etest_lib.lvlib\
          \x0Fa-child.lvclass\
          \x00\x00\x00\x00\
        \x00\x36\
        \x00\x57\
        \x00\x17\
        \x00\x31\
        \x00\x01\
        \x00\x04\
        \x00\x10\
        \x00\x40\
        \x00\x00\x00\x00\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data xxx="", yyy="", class `a` private data 
// str="y", st2="x", named `x`, belonging to class `a-child.lvclass` which 
// inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning 
// class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` 
// has version 1.4.16b64.
test_variant_positive!(
    test_lib_child_named_child_empty,
    [e!(0x2C, h!(0x40, Extended, d!(
        cls b"x" => [b"test_lib.lvlib", b"a-child.lvclass", b""],
            u0: 0x0000)))],
    v!(cls [54 . 87 . 23 . 49, 1 . 4 . 16 . 64]
           [b"test_lib.lvlib", b"a-child.lvclass", b""]
           = [b"\x00\x00\x00\x01y\x00\x00\x00\x01x",
              b""]),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\
      \x00\x2C\x40\x70\x00\x1E\x00\x00\
        \x20\x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\
        \x01x\
      \x00\x01\x00\x00\
      \x00\x00\x00\x02\
        \x20\x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\
        \x00\x36\x00\x57\x00\x17\x00\x31\
        \x00\x01\x00\x04\x00\x10\x00\x40\
        \x00\x00\x00\x0A\x00\x00\x00\x01y\x00\x00\x00\x01x\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data xxx="e", yyy="x", class `a` private data 
// str="", st2="", named `x`, belonging to class `a-child.lvclass` which 
// inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning 
// class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` 
// has version 1.4.16b64.
test_variant_positive!(
    test_lib_child_named_parent_empty,
    [e!(0x2C, h!(0x40, Extended, d!(
        cls b"x" => [b"test_lib.lvlib", b"a-child.lvclass", b""],
            u0: 0x0000)))],
    v!(cls [54 . 87 . 23 . 49, 1 . 4 . 16 . 64]
           [b"test_lib.lvlib", b"a-child.lvclass", b""]
           = [b"",
              b"\x00\x00\x00\x01e\x00\x00\x00\x01x"]),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\
      \x00\x2C\x40\x70\x00\x1E\x00\x00\
      \x20\x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x01x\
      \x00\x01\x00\x00\x00\x00\x00\x02\
      \x20\x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\
      \x00\x36\x00\x57\x00\x17\x00\x31\x00\x01\x00\x04\x00\x10\x00\x40\
      \x00\x00\x00\x00\
      \x00\x00\x00\x0A\x00\x00\x00\x01e\x00\x00\x00\x01x\
      \x00\x00\x00\x00");

// Class constant with private data xxx="ghi", yyy="jkl", class `a` private 
// data str="yx", st2="xy", named `foo`, belonging to class 
// `a-child.lvclass`, cast to class `a.lvclass`, which inherits from class 
// `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 
// 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` has version 
// 1.4.16b64.
test_variant_positive!(
    test_lib_child_cast_named_all_nonempty,
    [e!(0x3E, h!(0x40, Extended, d!(
        // For some reason it did not got named as expected. Most likely 
        // because to more generic class call has renamed class according to 
        // its target class input.
        cls b"test_lib.lvlib:a.lvclass"
            => [b"test_lib.lvlib", b"a.lvclass", b""],
            u0: 0x0000)))],
    v!(cls [54 . 87 . 23 . 49, 1 . 4 . 16 . 64]
           [b"test_lib.lvlib", b"a-child.lvclass", b""]
           = [b"\x00\x00\x00\x02yx\x00\x00\x00\x02xy",
              b"\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl"]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x3E\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x1A\
            \x0Etest_lib.lvlib\
            \x09a.lvclass\
            \x00\x00\
          \x18test_lib.lvlib:a.lvclass\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x02\
        \x20\
          \x0Etest_lib.lvlib\
          \x0Fa-child.lvclass\
          \x00\x00\x00\x00\
        \x00\x36\x00\x57\x00\x17\x00\x31\
        \x00\x01\x00\x04\x00\x10\x00\x40\
        \x00\x00\x00\x0C\
          \x00\x00\x00\x02yx\x00\x00\x00\x02xy\
        \x00\x00\x00\x0E\
          \x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\
      \x00\x00\x00\x00");

// Class constant with private data xxx="", yyy="", class `a` private data 
// str="", st2="", not named, belonging to class `a-child.lvclass`, cast to 
// class `a.lvclass`, which inherits from class `a.lvclass` inside library 
// `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has 
// version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
test_variant_positive!(
    test_lib_child_cast_unnamed_all_empty,
    [e!(0x3E, h!(0x40, Extended, d!(
        // For some reason it did not got named as expected. Most likely 
        // because to more generic class call has renamed class according to 
        // its target class input.
        cls b"test_lib.lvlib:a.lvclass"
            => [b"test_lib.lvlib", b"a.lvclass", b""],
            u0: 0x0000)))],
    v!(cls [54 . 87 . 23 . 49, 1 . 4 . 16 . 64]
           [b"test_lib.lvlib", b"a-child.lvclass", b""]
           = [b"",
              b""]),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\
      \x00\x3E\x40\x70\x00\x1E\x00\x00\
      \x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\
      \x18test_lib.lvlib:a.lvclass\x00\x00\x01\x00\x00\
      \x00\x00\x00\x02\
      \x20\x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\
      \x00\x36\x00\x57\x00\x17\x00\x31\x00\x01\x00\x04\x00\x10\x00\x40\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data xxx="", yyy="", class `a` private data 
// str="y", st2="x", named `x`, belonging to class `a-child.lvclass`, cast 
// to class `a.lvclass`, which inherits from class `a.lvclass` inside 
// library `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib 
// has version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
test_variant_positive!(
    test_lib_child_cast_named_child_empty,
    [e!(0x3E, h!(0x40, Extended, d!(
        // For some reason it did not got named as expected. Most likely 
        // because to more generic class call has renamed class according to 
        // its target class input.
        cls b"test_lib.lvlib:a.lvclass"
            => [b"test_lib.lvlib", b"a.lvclass", b""],
            u0: 0x0000)))],
    v!(cls [54 . 87 . 23 . 49, 1 . 4 . 16 . 64]
           [b"test_lib.lvlib", b"a-child.lvclass", b""]
           = [b"\x00\x00\x00\x01y\x00\x00\x00\x01x",
              b""]),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\
      \x00\x3E\x40\x70\x00\x1E\x00\x00\
      \x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\
      \x18test_lib.lvlib:a.lvclass\x00\x00\x01\x00\x00\
      \x00\x00\x00\x02\
      \x20\x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\
      \x00\x36\x00\x57\x00\x17\x00\x31\x00\x01\x00\x04\x00\x10\x00\x40\
      \x00\x00\x00\x0A\x00\x00\x00\x01y\x00\x00\x00\x01x\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Class constant with private data xxx="e", yyy="x", class `a` private data 
// str="", st2="", named `x`, belonging to class `a-child.lvclass`, cast to 
// class `a.lvclass`, which inherits from class `a.lvclass` inside library 
// `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has 
// version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
test_variant_positive!(
    test_lib_child_cast_named_parent_empty,
    [e!(0x3E, h!(0x40, Extended, d!(
        // For some reason it did not got named as expected. Most likely 
        // because to more generic class call has renamed class according to 
        // its target class input.
        cls b"test_lib.lvlib:a.lvclass"
            => [b"test_lib.lvlib", b"a.lvclass", b""],
            u0: 0x0000)))],
    v!(cls [54 . 87 . 23 . 49, 1 . 4 . 16 . 64]
           [b"test_lib.lvlib", b"a-child.lvclass", b""]
           = [b"",
              b"\x00\x00\x00\x01e\x00\x00\x00\x01x"]),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\
      \x00\x3E\x40\x70\x00\x1E\x00\x00\
      \x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\
      \x18test_lib.lvlib:a.lvclass\x00\x00\x01\x00\x00\
      \x00\x00\x00\x02\
      \x20\x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\
      \x00\x36\x00\x57\x00\x17\x00\x31\x00\x01\x00\x04\x00\x10\x00\x40\
      \x00\x00\x00\x00\
      \x00\x00\x00\x0A\x00\x00\x00\x01e\x00\x00\x00\x01x\
      \x00\x00\x00\x00");

// LabVIEW object constant named `foobaz`.
test_variant_positive!(
    test_variant_lvobject,
    [e!(0x20, h!(0x40, Extended, d!(
        cls b"foobaz"
            => [b"LabVIEW Object"],
            u0: 0x0000)))],
    v!(cls [] [] = []),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x20\
          \x40\x70\
          \x00\x1E\
          \x00\x00\
          \x0ELabVIEW Object\
          \x00\x06foobaz\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// DBL waveform constant named `Waveform` with 3:00:00,001 01.01.1904 
// timestamp (UTC+03), delta t 0,45, 3-element array with elements 0,77, 
// 0,78, 0,79 and empty variant.
test_variant_positive!(
    test_variant_named_dbl_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(f64wf b"Waveform")))],
    v!(f64wf [0.77, 0.78, 0.79],
             ts: (0x0000000000000000, 0x004189374BC6A7F0),
             incr: 0.45,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x10\
          \x40\x54\
          \x00\x03\
          \x08Waveform\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xDC\xCC\xCC\xCC\xCC\xCC\xCD\
      \x00\x00\x00\x03\
        \x3F\xE8\xA3\xD7\x0A\x3D\x70\xA4\
        \x3F\xE8\xF5\xC2\x8F\x5C\x28\xF6\
        \x3F\xE9\x47\xAE\x14\x7A\xE1\x48\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\
          \x00\x04\
            \x00\x00\
          \x00\x01\
        \x00\x00\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Empty waveform constant named `Waveform`.
test_variant_positive!(
    test_variant_named_empty_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(f64wf b"Waveform")))],
    v!(f64wf [],
             ts: (0x0000000000000000, 0x0000000000000000),
             incr: 1.0,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x10\
          \x40\x54\
          \x00\x03\
          \x08Waveform\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x3F\xF0\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\
          \x00\x04\
            \x00\x00\
          \x00\x01\
        \x00\x00\
        \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// I32 waveform constant named `Waveform` with 3:00:00,001 01.01.1904 
// timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 
// 75 and empty variant.
test_variant_positive!(
    test_variant_named_i32_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(i32wf b"Waveform")))],
    v!(i32wf [127, 84, 75],
             ts: (0x0000000000000000, 0x004189374BC6A7F0),
             incr: 0.75,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x10\x40\x54\x00\x0F\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\
        \x00\x00\x00\x7F\
        \x00\x00\x00\x54\
        \x00\x00\x00\x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// U64 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 
// timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 
// 75 and empty variant.
test_variant_positive!(
    test_variant_named_u64_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(u64wf b"Waveform")))],
    v!(u64wf [127, 84, 75],
             ts: (0x0000000000015180, 0x004189374BC6A7F0),
             incr: 0.75,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x10\
          \x40\x54\
          \x00\x14\
          \x08Waveform\
          \x00\
        \x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x01\x51\x80\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\
        \x00\x00\x00\x00\x00\x00\x00\x7F\
        \x00\x00\x00\x00\x00\x00\x00\x54\
        \x00\x00\x00\x00\x00\x00\x00\x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// F128 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty 
// variant.
test_variant_positive!(
    test_variant_named_f128_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(f128wf b"Waveform")))],
    v!(f128wf [f128!(127.0), f128!(84.0), f128!(75.0)],
              ts: (0x0000000000000000, 0x004189374BC6A7F0),
              incr: 0.75,
              attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                          => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x10\
          \x40\x54\
          \x00\x0A\
          \x08Waveform\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\
        \x40\x05\xFC\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x40\x05\x50\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x40\x05\x2C\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// F32 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty 
// variant.
test_variant_positive!(
    test_variant_named_f32_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(f32wf b"Waveform")))],
    v!(f32wf [127.0, 84.0, 75.0],
             ts: (0x0000000000000000, 0x004189374BC6A7F0),
             incr: 0.75,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x10\
          \x40\x54\
          \x00\x05\
          \x08Waveform\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\
        \x42\xFE\x00\x00\
        \x42\xA8\x00\x00\
        \x42\x96\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\
          \x00\x04\
        \x00\x00\x00\x01\
        \x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// I64 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty 
// variant.
test_variant_positive!(
    test_variant_named_i64_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(i64wf b"Waveform")))],
    v!(i64wf [127, 84, 75],
             ts: (0x0000000000000000, 0x004189374BC6A7F0),
             incr: 0.75,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x10\
          \x40\x54\
          \x00\x13\
          \x08Waveform\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\
        \x00\x00\x00\x00\x00\x00\x00\x7F\
        \x00\x00\x00\x00\x00\x00\x00\x54\
        \x00\x00\x00\x00\x00\x00\x00\x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// I16 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty 
// variant.
test_variant_positive!(
    test_variant_named_i16_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(i16wf b"Waveform")))],
    v!(i16wf [127, 84, 75],
             ts: (0x0000000000000000, 0x004189374BC6A7F0),
             incr: 0.75,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x10\x40\x54\x00\x02\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\x00\x7F\x00\x54\x00\x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00");

// I8 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty 
// variant.
test_variant_positive!(
    test_variant_named_i8_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(i8wf b"Waveform")))],
    v!(i8wf [127, 84, 75],
            ts: (0x0000000000000000, 0x004189374BC6A7F0),
            incr: 0.75,
            attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                        => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x10\x40\x54\x00\x0E\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\x7F\x54\x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00");

// U32 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty 
// variant.
test_variant_positive!(
    test_variant_named_u32_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(u32wf b"Waveform")))],
    v!(u32wf [127, 84, 75],
             ts: (0x0000000000000000, 0x004189374BC6A7F0),
             incr: 0.75,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x10\x40\x54\x00\x0D\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\x00\x00\x00\x7F\x00\x00\x00\x54\x00\x00\x00\x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00");

// U16 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty 
// variant.
test_variant_positive!(
    test_variant_named_u16_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(u16wf b"Waveform")))],
    v!(u16wf [127, 84, 75],
             ts: (0x0000000000000000, 0x004189374BC6A7F0),
             incr: 0.75,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x10\x40\x54\x00\x0C\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\x00\x7F\x00\x54\x00\x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00");

// U8 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty 
// variant.
test_variant_positive!(
    test_variant_named_u8_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(u8wf b"Waveform")))],
    v!(u8wf [127, 84, 75],
            ts: (0x0000000000000000, 0x004189374BC6A7F0),
            incr: 0.75,
            attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                        => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x10\x40\x54\x00\x0B\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\x7F\x54\x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00");

// Complex128 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 
// timestamp (UTC+03), delta t 0,75, 3-element array with elements 127+5i, 
// 84-9i, 75+,75i and empty variant.
test_variant_positive!(
    test_variant_named_c128_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(c128wf b"Waveform")))],
    v!(c128wf [c128!(127.0, i 5.0), c128!(84.0, i -9.0), c128!(75.0, i 0.75)],
              ts: (0x0000000000000000, 0x004189374BC6A7F0),
              incr: 0.75,
              attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                          => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x10\x40\x54\x00\x12\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\
        \x40\x05\xFC\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
          \x40\x01\x40\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x40\x05\x50\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
          \xC0\x02\x20\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
        \x40\x05\x2C\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
          \x3F\xFE\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00");

// Complex64 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 
// timestamp (UTC+03), delta t 0,75, 3-element array with elements 127+5i, 
// 84-9i, 75+,75i and empty variant.
test_variant_positive!(
    test_variant_named_c64_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(c64wf b"Waveform")))],
    v!(c64wf [c!(127.0, i 5.0), c!(84.0, i -9.0), c!(75.0, i 0.75)],
             ts: (0x0000000000000000, 0x004189374BC6A7F0),
             incr: 0.75,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x10\x40\x54\x00\x11\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\
        \x40\x5F\xC0\x00\x00\x00\x00\x00\x40\x14\x00\x00\x00\x00\x00\x00\
        \x40\x55\x00\x00\x00\x00\x00\x00\xC0\x22\x00\x00\x00\x00\x00\x00\
        \x40\x52\xC0\x00\x00\x00\x00\x00\x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00");

// Complex32 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 
// timestamp (UTC+03), delta t 0,75, 3-element array with elements 127+5i, 
// 84-9i, 75+,75i and empty variant.
test_variant_positive!(
    test_variant_named_c32_waveform,
    [e!(0x10, h!(0x40, Timestamped, d!(c32wf b"Waveform")))],
    v!(c32wf [c!(127.0, i 5.0), c!(84.0, i -9.0), c!(75.0, i 0.75)],
             ts: (0x0000000000000000, 0x004189374BC6A7F0),
             incr: 0.75,
             attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                         => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x10\x40\x54\x00\x10\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\
        \x42\xFE\x00\x00\x40\xA0\x00\x00\
        \x42\xA8\x00\x00\xC1\x10\x00\x00\
        \x42\x96\x00\x00\x3F\x40\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00");

// U8 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and 
// attribute foo=u32(0xAA) named `foobar`.
test_variant_positive!(
    test_variant_named_u8_waveform_with_foo_attribute,
    [e!(0x10, h!(0x40, Timestamped, d!(u8wf b"Waveform")))],
    v!(u8wf [127, 84, 75],
            ts: (0x0000000000015180, 0x004189374BC6A7F0),
            incr: 0.75,
            attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                        => v!(nothing),
                        root 0,
                        attributes [
                            b"foo" => var!([e!(0x0D,
                                               h!(0x40, U32,
                                                  d!(scalar b"foobar")))]
                                           => v!(u32 0xAA)),
                        ])),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x10\
          \x40\x54\
          \x00\x0B\
          \x08Waveform\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x01\x51\x80\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\
        \x7F\
        \x54\
        \x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\
          \x00\x04\
            \x00\x00\
          \x00\x01\
        \x00\x00\
        \x00\x00\x00\x01\
          \x00\x00\x00\x03foo\
          \x15\x00\x80\x00\
            \x00\x00\x00\x01\
              \x00\x0D\
                \x40\x07\
                \x00\x06foobar\
              \x00\x00\x01\
          \x00\x00\
          \x00\x00\x00\xAA\
          \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// U8 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp 
// (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and 
// attributes foo=u32(0xAA) named `foobar` and baz=i64(0x55) named `bazr`.
test_variant_positive!(
    test_variant_named_u8_waveform_with_foo_and_baz_attributes,
    [e!(0x10, h!(0x40, Timestamped, d!(u8wf b"Waveform")))],
    v!(u8wf [127, 84, 75],
            ts: (0x0000000000015180, 0x004189374BC6A7F0),
            incr: 0.75,
            attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                        => v!(nothing),
                        root 0,
                        attributes [
                            b"baz" => var!([e!(0x0B,
                                               h!(0x40, I64,
                                                  d!(scalar b"bazr")))]
                                           => v!(i64 0x55)),
                            b"foo" => var!([e!(0x0D,
                                               h!(0x40, U32,
                                                  d!(scalar b"foobar")))]
                                           => v!(u32 0xAA)),
                        ])),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x10\x40\x54\x00\x0B\x08Waveform\x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x01\x51\x80\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xE8\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x03\x7F\x54\x4B\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\
          \x00\x04\
            \x00\x00\
          \x00\x01\
        \x00\x00\
        \x00\x00\x00\x02\
          \x00\x00\x00\x03baz\
          \x15\x00\x80\x00\
            \x00\x00\x00\x01\
              \x00\x0B\
                \x40\x04\
                \x00\x04bazr\
              \x00\x00\x01\
            \x00\x00\
            \x00\x00\x00\x00\x00\x00\x00\x55\
            \x00\x00\x00\x00\
          \x00\x00\x00\x03foo\
          \x15\x00\x80\x00\
            \x00\x00\x00\x01\
              \x00\x0D\
                \x40\x07\
                \x00\x06foobar\
              \x00\x00\x01\
            \x00\x00\
            \x00\x00\x00\xAA\
            \x00\x00\x00\x00\
        \x00\x00\x00\x00");

// Valid occurrence named `occurrence`.
test_variant_positive!(
    test_variant_valid_named_occurrence,
    [e!(0x12, h!(0x40, Extended, d!(occurrence b"occurrence")))],
    v!(u32 0xE390_0827),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x12\
          \x40\x70\
          \x00\x04\
          \x0Aoccurrence\
        \x00\x00\x01\
      \x00\x00\
      \xE3\x90\x08\x27\
      \x00\x00\x00\x00");

// Invalid occurrence named `occur`.
test_variant_positive!(
    test_variant_invalid_named_occurrence,
    [e!(0x0C, h!(0x40, Extended, d!(occurrence b"occur")))],
    v!(u32 0),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x0C\
          \x40\x70\
          \x00\x04\
          \x05occur\
        \x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Invalid occurrence, not named.
test_variant_positive!(
    test_variant_invalid_unnamed_occurrence,
    [e!(0x6, h!(0x00, Extended, d!(occurrence b"")))],
    v!(u32 0),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x06\
          \x00\x70\
          \x00\x04\
        \x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Not a refnum constant output from relevant palette.
test_variant_positive!(
    test_variant_invalid_refnum_constant,
    [e!(0x06, h!(0x00, Extended, d!(invrefnum b"")))],
    v!(u32 0),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x06\
          \x00\x70\
          \x00\x00\
        \x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Digital waveform named `digital waveform out` with data data [[0xAA, 0x55, 
// 0x33], [0xFF, 0x77, 0xBB]] (two rows and three columns) and transitions 
// [0xAABBCCDD, 0x55773344].
test_variant_positive!(
    test_variant_digital_waveform_with_data_and_transitions,
    [e!(0x1C, h!(0x40, Timestamped, d!(dwf b"digital waveform out")))],
    v!(dwf ts: (0x0000000000015180, 0x004189374BC6A7F0),
           incr: 0.0,
           data: &[&[0xAA, 0x55, 0x33], &[0xFF, 0x77, 0xBB]],
           transitions: [0xAABBCCDD, 0x55773344],
           unknown0: 0x0002_3B03_6800_0000,
           attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                       => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x1C\
          \x40\x54\
          \x00\x08\
          \x14digital waveform out\
        \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x01\x51\x80\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x02\
        \xAA\xBB\xCC\xDD\
        \x55\x77\x33\x44\
      \x00\x00\x00\x02\
      \x00\x00\x00\x03\
        \xAA\x55\x33\
        \xFF\x77\xBB\
      \x00\x02\x3B\x03\x68\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Empty digital waveform constant named `digital waveform out`.
test_variant_positive!(
    test_variant_empty_digital_waveform,
    [e!(0x1C, h!(0x40, Timestamped, d!(dwf b"digital waveform out")))],
    v!(dwf ts: (0x0000000000000000, 0x0000000000000000),
           incr: 0.0,
           data: &[],
           transitions: [],
           unknown0: 0x0002_3B03_6800_0000,
           attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                       => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x1C\x40\x54\x00\x08\x14digital waveform out\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x02\x3B\x03\x68\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Digital waveform constant named `digital waveform out` with 3:00:00,001 
// 02.01.1904 timestamp (UTC+03) and delta t equal to 0,45.
test_variant_positive!(
    test_variant_digital_waveform,
    [e!(0x1C, h!(0x40, Timestamped, d!(dwf b"digital waveform out")))],
    v!(dwf ts: (0x0000000000015180, 0x004189374BC6A7F0),
           incr: 0.45,
           data: &[],
           transitions: [],
           unknown0: 0x0002_3B03_6800_0000,
           attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                       => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x1C\x40\x54\x00\x08\x14digital waveform out\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x01\x51\x80\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xDC\xCC\xCC\xCC\xCC\xCC\xCD\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x02\x3B\x03\x68\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Digital waveform constant named `digital waveform out` with 3:00:00,001 
// 02.01.1904 timestamp (UTC+03), delta t equal to 0,45 and data [[0xAA, 0x55, 
// 0x33], [0xFF, 0x77, 0xBB]] (two rows and three columns).
test_variant_positive!(
    test_variant_digital_waveform_with_data,
    [e!(0x1C, h!(0x40, Timestamped, d!(dwf b"digital waveform out")))],
    v!(dwf ts: (0x0000000000015180, 0x004189374BC6A7F0),
           incr: 0.45,
           data: &[&[0xAA, 0x55, 0x33], &[0xFF, 0x77, 0xBB]],
           transitions: [],
           unknown0: 0x0002_3B03_6800_0000,
           attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                       => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x1C\x40\x54\x00\x08\x14digital waveform out\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x01\x51\x80\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xDC\xCC\xCC\xCC\xCC\xCC\xCD\
      \x00\x00\x00\x00\
      \x00\x00\x00\x02\
      \x00\x00\x00\x03\
        \xAA\x55\x33\
        \xFF\x77\xBB\
      \x00\x02\x3B\x03\x68\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Digital waveform constant named `digital waveform out` with 3:00:00,001 
// 02.01.1904 timestamp (UTC+03), delta t equal to 0,45 and transitions 
// [0xAABBCCDD, 0x55773344].
test_variant_positive!(
    test_variant_digital_waveform_with_transitions,
    [e!(0x1C, h!(0x40, Timestamped, d!(dwf b"digital waveform out")))],
    v!(dwf ts: (0x0000000000015180, 0x004189374BC6A7F0),
           incr: 0.45,
           data: &[],
           transitions: [0xAABBCCDD, 0x55773344],
           unknown0: 0x0002_3B03_6800_0000,
           attrs: var!([e!(0x04, h!(0x00, Nothing, d!(nothing)))]
                       => v!(nothing))),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x1C\x40\x54\x00\x08\x14digital waveform out\
      \x00\x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\x00\x01\x51\x80\
      \x00\x41\x89\x37\x4B\xC6\xA7\xF0\
      \x3F\xDC\xCC\xCC\xCC\xCC\xCC\xCD\
      \x00\x00\x00\x02\
        \xAA\xBB\xCC\xDD\
        \x55\x77\x33\x44\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x02\x3B\x03\x68\x00\x00\x00\
      \x00\
      \x15\x00\x80\x00\
        \x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");

// Digital data with data [[0xAA, 0x55, 0x33], [0xFF, 0x77, 0xBB]] (two rows and 
// three columns), named `Y`.
test_variant_positive!(
    test_variant_digital_data,
    [e!(0x08, h!(0x40, Timestamped, d!(dd b"Y")))],
    v!(dd data: &[&[0xAA, 0x55, 0x33], &[0xFF, 0x77, 0xBB]],
          transitions: []),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\
        \x00\x08\
          \x40\x54\
          \x00\x07\
          \x01Y\
        \x00\x01\
      \x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x02\
      \x00\x00\x00\x03\
        \xAA\x55\x33\
        \xFF\x77\xBB\
      \x00\x00\x00\x00");

// Digital data with transitions [0xAABBCCDD, 0x55773344], named `Y`.
test_variant_positive!(
    test_variant_digital_data_transitions,
    [e!(0x08, h!(0x40, Timestamped, d!(dd b"Y")))],
    v!(dd data: &[],
          transitions: [0xAABBCCDD, 0x55773344]),
    b"\x15\x00\x80\x00\
      \x00\x00\x00\x01\x00\x08\x40\x54\x00\x07\x01Y\x00\x01\
      \x00\x00\
      \x00\x00\x00\x02\
        \xAA\xBB\xCC\xDD\
        \x55\x77\x33\x44\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00\
      \x00\x00\x00\x00");

/*
// EXT number 1e64, not named.
test_variant_positive!(
    test_variant_ext_1e64,
    [e!(0x05, h!(0x00, F128, d!(scalar b"")))],
    v!(f128 1e64),
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\
      \x40\xD3\x84\xF0\x3E\x93\xFF\x9F\x4D\xAA\x00\x00\x00\x00\x00\x00\
      \x00\x00\x00\x00");
*/

/*
// 
test_variant_positive!(
    ,
    [e!(0x08, h!(0x40, Timestamped, d!(dd b"Y")))],
    v!(dd data: &[&[0xAA, 0x55, 0x33], &[0xFF, 0x77, 0xBB]],
          transitions: [0xAABBCCDD, 0x55773344]),
    );
*/

/*
Test test_variant_nothing:
    Nothing named dumped_data (name actually not saved).
    1800 8000 0000 0001 0004 0000 0001 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00"

Test test_variant_named_i32:
    I32 number 0x80CD7FA5 (negative) named `max_samples_to_average`.
    1800 8000 0000 0001 001D 4003 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 0080 CD7F A500 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x03\x00\x16max_samples_to_average\x00\x00\x01\x00\x00\x80\xCD\x7F\xA5\x00\x00\x00\x00"

Test test_variant_named_u32:
    U32 number 0x80CD7FA5 named `max_samples_to_average`.
    1800 8000 0000 0001 001D 4007 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 0080 CD7F A500 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x07\x00\x16max_samples_to_average\x00\x00\x01\x00\x00\x80\xCD\x7F\xA5\x00\x00\x00\x00"

Test test_variant_typedefed_i32:
    I32 number 0x80CD7FA5 (negative) named `max_samples_to_average` and typedefed to test_variant_i32_typedef.ctl.
    1800 8000 0000 0001 0046 00F1 0000 0000 0000 0001 1C74 6573 745F 7661 7269 616E 745F 6933 325F 7479 7065 6465 662E 6374 6C00 2140 0300 166D 6178 5F73 616D 706C 6573 5F74 6F5F 6176 6572 6167 6500 0001 0000 80CD 7FA5 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00F\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x1Ctest_variant_i32_typedef.ctl\x00!@\x03\x00\x16max_samples_to_average\x00\x00\x01\x00\x00\x80\xCD\x7F\xA5\x00\x00\x00\x00"

Test test_variant_named_u64:
    U64 number 0x80CD7FA5 named `max_samples_to_average`.
    1800 8000 0000 0001 001D 4008 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 0000 0000 0080 CD7F A500 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x08\x00\x16max_samples_to_average\x00\x00\x01\x00\x00\x00\x00\x00\x00\x80\xCD\x7F\xA5\x00\x00\x00\x00"

Test test_variant_named_i64:
    I64 number 0x80CD7FA5 named `max_samples_to_average`.
    1800 8000 0000 0001 001D 4004 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 0000 0000 0080 CD7F A500 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x04\x00\x16max_samples_to_average\x00\x00\x01\x00\x00\x00\x00\x00\x00\x80\xCD\x7F\xA5\x00\x00\x00\x00"

Test test_variant_named_u16:
    U16 number 0x7FA5 named `max_samples_to_average`.
    1800 8000 0000 0001 001D 4006 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 007F A500 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x06\x00\x16max_samples_to_average\x00\x00\x01\x00\x00\x7F\xA5\x00\x00\x00\x00"

Test test_variant_named_i8:
    I8 number 0xA5 (negative) named `max_samples_to_average`.
    1800 8000 0000 0001 001D 4001 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 00A5 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x01\x00\x16max_samples_to_average\x00\x00\x01\x00\x00\xA5\x00\x00\x00\x00"

Test test_variant_named_u8:
    U8 number 0xA5 named `max_samples_to_average`.
    1800 8000 0000 0001 001D 4005 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 00A5 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x05\x00\x16max_samples_to_average\x00\x00\x01\x00\x00\xA5\x00\x00\x00\x00"

Test test_variant_named_ext:
    EXT number 0xA5 named `max_samples_to_average`.
    1800 8000 0000 0001 001D 400B 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 0040 064A 0000 0000 0000 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x0B\x00\x16max_samples_to_average\x00\x00\x01\x00\x00@\x06J\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_f64:
    F64 number 0xA5 named `max_samples_to_average`.
    1800 8000 0000 0001 001D 400A 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 0040 64A0 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x0A\x00\x16max_samples_to_average\x00\x00\x01\x00\x00@d\xA0\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_f32:
    F32 number 0xA5 named `max_samples_to_average`.
    1800 8000 0000 0001 001D 4009 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 0043 2500 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x09\x00\x16max_samples_to_average\x00\x00\x01\x00\x00C%\x00\x00\x00\x00\x00\x00"

Test test_variant_named_f32_nan:
    F32 number NaN named `max_samples_to_average`.
    1800 8000 0000 0001 001D 4009 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 0100 007F FFFF FF00 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1D@\x09\x00\x16max_samples_to_average\x00\x00\x01\x00\x00\x7F\xFF\xFF\xFF\x00\x00\x00\x00"

Test test_variant_f64_nan:
    F64 number NaN, not named.
    1800 8000 0000 0001 0005 000A 0000 0100 007F FFFF FFE0 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0A\x00\x00\x01\x00\x00\x7F\xFF\xFF\xFF\xE0\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_nan:
    EXT number NaN, not named.
    1800 8000 0000 0001 0005 000B 0000 0100 007F FFFF FFFE 0000 0000 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\x7F\xFF\xFF\xFF\xFE\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_inf:
    EXT number +inf, not named.
    1800 8000 0000 0001 0005 000B 0000 0100 007F FF00 0000 0000 0000 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\x7F\xFF\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_f64_inf:
    F64 number +inf, not named.
    1800 8000 0000 0001 0005 000A 0000 0100 007F F000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0A\x00\x00\x01\x00\x00\x7F\xF0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_f32_inf:
    F32 number +inf, not named.
    1800 8000 0000 0001 0005 0009 0000 0100 007F 8000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x09\x00\x00\x01\x00\x00\x7F\x80\x00\x00\x00\x00\x00\x00"

Test test_variant_f32_ninf:
    F32 number -inf, not named.
    1800 8000 0000 0001 0005 0009 0000 0100 00FF 8000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x09\x00\x00\x01\x00\x00\xFF\x80\x00\x00\x00\x00\x00\x00"

Test test_variant_f64_ninf:
    F64 number -inf, not named.
    1800 8000 0000 0001 0005 000A 0000 0100 00FF F000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0A\x00\x00\x01\x00\x00\xFF\xF0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_ninf:
    EXT number -inf, not named.
    1800 8000 0000 0001 0005 000B 0000 0100 00FF FF00 0000 0000 0000 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\xFF\xFF\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_nzero:
    EXT number -0, not named.
    1800 8000 0000 0001 0005 000B 0000 0100 0080 0000 0000 0000 0000 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_1e64:
    EXT number 1e64, not named.
    1800 8000 0000 0001 0005 000B 0000 0100 0040 D384 F03E 93FF 9F4D AA00 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00@\xD3\x84\xF0>\x93\xFF\x9FM\xAA\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_5646:
    EXT number 5646, not named.
    1800 8000 0000 0001 0005 000B 0000 0100 0040 0B60 E000 0000 0000 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00@\x0B`\xE0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_1en64:
    EXT number 1e-64, not named.
    1800 8000 0000 0001 0005 000B 0000 0100 003F 2A50 FFD4 4F4A 73D3 4C00 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00?*P\xFF\xD4OJs\xD3L\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_n1en64:
    EXT number -1e-64, not named.
    1800 8000 0000 0001 0005 000B 0000 0100 00BF 2A50 FFD4 4F4A 73D3 4C00 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\xBF*P\xFF\xD4OJs\xD3L\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_fxp_signed_64_32_5d5:
    Fixed-point 64-bit signed number with 32-bit integer part 5,5, not named.
    1800 8000 0000 0001 003C 005F 0351 0040 0000 0020 0001 0001 0000 0020 FFFF FFFF FFFF FFFF 0000 003F 0000 001F 7FFF FFFF FFFF FFFF 0000 0001 FFFF FFE1 0000 0000 0000 0001 0001 0000 0000 0005 8000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00<\x00_\x03Q\x00@\x00\x00\x00 \x00\x01\x00\x01\x00\x00\x00 \xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00\x00\x00?\x00\x00\x00\x1F\x7F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00\x00\x00\x01\xFF\xFF\xFF\xE1\x00\x00\x00\x00\x00\x00\x00\x01\x00\x01\x00\x00\x00\x00\x00\x05\x80\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_fxp_signed_64_32_n5d5_named:
    Fixed-point 64-bit signed number with 32-bit integer part -5,5, named `number`.
    1800 8000 0000 0001 0044 405F 0351 0040 0000 0020 0001 0001 0000 0020 FFFF FFFF FFFF FFFF 0000 003F 0000 001F 7FFF FFFF FFFF FFFF 0000 0001 FFFF FFE1 0000 0000 0000 0001 066E 756D 6265 7200 0001 0000 FFFF FFFA 8000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00D@_\x03Q\x00@\x00\x00\x00 \x00\x01\x00\x01\x00\x00\x00 \xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00\x00\x00?\x00\x00\x00\x1F\x7F\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00\x00\x00\x01\xFF\xFF\xFF\xE1\x00\x00\x00\x00\x00\x00\x00\x01\x06number\x00\x00\x01\x00\x00\xFF\xFF\xFF\xFA\x80\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_fxp_unsigned_64_32_5d5_named:
    Fixed-point 64-bit unsigned number with 32-bit integer part 5,5, named `number`.
    1800 8000 0000 0001 0044 405F 0311 0040 0000 0020 0000 0001 0000 0001 0000 0000 0000 0000 0000 0040 0000 0020 FFFF FFFF FFFF FFFF 0000 0001 FFFF FFE1 0000 0000 0000 0001 066E 756D 6265 7200 0001 0000 0000 0005 8000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00D@_\x03\x11\x00@\x00\x00\x00 \x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\x00\x00\x00 \xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00\x00\x00\x01\xFF\xFF\xFF\xE1\x00\x00\x00\x00\x00\x00\x00\x01\x06number\x00\x00\x01\x00\x00\x00\x00\x00\x05\x80\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_fxp_unsigned_8_8_64_named:
    Fixed-point 8-bit unsigned number with 8-bit integer part 64, named `number`.
    1800 8000 0000 0001 0044 405F 0311 0008 0000 0008 0000 0001 0000 0001 0000 0000 0000 0000 0000 0008 0000 0008 0000 0000 0000 00FF 0000 0001 0000 0001 0000 0000 0000 0001 066E 756D 6265 7200 0001 0000 0000 0000 0000 0040 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00D@_\x03\x11\x00\x08\x00\x00\x00\x08\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\xFF\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01\x06number\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00@\x00\x00\x00\x00"

Test test_variant_variant_nothing:
    Nothing named dumped_data and wrapped in another variant.
    1800 8000 0000 0001 0010 4053 0B64 756D 7065 645F 6461 7461 0001 0000 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@S\x0Bdumped_data\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_fxp_unsigned_8_0_0d5_named:
    Fixed-point 8-bit unsigned number with 0-bit integer part 0,5, named `number`.
    1800 8000 0000 0001 0044 405F 0311 0008 0000 0000 0000 0001 0000 0001 0000 0000 0000 0000 0000 0008 0000 0000 0000 0000 0000 00FF 0000 0001 FFFF FFF9 0000 0000 0000 0001 066E 756D 6265 7200 0001 0000 0000 0000 0000 0080 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00D@_\x03\x11\x00\x08\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xFF\x00\x00\x00\x01\xFF\xFF\xFF\xF9\x00\x00\x00\x00\x00\x00\x00\x01\x06number\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x00\x00\x00\x00"

Test test_variant_fxp_signed_8_0_n0d25_named:
    Fixed-point 8-bit signed number with 0-bit integer part -0,25, named `number`.
    1800 8000 0000 0001 0044 405F 0351 0008 0000 0000 0001 0001 0000 0000 FFFF FFFF FFFF FFFF 0000 0007 FFFF FFFF 0000 0000 0000 007F 0000 0001 FFFF FFF9 0000 0000 0000 0001 066E 756D 6265 7200 0001 0000 FFFF FFFF FFFF FFC0 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00D@_\x03Q\x00\x08\x00\x00\x00\x00\x00\x01\x00\x01\x00\x00\x00\x00\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00\x00\x00\x07\xFF\xFF\xFF\xFF\x00\x00\x00\x00\x00\x00\x00\x7F\x00\x00\x00\x01\xFF\xFF\xFF\xF9\x00\x00\x00\x00\x00\x00\x00\x01\x06number\x00\x00\x01\x00\x00\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xC0\x00\x00\x00\x00"

Test test_variant_cf32_n0d25_1d25i_named:
    Complex f32 number -0.25+1.25i, named `number`.
    1800 8000 0000 0001 000D 400C 0006 6E75 6D62 6572 0000 0100 00BE 8000 003F A000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0D@\x0C\x00\x06number\x00\x00\x01\x00\x00\xBE\x80\x00\x00?\xA0\x00\x00\x00\x00\x00\x00"

Test test_variant_cf64_n0d25_1d25i_named:
    Complex f64 number -0.25+1.25i, named `number`.
    1800 8000 0000 0001 000D 400D 0006 6E75 6D62 6572 0000 0100 00BF D000 0000 0000 003F F400 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0D@\x0D\x00\x06number\x00\x00\x01\x00\x00\xBF\xD0\x00\x00\x00\x00\x00\x00?\xF4\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_cext_n0d25_1d25i_named:
    Complex ext number -0.25+1.25i, named `number`.
    1800 8000 0000 0001 000D 400E 0006 6E75 6D62 6572 0000 0100 00BF FD00 0000 0000 0000 0000 0000 0000 003F FF40 0000 0000 0000 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0D@\x0E\x00\x06number\x00\x00\x01\x00\x00\xBF\xFD\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00?\xFF@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_typedefed_named_i32_array:
    Typedefed  to `test_variant_i32_array_typedef.ctl` array (named `max_samples_to_average_array`) of i32 numbers (named `max_samples_to_average`, name coming from typedef), array contains one 0x80CD7FA5 (negative) integer.
    1800 8000 0000 0002 001D 4003 0016 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 0000 5900 F100 0000 0000 0000 0122 7465 7374 5F76 6172 6961 6E74 5F69 3332 5F61 7272 6179 5F74 7970 6564 6566 2E63 746C 002E 4040 0001 FFFF FFFF 0000 1C6D 6178 5F73 616D 706C 6573 5F74 6F5F 6176 6572 6167 655F 6172 7261 7900 0001 0001 0000 0001 80CD 7FA5 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x02\x00\x1D@\x03\x00\x16max_samples_to_average\x00\x00Y\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x22test_variant_i32_array_typedef.ctl\x00.@@\x00\x01\xFF\xFF\xFF\xFF\x00\x00\x1Cmax_samples_to_average_array\x00\x00\x01\x00\x01\x00\x00\x00\x01\x80\xCD\x7F\xA5\x00\x00\x00\x00"

Test test_variant_named_typedefed_named_i32_array:
    Typedefed  to `test_variant_typedefed_i32_array_typedef.ctl` array (named `max_samples_to_average_array`) of i32 numbers (named `max_samples_to_average` and typedefed to `test_variant_i32_typedef.ctl`, name coming from typedef), array contains one -10 (negative) integer.
    1800 8000 0000 0002 0046 00F1 0000 0000 0000 0001 1C74 6573 745F 7661 7269 616E 745F 6933 325F 7479 7065 6465 662E 6374 6C00 2140 0300 166D 6178 5F73 616D 706C 6573 5F74 6F5F 6176 6572 6167 6500 0063 00F1 0000 0000 0000 0001 2C74 6573 745F 7661 7269 616E 745F 7479 7065 6465 6665 645F 6933 325F 6172 7261 795F 7479 7065 6465 662E 6374 6C00 2E40 4000 01FF FFFF FF00 001C 6D61 785F 7361 6D70 6C65 735F 746F 5F61 7665 7261 6765 5F61 7272 6179 0000 0100 0100 0000 01FF FFFF F600 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x02\x00F\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x1Ctest_variant_i32_typedef.ctl\x00!@\x03\x00\x16max_samples_to_average\x00\x00c\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01,test_variant_typedefed_i32_array_typedef.ctl\x00.@@\x00\x01\xFF\xFF\xFF\xFF\x00\x00\x1Cmax_samples_to_average_array\x00\x00\x01\x00\x01\x00\x00\x00\x01\xFF\xFF\xFF\xF6\x00\x00\x00\x00"

Test test_variant_named_named_boolean_4d_array:
    4D array named `falses` containing boolean values named `false`. Number of values is so that first dimension has size two, second dimension has size three, third dimension has size four and fourth dimension has size five. All values are false.
    1800 8000 0000 0002 000A 4021 0566 616C 7365 0020 4040 0004 FFFF FFFF FFFF FFFF FFFF FFFF FFFF FFFF 0000 0666 616C 7365 7300 0001 0001 0000 0002 0000 0003 0000 0004 0000 0005 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x02\x00\x0A@!\x05false\x00 @@\x00\x04\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00\x00\x06falses\x00\x00\x01\x00\x01\x00\x00\x00\x02\x00\x00\x00\x03\x00\x00\x00\x04\x00\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_named_boolean_4d_empty_array:
    4D array named `falses` containing boolean values named `false`. Array is empty.
    1800 8000 0000 0002 000A 4021 0566 616C 7365 0020 4040 0004 FFFF FFFF FFFF FFFF FFFF FFFF FFFF FFFF 0000 0666 616C 7365 7300 0001 0001 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x02\x00\x0A@!\x05false\x00 @@\x00\x04\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x00\x00\x06falses\x00\x00\x01\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_2xi32_cluster:
    Unnamed cluster with two unnamed I32 numbers: 0xAA55 and 0x77EE.
    1800 8000 0000 0002 0005 0003 0000 0A00 5000 0200 0000 0000 0100 0100 00AA 5500 0077 EE00 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x02\x00\x05\x00\x03\x00\x00\x0A\x00P\x00\x02\x00\x00\x00\x00\x00\x01\x00\x01\x00\x00\xAAU\x00\x00w\xEE\x00\x00\x00\x00"

Test test_variant_named_i32_i32_cluster:
    Unnamed cluster with two I32 numbers: 0xAA55 (named x) and 0x77EE (unnamed).
    1800 8000 0000 0003 0007 4003 0001 7800 0500 0300 000A 0050 0002 0000 0001 0001 0002 0000 AA55 0000 77EE 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x03\x00\x07@\x03\x00\x01x\x00\x05\x00\x03\x00\x00\x0A\x00P\x00\x02\x00\x00\x00\x01\x00\x01\x00\x02\x00\x00\xAAU\x00\x00w\xEE\x00\x00\x00\x00"

Test test_variant_named_2xi32_cluster:
    Unnamed cluster with two I32 numbers: 0xAA55 (named x) and 0x77EE (named y).
    1800 8000 0000 0003 0007 4003 0001 7800 0740 0300 0179 000A 0050 0002 0000 0001 0001 0002 0000 AA55 0000 77EE 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x03\x00\x07@\x03\x00\x01x\x00\x07@\x03\x00\x01y\x00\x0A\x00P\x00\x02\x00\x00\x00\x01\x00\x01\x00\x02\x00\x00\xAAU\x00\x00w\xEE\x00\x00\x00\x00"

Test test_variant_named_named_2xi32_cluster:
    Cluster named `clust` with two I32 numbers: 0xAA55 (named x) and 0x77EE (named y).
    1800 8000 0000 0003 0007 4003 0001 7800 0740 0300 0179 0010 4050 0002 0000 0001 0563 6C75 7374 0001 0002 0000 AA55 0000 77EE 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x03\x00\x07@\x03\x00\x01x\x00\x07@\x03\x00\x01y\x00\x10@P\x00\x02\x00\x00\x00\x01\x05clust\x00\x01\x00\x02\x00\x00\xAAU\x00\x00w\xEE\x00\x00\x00\x00"

Test test_variant_variant_variant_nothing:
    Nothing named dumped_data and wrapped in another variant in turn wrapped in variant.
    1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 0010 4053 0B64 756D 7065 645F 6461 7461 0001 0000 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@S\x0Bdumped_data\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_i32_named_named_i32_array_cluster:
    Cluster named `clust` with one I32 number 0xAA55 (named x) and array (named ys) with one I32 number 0x77EE (named y).
    1800 8000 0000 0004 0007 4003 0001 7800 0740 0300 0179 0010 4040 0001 FFFF FFFF 0001 0279 7300 0010 4050 0002 0000 0002 0563 6C75 7374 0001 0003 0000 AA55 0000 0001 0000 77EE 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x04\x00\x07@\x03\x00\x01x\x00\x07@\x03\x00\x01y\x00\x10@@\x00\x01\xFF\xFF\xFF\xFF\x00\x01\x02ys\x00\x00\x10@P\x00\x02\x00\x00\x00\x02\x05clust\x00\x01\x00\x03\x00\x00\xAAU\x00\x00\x00\x01\x00\x00w\xEE\x00\x00\x00\x00"

Test test_variant_named_i32_named_named_typedefed_i32_array_cluster:
    Cluster named `clust` with one I32 number 0xAA55 (named x) and array (named ys) with one I32 number 0x77EE (named y), number in array is typedefed to `test_variant_i32_typedef.ctl`.
    1800 8000 0000 0004 0007 4003 0001 7800 3000 F100 0000 0000 0000 011C 7465 7374 5F76 6172 6961 6E74 5F69 3332 5F74 7970 6564 6566 2E63 746C 000B 4003 0001 7900 1040 4000 01FF FFFF FF00 0102 7973 0000 1040 5000 0200 0000 0205 636C 7573 7400 0100 0300 00AA 5500 0000 0100 0077 EE00 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x04\x00\x07@\x03\x00\x01x\x000\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x1Ctest_variant_i32_typedef.ctl\x00\x0B@\x03\x00\x01y\x00\x10@@\x00\x01\xFF\xFF\xFF\xFF\x00\x01\x02ys\x00\x00\x10@P\x00\x02\x00\x00\x00\x02\x05clust\x00\x01\x00\x03\x00\x00\xAAU\x00\x00\x00\x01\x00\x00w\xEE\x00\x00\x00\x00"

Test test_variant_named_i32_named_named_typedefed_i32_array_cluster:
    Cluster named `clust` (typedefed to `test_variant_i32_typedefed_i32_array_cluster_typedef.ctl`) with one I32 number 0xAA55 (named x) and array (named ys) with one I32 number 0x77EE (named y), number in array is typedefed to `test_variant_i32_typedef.ctl`.
    1800 8000 0000 0004 0007 4003 0001 7800 3000 F100 0000 0000 0000 011C 7465 7374 5F76 6172 6961 6E74 5F69 3332 5F74 7970 6564 6566 2E63 746C 000B 4003 0001 7900 1040 4000 01FF FFFF FF00 0102 7973 0000 5500 F100 0000 0000 0000 0138 7465 7374 5F76 6172 6961 6E74 5F69 3332 5F74 7970 6564 6566 6564 5F69 3332 5F61 7272 6179 5F63 6C75 7374 6572 5F74 7970 6564 6566 2E63 746C 0014 4050 0002 0000 0002 0563 6C75 7374 0001 0003 0000 AA55 0000 0001 0000 77EE 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x04\x00\x07@\x03\x00\x01x\x000\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x1Ctest_variant_i32_typedef.ctl\x00\x0B@\x03\x00\x01y\x00\x10@@\x00\x01\xFF\xFF\xFF\xFF\x00\x01\x02ys\x00\x00U\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x018test_variant_i32_typedefed_i32_array_cluster_typedef.ctl\x00\x14@P\x00\x02\x00\x00\x00\x02\x05clust\x00\x01\x00\x03\x00\x00\xAAU\x00\x00\x00\x01\x00\x00w\xEE\x00\x00\x00\x00"

Test test_variant_named_error:
    Error constant named `error in (no error)` and containing no error (status false, code zero, empty message).
    1800 8000 0000 0004 000C 4021 0673 7461 7475 7300 000B 4003 0004 636F 6465 0000 1040 30FF FFFF FF06 736F 7572 6365 0000 2040 5000 0300 0000 0100 0213 6572 726F 7220 696E 2028 6E6F 2065 7272 6F72 2900 0100 0300 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x04\x00\x0C@!\x06status\x00\x00\x0B@\x03\x00\x04code\x00\x00\x10@0\xFF\xFF\xFF\xFF\x06source\x00\x00 @P\x00\x03\x00\x00\x00\x01\x00\x02\x13error in (no error)\x00\x01\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_lvobject:
    LabVIEW object constant named `foobaz`.
    1800 8000 0000 0001 0020 4070 001E 0000 0E4C 6162 5649 4557 204F 626A 6563 7400 0666 6F6F 6261 7A00 0001 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00 @p\x00\x1E\x00\x00\x0ELabVIEW Object\x00\x06foobaz\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_string:
    String containing `foobar`.
    1800 8000 0000 0001 0008 0030 FFFF FFFF 0001 0000 0000 0006 666F 6F62 6172 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x08\x000\xFF\xFF\xFF\xFF\x00\x01\x00\x00\x00\x00\x00\x06foobar\x00\x00\x00\x00"

Test test_variant_named_string:
    String named `str` containing `foobar`.
    1800 8000 0000 0001 000C 4030 FFFF FFFF 0373 7472 0001 0000 0000 0006 666F 6F62 6172 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@0\xFF\xFF\xFF\xFF\x03str\x00\x01\x00\x00\x00\x00\x00\x06foobar\x00\x00\x00\x00"

Test test_variant_named_relative_path:
    Path named `pth` containing `foo`.
    1800 8000 0000 0001 000C 4032 FFFF FFFF 0370 7468 0001 0000 5054 4830 0000 0008 0001 0001 0366 6F6F 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@2\xFF\xFF\xFF\xFF\x03pth\x00\x01\x00\x00PTH0\x00\x00\x00\x08\x00\x01\x00\x01\x03foo\x00\x00\x00\x00"

Test test_variant_absolute_path:
    Path containing `C:\foo`.
    1800 8000 0000 0001 0008 0032 FFFF FFFF 0001 0000 5054 4830 0000 000A 0000 0002 0143 0366 6F6F 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x08\x002\xFF\xFF\xFF\xFF\x00\x01\x00\x00PTH0\x00\x00\x00\x0A\x00\x00\x00\x02\x01C\x03foo\x00\x00\x00\x00"

Test test_variant_invalid_path:
    Not a path constant.
    1800 8000 0000 0001 0008 0032 FFFF FFFF 0001 0000 5054 4830 0000 0004 0002 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x08\x002\xFF\xFF\xFF\xFF\x00\x01\x00\x00PTH0\x00\x00\x00\x04\x00\x02\x00\x00\x00\x00\x00\x00"

Test test_variant_empty_path:
    Empty path constant.
    1800 8000 0000 0001 0008 0032 FFFF FFFF 0001 0000 5054 4830 0000 0004 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x08\x002\xFF\xFF\xFF\xFF\x00\x01\x00\x00PTH0\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_true_boolean:
    True constant.
    1800 8000 0000 0001 0004 0021 0001 0000 0100 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00!\x00\x01\x00\x00\x01\x00\x00\x00\x00"

Test test_variant_named_false_boolean:
    False constant named `false`.
    1800 8000 0000 0001 000A 4021 0566 616C 7365 0001 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0A@!\x05false\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_timestamp:
    Timestamp to date 3:00:00,001 01.01.1904.
    1800 8000 0000 0001 0006 0054 0006 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x06\x00T\x00\x06\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0\x00\x00\x00\x00"

Test test_variant_named_timestamp:
    Timestamp to date 3:00:00,001 01.01.1904 named `current time`.
    1800 8000 0000 0001 0014 4054 0006 0C63 7572 7265 6E74 2074 696D 6500 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x14@T\x00\x06\x0Ccurrent time\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0\x00\x00\x00\x00"

Test test_variant_named_instrument_descriptor:
    Instrument descriptor named `foodesc` pointing to instrument `foo`. Descriptor was not properly configured, but still uses `niDMM` instrument as IVI class.
    1800 8000 0000 0002 001E 0037 FFFF FFFF 0003 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 003C 4070 000F 056E 6944 4D4D 0001 0000 0000 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 1549 6E73 7472 756D 656E 7420 4465 7363 7269 7074 6F72 0001 0001 0000 0003 666F 6F00 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x02\x00\x1E\x007\xFF\xFF\xFF\xFF\x00\x03\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00<@p\x00\x0F\x05niDMM\x00\x01\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x15Instrument Descriptor\x00\x01\x00\x01\x00\x00\x00\x03foo\x00\x00\x00\x00"

Test test_variant_named_daqmx_task:
    DAQmx task named `DAQmx Task Name` pointing to task `bar`. Task was not properly configured.
    1800 8000 0000 0002 001E 0037 FFFF FFFF 0009 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 003A 4070 0015 0454 6173 6B00 0001 0000 054E 4944 4151 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 0F44 4151 6D78 2054 6173 6B20 4E61 6D65 0001 0001 0000 0003 6261 7200 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x02\x00\x1E\x007\xFF\xFF\xFF\xFF\x00\x09\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00:@p\x00\x15\x04Task\x00\x00\x01\x00\x00\x05NIDAQ\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x0FDAQmx Task Name\x00\x01\x00\x01\x00\x00\x00\x03bar\x00\x00\x00\x00"

Test test_lib_lib_cls_public_typedefed_bool:
    Boolean false typedefed to public control cls_public_bool.ctl inside class test_lib_nested_class.lvclass inside library test_lib_nested.lvlib inside library test_lib.lvlib.
    1800 8000 0000 0001 006D 00F1 0000 0000 0000 0004 0E74 6573 745F 6C69 622E 6C76 6C69 6215 7465 7374 5F6C 6962 5F6E 6573 7465 642E 6C76 6C69 621D 7465 7374 5F6C 6962 5F6E 6573 7465 645F 636C 6173 732E 6C76 636C 6173 7313 636C 735F 7075 626C 6963 5F62 6F6F 6C2E 6374 6C00 0E40 2104 626F 6F6C 0000 0100 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00m\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x04\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_class.lvclass\x13cls_public_bool.ctl\x00\x0E@!\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_lib_cls_protected_typedefed_bool:
    Boolean false typedefed to protected control cls_protected_bool.ctl inside class test_lib_nested_class.lvclass inside library test_lib_nested.lvlib inside library test_lib.lvlib.
    1800 8000 0000 0001 0070 00F1 0000 0000 0000 0004 0E74 6573 745F 6C69 622E 6C76 6C69 6215 7465 7374 5F6C 6962 5F6E 6573 7465 642E 6C76 6C69 621D 7465 7374 5F6C 6962 5F6E 6573 7465 645F 636C 6173 732E 6C76 636C 6173 7316 636C 735F 7072 6F74 6563 7465 645F 626F 6F6C 2E63 746C 000E 4021 0462 6F6F 6C00 0001 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00p\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x04\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_class.lvclass\x16cls_protected_bool.ctl\x00\x0E@!\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_lib_cls_private_typedefed_bool:
    Boolean false typedefed to private control cls_private_bool.ctl inside class test_lib_nested_class.lvclass inside library test_lib_nested.lvlib inside library test_lib.lvlib.
    1800 8000 0000 0001 006E 00F1 0000 0000 0000 0004 0E74 6573 745F 6C69 622E 6C76 6C69 6215 7465 7374 5F6C 6962 5F6E 6573 7465 642E 6C76 6C69 621D 7465 7374 5F6C 6962 5F6E 6573 7465 645F 636C 6173 732E 6C76 636C 6173 7314 636C 735F 7072 6976 6174 655F 626F 6F6C 2E63 746C 000E 4021 0462 6F6F 6C00 0001 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00n\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x04\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_class.lvclass\x14cls_private_bool.ctl\x00\x0E@!\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_lib_cls_community_typedefed_bool:
    Boolean false typedefed to community control cls_community_bool.ctl inside class test_lib_nested_class.lvclass inside library test_lib_nested.lvlib inside library test_lib.lvlib.
    1800 8000 0000 0001 0070 00F1 0000 0000 0000 0004 0E74 6573 745F 6C69 622E 6C76 6C69 6215 7465 7374 5F6C 6962 5F6E 6573 7465 642E 6C76 6C69 621D 7465 7374 5F6C 6962 5F6E 6573 7465 645F 636C 6173 732E 6C76 636C 6173 7316 636C 735F 636F 6D6D 756E 6974 795F 626F 6F6C 2E63 746C 000E 4021 0462 6F6F 6C00 0001 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00p\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x04\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_class.lvclass\x16cls_community_bool.ctl\x00\x0E@!\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_lib_cls_named_true:
    Class constant with private data bool=true named foobar belonging to class test_lib_nested_class.lvclass inside library test_lib_nested.lvlib inside library test_lib.lvlib. Owning class has version 1.0.0b1, test_lib_nested lib has version 1.0.0b0, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0056 4070 001E 0000 440E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 6E65 7374 6564 2E6C 766C 6962 1D74 6573 745F 6C69 625F 6E65 7374 6564 5F63 6C61 7373 2E6C 7663 6C61 7373 0000 0666 6F6F 6261 7200 0001 0000 0000 0001 440E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 6E65 7374 6564 2E6C 766C 6962 1D74 6573 745F 6C69 625F 6E65 7374 6564 5F63 6C61 7373 2E6C 7663 6C61 7373 0000 0000 0001 0000 0000 0001 0000 0001 0100 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00V@p\x00\x1E\x00\x00D\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_class.lvclass\x00\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01D\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_class.lvclass\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x01\x00\x00\x00\x00"

Test test_lib_lib_cls_named_false:
    Class constant with private data bool=false named foobar belonging to class test_lib_nested_class.lvclass inside library test_lib_nested.lvlib inside library test_lib.lvlib. Owning class has version 1.0.0b1, test_lib_nested lib has version 1.0.0b0, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0056 4070 001E 0000 440E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 6E65 7374 6564 2E6C 766C 6962 1D74 6573 745F 6C69 625F 6E65 7374 6564 5F63 6C61 7373 2E6C 7663 6C61 7373 0000 0666 6F6F 6261 7200 0001 0000 0000 0001 440E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 6E65 7374 6564 2E6C 766C 6962 1D74 6573 745F 6C69 625F 6E65 7374 6564 5F63 6C61 7373 2E6C 7663 6C61 7373 0000 0000 0001 0000 0000 0001 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00V@p\x00\x1E\x00\x00D\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_class.lvclass\x00\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01D\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_class.lvclass\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_community_typedefed_bool:
    Boolean false typedefed to community control lib_community_bool.ctl inside library test_lib.lvlib.
    1800 8000 0000 0001 003C 00F1 0000 0000 0000 0002 0E74 6573 745F 6C69 622E 6C76 6C69 6216 6C69 625F 636F 6D6D 756E 6974 795F 626F 6F6C 2E63 746C 000E 4021 0462 6F6F 6C00 0001 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00<\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x02\x0Etest_lib.lvlib\x16lib_community_bool.ctl\x00\x0E@!\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_public_typedefed_bool:
    Boolean false typedefed to public control lib_public_bool.ctl inside library test_lib.lvlib.
    1800 8000 0000 0001 0039 00F1 0000 0000 0000 0002 0E74 6573 745F 6C69 622E 6C76 6C69 6213 6C69 625F 7075 626C 6963 5F62 6F6F 6C2E 6374 6C00 0E40 2104 626F 6F6C 0000 0100 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x009\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x02\x0Etest_lib.lvlib\x13lib_public_bool.ctl\x00\x0E@!\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_private_typedefed_bool:
    Boolean false typedefed to private control lib_private_bool.ctl inside library test_lib.lvlib. Owning library has version 1.0.0b0
    1800 8000 0000 0001 003A 00F1 0000 0000 0000 0002 0E74 6573 745F 6C69 622E 6C76 6C69 6214 6C69 625F 7072 6976 6174 655F 626F 6F6C 2E63 746C 000E 4021 0462 6F6F 6C00 0001 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00:\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x02\x0Etest_lib.lvlib\x14lib_private_bool.ctl\x00\x0E@!\x04bool\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_typedefed_named_2xi32_cluster_notifier:
    Valid notifier named `notifier out` notifying about elements named `clust` which have type `test_variant_2xi32_typedef.ctl` which is a cluster with two I32 numbers named `x` and `y`.
    1800 8000 0000 0004 0007 4003 0001 7800 0740 0300 0179 003B 00F1 0000 0000 0000 0001 1E74 6573 745F 7661 7269 616E 745F 3278 6933 325F 7479 7065 6465 662E 6374 6C00 1440 5000 0200 0000 0105 636C 7573 7400 1840 7000 1100 0100 020C 6E6F 7469 6669 6572 206F 7574 0000 0100 03B7 9000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x04\x00\x07@\x03\x00\x01x\x00\x07@\x03\x00\x01y\x00;\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x1Etest_variant_2xi32_typedef.ctl\x00\x14@P\x00\x02\x00\x00\x00\x01\x05clust\x00\x18@p\x00\x11\x00\x01\x00\x02\x0Cnotifier out\x00\x00\x01\x00\x03\xB7\x90\x00\x00\x00\x00\x00\x00"

Test test_variant_named_typedefed_named_2xi32_cluster_queue:
    Valid queue named `queue out` containing elements named `clust` which have type `test_variant_2xi32_typedef.ctl` which is a cluster with two I32 numbers named `x` and `y`.
    1800 8000 0000 0004 0007 4003 0001 7800 0740 0300 0179 003B 00F1 0000 0000 0000 0001 1E74 6573 745F 7661 7269 616E 745F 3278 6933 325F 7479 7065 6465 662E 6374 6C00 1440 5000 0200 0000 0105 636C 7573 7400 1440 7000 1200 0100 0209 7175 6575 6520 6F75 7400 0100 03B7 A000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x04\x00\x07@\x03\x00\x01x\x00\x07@\x03\x00\x01y\x00;\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x1Etest_variant_2xi32_typedef.ctl\x00\x14@P\x00\x02\x00\x00\x00\x01\x05clust\x00\x14@p\x00\x12\x00\x01\x00\x02\x09queue out\x00\x01\x00\x03\xB7\xA0\x00\x00\x00\x00\x00\x00"

Test test_variant_named_typedefed_named_2xi32_cluster_dvr:
    Valid data value reference named `data value reference` containing element named `clust` which has type `test_variant_2xi32_typedef.ctl` which is a cluster with two I32 numbers named `x` and `y`.
    1800 8000 0000 0004 0007 4003 0001 7800 0740 0300 0179 003B 00F1 0000 0000 0000 0001 1E74 6573 745F 7661 7269 616E 745F 3278 6933 325F 7479 7065 6465 662E 6374 6C00 1440 5000 0200 0000 0105 636C 7573 7400 2140 7000 2000 0100 0200 1464 6174 6120 7661 6C75 6520 7265 6665 7265 6E63 6500 0001 0003 2820 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x04\x00\x07@\x03\x00\x01x\x00\x07@\x03\x00\x01y\x00;\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x1Etest_variant_2xi32_typedef.ctl\x00\x14@P\x00\x02\x00\x00\x00\x01\x05clust\x00!@p\x00 \x00\x01\x00\x02\x00\x14data value reference\x00\x00\x01\x00\x03( \x00\x00\x00\x00\x00\x00"

Test test_variant_named_typedefed_named_2xi32_cluster_uevent:
    Valid user event named `clust` containing elements named `clust` which have type `test_variant_2xi32_typedef.ctl` which is a cluster with two I32 numbers named `x` and `y`.
    1800 8000 0000 0004 0007 4003 0001 7800 0740 0300 0179 003B 00F1 0000 0000 0000 0001 1E74 6573 745F 7661 7269 616E 745F 3278 6933 325F 7479 7065 6465 662E 6374 6C00 1440 5000 0200 0000 0105 636C 7573 7400 1040 7000 1900 0100 0205 636C 7573 7400 0100 03B8 A000 0100 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x04\x00\x07@\x03\x00\x01x\x00\x07@\x03\x00\x01y\x00;\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x1Etest_variant_2xi32_typedef.ctl\x00\x14@P\x00\x02\x00\x00\x00\x01\x05clust\x00\x10@p\x00\x19\x00\x01\x00\x02\x05clust\x00\x01\x00\x03\xB8\xA0\x00\x01\x00\x00\x00\x00"

Test test_variant_valid_named_occurrence:
    Valid occurrence named `occurrence`.
    1800 8000 0000 0001 0012 4070 0004 0A6F 6363 7572 7265 6E63 6500 0001 0000 DE90 0151 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x12@p\x00\x04\x0Aoccurrence\x00\x00\x01\x00\x00\xDE\x90\x01Q\x00\x00\x00\x00"

Test test_variant_invalid_named_occurrence:
    Invalid occurrence named `occur`.
    1800 8000 0000 0001 000C 4070 0004 056F 6363 7572 0001 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@p\x00\x04\x05occur\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_invalid_unnamed_occurrence:
    Invalid occurrence, not named.
    1800 8000 0000 0001 0006 0070 0004 0001 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x06\x00p\x00\x04\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_invalid_refnum_constant:
    Not a refnum constant output from relevant palette.
    1800 8000 0000 0001 0006 0070 0000 0001 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x06\x00p\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_longnamed_named_named_2xboolean_cluster_named_named_typedefed_named_2xi32_cluster_queue_named_named_named_2xboolean_cluster_queue_named_named_2xboolean_cluster_cluster:
    Cluster constant with long name and elements 1) cluster named `ab` with two false booleans named `a` and `b`; 2) queue named `queue out` with elements being cluster named `clust` typedefed to `test_variant_2xi32_typedef.ctl` which has two I32 elements named `x` and `y`; 3) queue named `abqueue` with elements being cluster named `ab` with two booleans named `a` and `b` as elements; 4) cluster named `ab` with two false booleans named `a` and `b`. All queue constants do not represent any valid queue. The whole cluster is named `big-cluster--0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`
    1800 8000 0000 0009 0006 4021 0161 0006 4021 0162 000E 4050 0002 0000 0001 0261 6200 0007 4003 0001 7800 0740 0300 0179 003B 00F1 0000 0000 0000 0001 1E74 6573 745F 7661 7269 616E 745F 3278 6933 325F 7479 7065 6465 662E 6374 6C00 1440 5000 0200 0300 0405 636C 7573 7400 1440 7000 1200 0100 0509 7175 6575 6520 6F75 7400 1240 7000 1200 0100 0207 6162 7175 6575 6501 0E40 5000 0400 0200 0600 0700 02FF 6269 672D 636C 7573 7465 722D 3031 3233 3435 3637 3839 4142 4344 4546 4748 494A 4B4C 4D4E 4F50 5152 5354 5556 5758 595A 6162 6364 6566 6768 696A 6B6C 6D6E 6F70 7172 7374 7576 7778 797A 2D30 3132 3334 3536 3738 3941 4243 4445 4647 4849 4A4B 4C4D 4E4F 5051 5253 5455 5657 5859 5A61 6263 6465 6667 6869 6A6B 6C6D 6E6F 7071 7273 7475 7677 7879 7A2D 3031 3233 3435 3637 3839 4142 4344 4546 4748 494A 4B4C 4D4E 4F50 5152 5354 5556 5758 595A 6162 6364 6566 6768 696A 6B6C 6D6E 6F70 7172 7374 7576 7778 797A 2D30 3132 3334 3536 3738 3941 4243 4445 4647 4849 4A4B 4C4D 4E4F 5051 5253 5455 5657 5859 5A61 6263 6465 6667 6869 6A6B 6C6D 6E6F 7071 7200 0100 0800 0000 0000 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x09\x00\x06@!\x01a\x00\x06@!\x01b\x00\x0E@P\x00\x02\x00\x00\x00\x01\x02ab\x00\x00\x07@\x03\x00\x01x\x00\x07@\x03\x00\x01y\x00;\x00\xF1\x00\x00\x00\x00\x00\x00\x00\x01\x1Etest_variant_2xi32_typedef.ctl\x00\x14@P\x00\x02\x00\x03\x00\x04\x05clust\x00\x14@p\x00\x12\x00\x01\x00\x05\x09queue out\x00\x12@p\x00\x12\x00\x01\x00\x02\x07abqueue\x01\x0E@P\x00\x04\x00\x02\x00\x06\x00\x07\x00\x02\xFFbig-cluster-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqr\x00\x01\x00\x08\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_longnamed_boolean:
    Boolean true named `big-name-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`.
    1800 8000 0000 0001 0104 4021 FF62 6967 2D6E 616D 652D 3031 3233 3435 3637 3839 4142 4344 4546 4748 494A 4B4C 4D4E 4F50 5152 5354 5556 5758 595A 6162 6364 6566 6768 696A 6B6C 6D6E 6F70 7172 7374 7576 7778 797A 2D30 3132 3334 3536 3738 3941 4243 4445 4647 4849 4A4B 4C4D 4E4F 5051 5253 5455 5657 5859 5A61 6263 6465 6667 6869 6A6B 6C6D 6E6F 7071 7273 7475 7677 7879 7A2D 3031 3233 3435 3637 3839 4142 4344 4546 4748 494A 4B4C 4D4E 4F50 5152 5354 5556 5758 595A 6162 6364 6566 6768 696A 6B6C 6D6E 6F70 7172 7374 7576 7778 797A 2D30 3132 3334 3536 3738 3941 4243 4445 4647 4849 4A4B 4C4D 4E4F 5051 5253 5455 5657 5859 5A61 6263 6465 6667 6869 6A6B 6C6D 6E6F 7071 7273 7475 0001 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x01\x04@!\xFFbig-name-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_longnamed_i32:
    I32 0x7FAA5566 named `big-name-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`.
    1800 8000 0000 0001 0105 4003 00FF 6269 672D 6E61 6D65 2D30 3132 3334 3536 3738 3941 4243 4445 4647 4849 4A4B 4C4D 4E4F 5051 5253 5455 5657 5859 5A61 6263 6465 6667 6869 6A6B 6C6D 6E6F 7071 7273 7475 7677 7879 7A2D 3031 3233 3435 3637 3839 4142 4344 4546 4748 494A 4B4C 4D4E 4F50 5152 5354 5556 5758 595A 6162 6364 6566 6768 696A 6B6C 6D6E 6F70 7172 7374 7576 7778 797A 2D30 3132 3334 3536 3738 3941 4243 4445 4647 4849 4A4B 4C4D 4E4F 5051 5253 5455 5657 5859 5A61 6263 6465 6667 6869 6A6B 6C6D 6E6F 7071 7273 7475 7677 7879 7A2D 3031 3233 3435 3637 3839 4142 4344 4546 4748 494A 4B4C 4D4E 4F50 5152 5354 5556 5758 595A 6162 6364 6566 6768 696A 6B6C 6D6E 6F70 7172 7374 7500 0100 007F AA55 6600 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x01\x05@\x03\x00\xFFbig-name-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu\x00\x01\x00\x00\x7F\xAAUf\x00\x00\x00\x00"

Test test_lib_lib_cls2_named_true:
    Class constant with private data bool=true named foobar belonging to class test_lib_nested_clas2.lvclass inside library test_lib_nested.lvlib inside library test_lib.lvlib. Owning class has version 65535.21845.43690b48059, test_lib_nested lib has version 1.0.0b0, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0056 4070 001E 0000 440E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 6E65 7374 6564 2E6C 766C 6962 1D74 6573 745F 6C69 625F 6E65 7374 6564 5F63 6C61 7332 2E6C 7663 6C61 7373 0000 0666 6F6F 6261 7200 0001 0000 0000 0001 440E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 6E65 7374 6564 2E6C 766C 6962 1D74 6573 745F 6C69 625F 6E65 7374 6564 5F63 6C61 7332 2E6C 7663 6C61 7373 0000 0000 FFFF 5555 AAAA BBBB 0000 0001 0100 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00V@p\x00\x1E\x00\x00D\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_clas2.lvclass\x00\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01D\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_clas2.lvclass\x00\x00\x00\x00\xFF\xFFUU\xAA\xAA\xBB\xBB\x00\x00\x00\x01\x01\x00\x00\x00\x00"

Test test_lib_lib_cls2_named_false:
    Class constant with private data bool=false named foobar belonging to class test_lib_nested_clas2.lvclass inside library test_lib_nested.lvlib inside library test_lib.lvlib. Owning class has version 65535.21845.43690b48059, test_lib_nested lib has version 1.0.0b0, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0056 4070 001E 0000 440E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 6E65 7374 6564 2E6C 766C 6962 1D74 6573 745F 6C69 625F 6E65 7374 6564 5F63 6C61 7332 2E6C 7663 6C61 7373 0000 0666 6F6F 6261 7200 0001 0000 0000 0001 440E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 6E65 7374 6564 2E6C 766C 6962 1D74 6573 745F 6C69 625F 6E65 7374 6564 5F63 6C61 7332 2E6C 7663 6C61 7373 0000 0000 FFFF 5555 AAAA BBBB 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00V@p\x00\x1E\x00\x00D\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_clas2.lvclass\x00\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01D\x0Etest_lib.lvlib\x15test_lib_nested.lvlib\x1Dtest_lib_nested_clas2.lvclass\x00\x00\x00\x00\xFF\xFFUU\xAA\xAA\xBB\xBB\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_cls_named_empty:
    Class constant with private data str="" named `foobar` belonging to class `test_lib_class.lvclass` inside library `test_lib.lvlib`. Owning class has version 21845.43690.30583b17477, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0038 4070 001E 0000 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 732E 6C76 636C 6173 7300 0666 6F6F 6261 7200 0001 0000 0000 0001 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 732E 6C76 636C 6173 7300 5555 AAAA 7777 4445 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x008@p\x00\x1E\x00\x00'\x0Etest_lib.lvlib\x16test_lib_class.lvclass\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01'\x0Etest_lib.lvlib\x16test_lib_class.lvclass\x00UU\xAA\xAAwwDE\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_cls_named_nonempty:
    Class constant with private data str="foobarbaz" named `foobar` belonging to class `test_lib_class.lvclass` inside library `test_lib.lvlib`. Owning class has version 21845.43690.30583b17477, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0038 4070 001E 0000 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 732E 6C76 636C 6173 7300 0666 6F6F 6261 7200 0001 0000 0000 0001 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 732E 6C76 636C 6173 7300 5555 AAAA 7777 4445 0000 000D 0000 0009 666F 6F62 6172 6261 7A00 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x008@p\x00\x1E\x00\x00'\x0Etest_lib.lvlib\x16test_lib_class.lvclass\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01'\x0Etest_lib.lvlib\x16test_lib_class.lvclass\x00UU\xAA\xAAwwDE\x00\x00\x00\x0D\x00\x00\x00\x09foobarbaz\x00\x00\x00\x00"

Test test_lib_cls2_named_empty_empty:
    Class constant with private data str="", st2="" named `foobar` belonging to class `test_lib_clas2.lvclass` inside library `test_lib.lvlib`. Owning class has version 1.2.3b5, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0038 4070 001E 0000 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 322E 6C76 636C 6173 7300 0666 6F6F 6261 7200 0001 0000 0000 0001 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 322E 6C76 636C 6173 7300 0001 0002 0003 0005 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x008@p\x00\x1E\x00\x00'\x0Etest_lib.lvlib\x16test_lib_clas2.lvclass\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01'\x0Etest_lib.lvlib\x16test_lib_clas2.lvclass\x00\x00\x01\x00\x02\x00\x03\x00\x05\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_cls2_named_nonempty_empty:
    Class constant with private data str="abc", st2="" named `foobar` belonging to class `test_lib_clas2.lvclass` inside library `test_lib.lvlib`. Owning class has version 1.2.3b5, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0038 4070 001E 0000 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 322E 6C76 636C 6173 7300 0666 6F6F 6261 7200 0001 0000 0000 0001 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 322E 6C76 636C 6173 7300 0001 0002 0003 0005 0000 000B 0000 0003 6162 6300 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x008@p\x00\x1E\x00\x00'\x0Etest_lib.lvlib\x16test_lib_clas2.lvclass\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01'\x0Etest_lib.lvlib\x16test_lib_clas2.lvclass\x00\x00\x01\x00\x02\x00\x03\x00\x05\x00\x00\x00\x0B\x00\x00\x00\x03abc\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_cls2_named_empty_nonempty:
    Class constant with private data str="", st2="def" named `foobar` belonging to class `test_lib_clas2.lvclass` inside library `test_lib.lvlib`. Owning class has version 1.2.3b5, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0038 4070 001E 0000 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 322E 6C76 636C 6173 7300 0666 6F6F 6261 7200 0001 0000 0000 0001 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 322E 6C76 636C 6173 7300 0001 0002 0003 0005 0000 000B 0000 0000 0000 0003 6465 6600 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x008@p\x00\x1E\x00\x00'\x0Etest_lib.lvlib\x16test_lib_clas2.lvclass\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01'\x0Etest_lib.lvlib\x16test_lib_clas2.lvclass\x00\x00\x01\x00\x02\x00\x03\x00\x05\x00\x00\x00\x0B\x00\x00\x00\x00\x00\x00\x00\x03def\x00\x00\x00\x00"

Test test_lib_cls2_named_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl" named `foobar` belonging to class `test_lib_clas2.lvclass` inside library `test_lib.lvlib`. Owning class has version 1.2.3b5, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0038 4070 001E 0000 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 322E 6C76 636C 6173 7300 0666 6F6F 6261 7200 0001 0000 0000 0001 270E 7465 7374 5F6C 6962 2E6C 766C 6962 1674 6573 745F 6C69 625F 636C 6173 322E 6C76 636C 6173 7300 0001 0002 0003 0005 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x008@p\x00\x1E\x00\x00'\x0Etest_lib.lvlib\x16test_lib_clas2.lvclass\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01'\x0Etest_lib.lvlib\x16test_lib_clas2.lvclass\x00\x00\x01\x00\x02\x00\x03\x00\x05\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_cls3_named_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl" named `foobar` belonging to class `test_lib_class3.lvclass` inside library `test_lib.lvlib`. Owning class has version 1.3.5b7, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 003A 4070 001E 0000 280E 7465 7374 5F6C 6962 2E6C 766C 6962 1774 6573 745F 6C69 625F 636C 6173 7333 2E6C 7663 6C61 7373 0000 0666 6F6F 6261 7200 0001 0000 0000 0001 280E 7465 7374 5F6C 6962 2E6C 766C 6962 1774 6573 745F 6C69 625F 636C 6173 7333 2E6C 7663 6C61 7373 0000 0000 0001 0003 0005 0007 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00:@p\x00\x1E\x00\x00(\x0Etest_lib.lvlib\x17test_lib_class3.lvclass\x00\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01(\x0Etest_lib.lvlib\x17test_lib_class3.lvclass\x00\x00\x00\x00\x00\x01\x00\x03\x00\x05\x00\x07\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_cls4_named_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl" named `foobar` belonging to class `test_lib_cla4.lvclass` inside library `test_lib.lvlib`. Owning class has version 127.45.44b43, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0038 4070 001E 0000 260E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 636C 6134 2E6C 7663 6C61 7373 0000 0666 6F6F 6261 7200 0001 0000 0000 0001 260E 7465 7374 5F6C 6962 2E6C 766C 6962 1574 6573 745F 6C69 625F 636C 6134 2E6C 7663 6C61 7373 0000 007F 002D 002C 002B 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x008@p\x00\x1E\x00\x00&\x0Etest_lib.lvlib\x15test_lib_cla4.lvclass\x00\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01&\x0Etest_lib.lvlib\x15test_lib_cla4.lvclass\x00\x00\x00\x7F\x00-\x00,\x00+\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_cls5_named_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl" named `foobar` belonging to class `test_lib_cl5.lvclass` inside library `test_lib.lvlib`. Owning class has version 1.5.10b50, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0036 4070 001E 0000 250E 7465 7374 5F6C 6962 2E6C 766C 6962 1474 6573 745F 6C69 625F 636C 352E 6C76 636C 6173 7300 0666 6F6F 6261 7200 0001 0000 0000 0001 250E 7465 7374 5F6C 6962 2E6C 766C 6962 1474 6573 745F 6C69 625F 636C 352E 6C76 636C 6173 7300 0000 0001 0005 000A 0032 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x006@p\x00\x1E\x00\x00%\x0Etest_lib.lvlib\x14test_lib_cl5.lvclass\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\x01%\x0Etest_lib.lvlib\x14test_lib_cl5.lvclass\x00\x00\x00\x00\x01\x00\x05\x00\x0A\x002\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_cls6_unnamed_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl", not named, belonging to class `test_lib_c6.lvclass` inside library `test_lib.lvlib`. Owning class has version 1.42.84b126, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 002E 0070 001E 0000 240E 7465 7374 5F6C 6962 2E6C 766C 6962 1374 6573 745F 6C69 625F 6336 2E6C 7663 6C61 7373 0000 0001 0000 0000 0001 240E 7465 7374 5F6C 6962 2E6C 766C 6962 1374 6573 745F 6C69 625F 6336 2E6C 7663 6C61 7373 0000 0000 0001 002A 0054 007E 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00.\x00p\x00\x1E\x00\x00$\x0Etest_lib.lvlib\x13test_lib_c6.lvclass\x00\x00\x00\x01\x00\x00\x00\x00\x00\x01$\x0Etest_lib.lvlib\x13test_lib_c6.lvclass\x00\x00\x00\x00\x00\x01\x00*\x00T\x00~\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_cls7_unnamed_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl", not named, belonging to class `test_lib_7.lvclass` inside library `test_lib.lvlib`. Owning class has version 1.2.3b700, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 002C 0070 001E 0000 230E 7465 7374 5F6C 6962 2E6C 766C 6962 1274 6573 745F 6C69 625F 372E 6C76 636C 6173 7300 0001 0000 0000 0001 230E 7465 7374 5F6C 6962 2E6C 766C 6962 1274 6573 745F 6C69 625F 372E 6C76 636C 6173 7300 0001 0002 0003 02BC 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00,\x00p\x00\x1E\x00\x00#\x0Etest_lib.lvlib\x12test_lib_7.lvclass\x00\x00\x01\x00\x00\x00\x00\x00\x01#\x0Etest_lib.lvlib\x12test_lib_7.lvclass\x00\x00\x01\x00\x02\x00\x03\x02\xBC\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_cls8_named_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl", named `a`, belonging to class `test_li8.lvclass` inside library `test_lib.lvlib`. Owning class has version 2.4.8b16, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 002C 4070 001E 0000 210E 7465 7374 5F6C 6962 2E6C 766C 6962 1074 6573 745F 6C69 382E 6C76 636C 6173 7300 0161 0001 0000 0000 0001 210E 7465 7374 5F6C 6962 2E6C 766C 6962 1074 6573 745F 6C69 382E 6C76 636C 6173 7300 0000 0002 0004 0008 0010 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00,@p\x00\x1E\x00\x00!\x0Etest_lib.lvlib\x10test_li8.lvclass\x00\x01a\x00\x01\x00\x00\x00\x00\x00\x01!\x0Etest_lib.lvlib\x10test_li8.lvclass\x00\x00\x00\x00\x02\x00\x04\x00\x08\x00\x10\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_cls9_named_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl", named `a`, belonging to class `test9.lvclass` inside library `test_lib.lvlib`. Owning class has version 127.63.31b15, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 002A 4070 001E 0000 1E0E 7465 7374 5F6C 6962 2E6C 766C 6962 0D74 6573 7439 2E6C 7663 6C61 7373 0000 0161 0001 0000 0000 0001 1E0E 7465 7374 5F6C 6962 2E6C 766C 6962 0D74 6573 7439 2E6C 7663 6C61 7373 0000 007F 003F 001F 000F 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00*@p\x00\x1E\x00\x00\x1E\x0Etest_lib.lvlib\x0Dtest9.lvclass\x00\x00\x01a\x00\x01\x00\x00\x00\x00\x00\x01\x1E\x0Etest_lib.lvlib\x0Dtest9.lvclass\x00\x00\x00\x7F\x00?\x00\x1F\x00\x0F\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_cls10_named_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl", named `a`, belonging to class `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 1.4.16b64, test_lib has version 1.0.0b0.
    1800 8000 0000 0001 0026 4070 001E 0000 1A0E 7465 7374 5F6C 6962 2E6C 766C 6962 0961 2E6C 7663 6C61 7373 0000 0161 0001 0000 0000 0001 1A0E 7465 7374 5F6C 6962 2E6C 766C 6962 0961 2E6C 7663 6C61 7373 0000 0001 0004 0010 0040 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00&@p\x00\x1E\x00\x00\x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\x01a\x00\x01\x00\x00\x00\x00\x00\x01\x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\x00\x01\x00\x04\x00\x10\x00@\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_cls11_named_nonempty_nonempty:
    Class constant with private data str="ghi", st2="jkl", named `a`, belonging to class `b.lvclass` . Owning class has version 64.61.63b62.
    1800 8000 0000 0001 0016 4070 001E 0000 0B09 622E 6C76 636C 6173 7300 0161 0001 0000 0000 0001 0B09 622E 6C76 636C 6173 7300 0040 003D 003F 003E 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x16@p\x00\x1E\x00\x00\x0B\x09b.lvclass\x00\x01a\x00\x01\x00\x00\x00\x00\x00\x01\x0B\x09b.lvclass\x00\x00@\x00=\x00?\x00>\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_variant_variant_variant_variant_true:
    True constant named dumped_data and wrapped in another variant which is twice wrapped in variant.
    1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 0010 4021 0B64 756D 7065 645F 6461 7461 0001 0000 0100 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@!\x0Bdumped_data\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_longtypedefed_named_bool:
    Boolean false named `OK button` typedefed to control `long_name_long_name.ctl` which is located in library `long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name.lvlib` which is located in library `long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name.lvlib`.
    1800 8000 0000 0001 01CE 00F1 D839 B265 0000 0003 CD6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 2E6C 766C 6962 CD6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 2E6C 766C 6962 176C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 2E63 746C 0012 4021 094F 4B20 4275 7474 6F6E 0001 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x01\xCE\x00\xF1\xD89\xB2e\x00\x00\x00\x03\xCDlong_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name.lvlib\xCDlong_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name.lvlib\x17long_name_long_name.ctl\x00\x12@!\x09OK Button\x00\x01\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_longnamed_longnamed_longnamed_cls:
    Class constant named `long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name.lvlib:long_name_long_name_long_name_long_name.lvclass` of a class named `long_name_long_name_long_name_long_name.lvclass` with version 10.20.30b40 living in library `long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name.lvlib`.
    1800 8000 0000 0001 0206 4070 001E 0000 FFCD 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 652E 6C76 6C69 622F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 652E 6C76 636C 6173 7300 FD6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 2E6C 766C 6962 3A6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 5F6C 6F6E 675F 6E61 6D65 2E6C 7663 6C61 7373 0001 0000 0000 0001 FFCD 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 652E 6C76 6C69 622F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 655F 6C6F 6E67 5F6E 616D 652E 6C76 636C 6173 7300 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x02\x06@p\x00\x1E\x00\x00\xFF\xCDlong_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name.lvlib/long_name_long_name_long_name_long_name.lvclass\x00\xFDlong_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name.lvlib:long_name_long_name_long_name_long_name.lvclass\x00\x01\x00\x00\x00\x00\x00\x01\xFF\xCDlong_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name_long_name.lvlib/long_name_long_name_long_name_long_name.lvclass\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_child_named_all_nonempty:
    Class constant with private data xxx="ghi", yyy="jkl", class `a` private data str="yx", st2="xy", named `foo`, belonging to class `a-child.lvclass` which inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
    1800 8000 0000 0001 002E 4070 001E 0000 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0366 6F6F 0001 0000 0000 0002 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0000 0036 0057 0017 0031 0001 0004 0010 0040 0000 000C 0000 0002 7978 0000 0002 7879 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00.@p\x00\x1E\x00\x00 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x03foo\x00\x01\x00\x00\x00\x00\x00\x02 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\x006\x00W\x00\x17\x001\x00\x01\x00\x04\x00\x10\x00@\x00\x00\x00\x0C\x00\x00\x00\x02yx\x00\x00\x00\x02xy\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_child_unnamed_all_empty:
    Class constant with private data xxx="", yyy="", class `a` private data str="", st2="", not named, belonging to class `a-child.lvclass` which inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
    1800 8000 0000 0001 002A 0070 001E 0000 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0001 0000 0000 0002 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0000 0036 0057 0017 0031 0001 0004 0010 0040 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00*\x00p\x00\x1E\x00\x00 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x01\x00\x00\x00\x00\x00\x02 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\x006\x00W\x00\x17\x001\x00\x01\x00\x04\x00\x10\x00@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_child_named_child_empty:
    Class constant with private data xxx="", yyy="", class `a` private data str="y", st2="x", named `x`, belonging to class `a-child.lvclass` which inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
    1800 8000 0000 0001 002C 4070 001E 0000 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0178 0001 0000 0000 0002 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0000 0036 0057 0017 0031 0001 0004 0010 0040 0000 000A 0000 0001 7900 0000 0178 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00,@p\x00\x1E\x00\x00 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x01x\x00\x01\x00\x00\x00\x00\x00\x02 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\x006\x00W\x00\x17\x001\x00\x01\x00\x04\x00\x10\x00@\x00\x00\x00\x0A\x00\x00\x00\x01y\x00\x00\x00\x01x\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_child_named_parent_empty:
    Class constant with private data xxx="e", yyy="x", class `a` private data str="", st2="", named `x`, belonging to class `a-child.lvclass` which inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
    1800 8000 0000 0001 002C 4070 001E 0000 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0178 0001 0000 0000 0002 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0000 0036 0057 0017 0031 0001 0004 0010 0040 0000 0000 0000 000A 0000 0001 6500 0000 0178 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00,@p\x00\x1E\x00\x00 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x01x\x00\x01\x00\x00\x00\x00\x00\x02 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\x006\x00W\x00\x17\x001\x00\x01\x00\x04\x00\x10\x00@\x00\x00\x00\x00\x00\x00\x00\x0A\x00\x00\x00\x01e\x00\x00\x00\x01x\x00\x00\x00\x00"

Test test_lib_child_cast_named_all_nonempty:
    Class constant with private data xxx="ghi", yyy="jkl", class `a` private data str="yx", st2="xy", named `foo`, belonging to class `a-child.lvclass`, cast to class `a.lvclass`, which inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
    1800 8000 0000 0001 003E 4070 001E 0000 1A0E 7465 7374 5F6C 6962 2E6C 766C 6962 0961 2E6C 7663 6C61 7373 0000 1874 6573 745F 6C69 622E 6C76 6C69 623A 612E 6C76 636C 6173 7300 0001 0000 0000 0002 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0000 0036 0057 0017 0031 0001 0004 0010 0040 0000 000C 0000 0002 7978 0000 0002 7879 0000 000E 0000 0003 6768 6900 0000 036A 6B6C 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00>@p\x00\x1E\x00\x00\x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\x18test_lib.lvlib:a.lvclass\x00\x00\x01\x00\x00\x00\x00\x00\x02 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\x006\x00W\x00\x17\x001\x00\x01\x00\x04\x00\x10\x00@\x00\x00\x00\x0C\x00\x00\x00\x02yx\x00\x00\x00\x02xy\x00\x00\x00\x0E\x00\x00\x00\x03ghi\x00\x00\x00\x03jkl\x00\x00\x00\x00"

Test test_lib_child_cast_unnamed_all_empty:
    Class constant with private data xxx="", yyy="", class `a` private data str="", st2="", not named, belonging to class `a-child.lvclass`, cast to class `a.lvclass`, which inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
    1800 8000 0000 0001 003E 4070 001E 0000 1A0E 7465 7374 5F6C 6962 2E6C 766C 6962 0961 2E6C 7663 6C61 7373 0000 1874 6573 745F 6C69 622E 6C76 6C69 623A 612E 6C76 636C 6173 7300 0001 0000 0000 0002 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0000 0036 0057 0017 0031 0001 0004 0010 0040 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00>@p\x00\x1E\x00\x00\x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\x18test_lib.lvlib:a.lvclass\x00\x00\x01\x00\x00\x00\x00\x00\x02 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\x006\x00W\x00\x17\x001\x00\x01\x00\x04\x00\x10\x00@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_child_cast_named_child_empty:
    Class constant with private data xxx="", yyy="", class `a` private data str="y", st2="x", named `x`, belonging to class `a-child.lvclass`, cast to class `a.lvclass`, which inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
    1800 8000 0000 0001 003E 4070 001E 0000 1A0E 7465 7374 5F6C 6962 2E6C 766C 6962 0961 2E6C 7663 6C61 7373 0000 1874 6573 745F 6C69 622E 6C76 6C69 623A 612E 6C76 636C 6173 7300 0001 0000 0000 0002 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0000 0036 0057 0017 0031 0001 0004 0010 0040 0000 000A 0000 0001 7900 0000 0178 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00>@p\x00\x1E\x00\x00\x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\x18test_lib.lvlib:a.lvclass\x00\x00\x01\x00\x00\x00\x00\x00\x02 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\x006\x00W\x00\x17\x001\x00\x01\x00\x04\x00\x10\x00@\x00\x00\x00\x0A\x00\x00\x00\x01y\x00\x00\x00\x01x\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_lib_child_cast_named_parent_empty:
    Class constant with private data xxx="e", yyy="x", class `a` private data str="", st2="", named `x`, belonging to class `a-child.lvclass`, cast to class `a.lvclass`, which inherits from class `a.lvclass` inside library `test_lib.lvlib`. Owning class has version 54.87.23b49, test_lib has version 1.0.0b0, `a.lvclass` has version 1.4.16b64.
    1800 8000 0000 0001 003E 4070 001E 0000 1A0E 7465 7374 5F6C 6962 2E6C 766C 6962 0961 2E6C 7663 6C61 7373 0000 1874 6573 745F 6C69 622E 6C76 6C69 623A 612E 6C76 636C 6173 7300 0001 0000 0000 0002 200E 7465 7374 5F6C 6962 2E6C 766C 6962 0F61 2D63 6869 6C64 2E6C 7663 6C61 7373 0000 0000 0036 0057 0017 0031 0001 0004 0010 0040 0000 0000 0000 000A 0000 0001 6500 0000 0178 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00>@p\x00\x1E\x00\x00\x1A\x0Etest_lib.lvlib\x09a.lvclass\x00\x00\x18test_lib.lvlib:a.lvclass\x00\x00\x01\x00\x00\x00\x00\x00\x02 \x0Etest_lib.lvlib\x0Fa-child.lvclass\x00\x00\x00\x00\x006\x00W\x00\x17\x001\x00\x01\x00\x04\x00\x10\x00@\x00\x00\x00\x00\x00\x00\x00\x0A\x00\x00\x00\x01e\x00\x00\x00\x01x\x00\x00\x00\x00"

Test test_variant_variant_variant_variant_variant_true:
    True constant named dumped_data and wrapped in another variant which is thrice wrapped in variant.
    1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 0010 4021 0B64 756D 7065 645F 6461 7461 0001 0000 0100 0000 0000 0000 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@!\x0Bdumped_data\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_c128_waveform:
    Complex128 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127+5i, 84-9i, 75+,75i and empty variant.
    1800 8000 0000 0001 0010 4054 0012 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 4005 FC00 0000 0000 0000 0000 0000 0000 4001 4000 0000 0000 0000 0000 0000 0000 4005 5000 0000 0000 0000 0000 0000 0000 C002 2000 0000 0000 0000 0000 0000 0000 4005 2C00 0000 0000 0000 0000 0000 0000 3FFE 8000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x12\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03@\x05\xFC\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\x01@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\x05P\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xC0\x02 \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\x05,\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00?\xFE\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_c64_waveform:
    Complex64 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127+5i, 84-9i, 75+,75i and empty variant.
    1800 8000 0000 0001 0010 4054 0011 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 405F C000 0000 0000 4014 0000 0000 0000 4055 0000 0000 0000 C022 0000 0000 0000 4052 C000 0000 0000 3FE8 0000 0000 0000 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x11\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03@_\xC0\x00\x00\x00\x00\x00@\x14\x00\x00\x00\x00\x00\x00@U\x00\x00\x00\x00\x00\x00\xC0\x22\x00\x00\x00\x00\x00\x00@R\xC0\x00\x00\x00\x00\x00?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_c32_waveform:
    Complex32 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127+5i, 84-9i, 75+,75i and empty variant.
    1800 8000 0000 0001 0010 4054 0010 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 42FE 0000 40A0 0000 42A8 0000 C110 0000 4296 0000 3F40 0000 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x10\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03B\xFE\x00\x00@\xA0\x00\x00B\xA8\x00\x00\xC1\x10\x00\x00B\x96\x00\x00?@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_unnamed_c32_waveform:
    Complex32 unnamed waveform constant with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127+5i, 84-9i, 75+,75i and empty variant.
    1800 8000 0000 0001 0010 4054 0010 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 42FE 0000 40A0 0000 42A8 0000 C110 0000 4296 0000 3F40 0000 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x10\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03B\xFE\x00\x00@\xA0\x00\x00B\xA8\x00\x00\xC1\x10\x00\x00B\x96\x00\x00?@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_variant_variant_variant_nothing:
    Nothing named dumped_data and wrapped in another variant which is twice wrapped in variant.
    1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 0010 4053 0B64 756D 7065 645F 6461 7461 0001 0000 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@S\x0Bdumped_data\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_empty_waveform:
    Empty waveform constant named `Waveform`.
    1800 8000 0000 0001 0010 4054 0003 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0000 0000 0000 0000 3FF0 0000 0000 0000 0000 0000 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x03\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00?\xF0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_f128_waveform:
    F128 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 000A 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 4005 FC00 0000 0000 0000 0000 0000 0000 4005 5000 0000 0000 0000 0000 0000 0000 4005 2C00 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x0A\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03@\x05\xFC\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\x05P\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\x05,\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_f32_waveform:
    F32 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 0005 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 42FE 0000 42A8 0000 4296 0000 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x05\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03B\xFE\x00\x00B\xA8\x00\x00B\x96\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_dbl_waveform:
    DBL waveform constant named `Waveform` with 3:00:00,001 01.01.1904 timestamp (UTC+03), delta t 0,45, 3-element array with elements 0,77, 0,78, 0,79 and empty variant.
    1800 8000 0000 0001 0010 4054 0003 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FDC CCCC CCCC CCCD 0000 0003 3FE8 A3D7 0A3D 70A4 3FE8 F5C2 8F5C 28F6 3FE9 47AE 147A E148 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x03\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xDC\xCC\xCC\xCC\xCC\xCC\xCD\x00\x00\x00\x03?\xE8\xA3\xD7\x0A=p\xA4?\xE8\xF5\xC2\x8F\x5C(\xF6?\xE9G\xAE\x14z\xE1H\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_i32_waveform:
    I32 waveform constant named `Waveform` with 3:00:00,001 01.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 000F 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 0000 007F 0000 0054 0000 004B 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x0F\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x7F\x00\x00\x00T\x00\x00\x00K\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_u64_waveform:
    U64 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 0014 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 0000 0000 0000 007F 0000 0000 0000 0054 0000 0000 0000 004B 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x14\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00\x7F\x00\x00\x00\x00\x00\x00\x00T\x00\x00\x00\x00\x00\x00\x00K\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_i64_waveform:
    I64 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 0013 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 0000 0000 0000 007F 0000 0000 0000 0054 0000 0000 0000 004B 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x13\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00\x7F\x00\x00\x00\x00\x00\x00\x00T\x00\x00\x00\x00\x00\x00\x00K\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_i16_waveform:
    I16 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 0002 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 007F 0054 004B 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x02\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x7F\x00T\x00K\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_i8_waveform:
    I8 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 000E 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 7F54 4B00 0000 0000 0000 0000 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x0E\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x7FTK\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_u32_waveform:
    U32 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 000D 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 0000 007F 0000 0054 0000 004B 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x0D\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x7F\x00\x00\x00T\x00\x00\x00K\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_u16_waveform:
    U16 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 000C 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 007F 0054 004B 0000 0000 0000 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x0C\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x7F\x00T\x00K\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_u8_waveform:
    U8 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and empty variant.
    1800 8000 0000 0001 0010 4054 000B 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 7F54 4B00 0000 0000 0000 0000 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x0B\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x7FTK\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_u8_waveform_with_foo_attribute:
    U8 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and attribute foo=u32(0xAA) named `foobar`.
    1800 8000 0000 0001 0010 4054 000B 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0001 5180 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 7F54 4B00 0000 0000 0000 0000 1800 8000 0000 0001 0004 0000 0001 0000 0000 0001 0000 0003 666F 6F18 0080 0000 0000 0100 0D40 0700 0666 6F6F 6261 7200 0001 0000 0000 00AA 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x0B\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01Q\x80\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x7FTK\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x01\x00\x00\x00\x03foo\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0D@\x07\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\xAA\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_named_u8_waveform_with_foo_attribute:
    U8 waveform constant named `Waveform` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127, 84, 75 and attributes foo=u32(0xAA) named `foobar` and baz=i64(0x55) named `bazr`.
    1800 8000 0000 0001 0010 4054 000B 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0001 5180 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 7F54 4B00 0000 0000 0000 0000 1800 8000 0000 0001 0004 0000 0001 0000 0000 0002 0000 0003 6261 7A18 0080 0000 0000 0100 0B40 0400 0462 617A 7200 0001 0000 0000 0000 0000 0055 0000 0000 0000 0003 666F 6F18 0080 0000 0000 0100 0D40 0700 0666 6F6F 6261 7200 0001 0000 0000 00AA 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x0B\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01Q\x80\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x7FTK\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x02\x00\x00\x00\x03baz\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0B@\x04\x00\x04bazr\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00U\x00\x00\x00\x00\x00\x00\x00\x03foo\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0D@\x07\x00\x06foobar\x00\x00\x01\x00\x00\x00\x00\x00\xAA\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_variant_variant_variant_nothing:
    Nothing named dumped_data and wrapped in another variant which is twice wrapped in variant.
    1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 000C 4053 0756 6172 6961 6E74 0001 0000 1800 8000 0000 0001 0010 4053 0B64 756D 7065 645F 6461 7461 0001 0000 1800 8000 0000 0001 0004 0000 0001 0000 0000 0000 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x0C@S\x07Variant\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x10@S\x0Bdumped_data\x00\x01\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_empty_digital_waveform:
    Empty digital waveform constant named `digital waveform out`.
    1800 8000 0000 0001 001C 4054 0008 1464 6967 6974 616C 2077 6176 6566 6F72 6D20 6F75 7400 0001 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0002 3B03 6800 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1C@T\x00\x08\x14digital waveform out\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02;\x03h\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_digital_waveform:
    Digital waveform constant named `digital waveform out` with 3:00:00,001 02.01.1904 timestamp (UTC+03) and delta t equal to 0,45.
    1800 8000 0000 0001 001C 4054 0008 1464 6967 6974 616C 2077 6176 6566 6F72 6D20 6F75 7400 0001 0000 0000 0000 0001 5180 0041 8937 4BC6 A7F0 3FDC CCCC CCCC CCCD 0000 0000 0000 0000 0000 0000 0002 3B03 6800 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1C@T\x00\x08\x14digital waveform out\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01Q\x80\x00A\x897K\xC6\xA7\xF0?\xDC\xCC\xCC\xCC\xCC\xCC\xCD\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02;\x03h\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_digital_data:
    Digital data with data [[0xAA, 0x55, 0x33], [0xFF, 0x77, 0xBB]] (two rows and three columns), named `Y`.
    1800 8000 0000 0001 0008 4054 0007 0159 0001 0000 0000 0000 0000 0002 0000 0003 AA55 33FF 77BB 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x08@T\x00\x07\x01Y\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x03\xAAU3\xFFw\xBB\x00\x00\x00\x00"

Test test_variant_digital_waveform_with_data:
    Digital waveform constant named `digital waveform out` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t equal to 0,45 and data [[0xAA, 0x55, 0x33], [0xFF, 0x77, 0xBB]] (two rows and three columns).
    1800 8000 0000 0001 001C 4054 0008 1464 6967 6974 616C 2077 6176 6566 6F72 6D20 6F75 7400 0001 0000 0000 0000 0001 5180 0041 8937 4BC6 A7F0 3FDC CCCC CCCC CCCD 0000 0000 0000 0002 0000 0003 AA55 33FF 77BB 0002 3B03 6800 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1C@T\x00\x08\x14digital waveform out\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01Q\x80\x00A\x897K\xC6\xA7\xF0?\xDC\xCC\xCC\xCC\xCC\xCC\xCD\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x03\xAAU3\xFFw\xBB\x00\x02;\x03h\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_digital_data_transitions:
    Digital data with transitions [0xAABBCCDD, 0x55773344], named `Y`.
    1800 8000 0000 0001 0008 4054 0007 0159 0001 0000 0000 0002 AABB CCDD 5577 3344 0000 0000 0000 0000 0000 0000
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x08@T\x00\x07\x01Y\x00\x01\x00\x00\x00\x00\x00\x02\xAA\xBB\xCC\xDDUw3D\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_digital_waveform_with_transitions:
    Digital waveform constant named `digital waveform out` with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t equal to 0,45 and transitions [0xAABBCCDD, 0x55773344].
    1800 8000 0000 0001 001C 4054 0008 1464 6967 6974 616C 2077 6176 6566 6F72 6D20 6F75 7400 0001 0000 0000 0000 0001 5180 0041 8937 4BC6 A7F0 3FDC CCCC CCCC CCCD 0000 0002 AABB CCDD 5577 3344 0000 0000 0000 0000 0002 3B03 6800 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1C@T\x00\x08\x14digital waveform out\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01Q\x80\x00A\x897K\xC6\xA7\xF0?\xDC\xCC\xCC\xCC\xCC\xCC\xCD\x00\x00\x00\x02\xAA\xBB\xCC\xDDUw3D\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02;\x03h\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_digital_waveform_with_data_and_transitions:
    Digital waveform named `digital waveform out` with data data [[0xAA, 0x55, 0x33], [0xFF, 0x77, 0xBB]] (two rows and three columns) and transitions [0xAABBCCDD, 0x55773344].
    1800 8000 0000 0001 001C 4054 0008 1464 6967 6974 616C 2077 6176 6566 6F72 6D20 6F75 7400 0001 0000 0000 0000 0001 5180 0041 8937 4BC6 A7F0 0000 0000 0000 0000 0000 0002 AABB CCDD 5577 3344 0000 0002 0000 0003 AA55 33FF 77BB 0002 3B03 6800 0000 0018 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x18\x00\x80\x00\x00\x00\x00\x01\x00\x1C@T\x00\x08\x14digital waveform out\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x01Q\x80\x00A\x897K\xC6\xA7\xF0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\xAA\xBB\xCC\xDDUw3D\x00\x00\x00\x02\x00\x00\x00\x03\xAAU3\xFFw\xBB\x00\x02;\x03h\x00\x00\x00\x00\x18\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"


Test test_variant_ext_1e64:
    EXT number 1e64, not named.
    1500 8000 0000 0001 0005 000B 0000 0100 0040 D384 F03E 93FF 9F4D AA00 0000 0000 0000 0000 0000
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00@\xD3\x84\xF0>\x93\xFF\x9FM\xAA\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_1en64:
    EXT number 1e-64, not named.
    1500 8000 0000 0001 0005 000B 0000 0100 003F 2A50 FFD4 4F4A 73D3 4C00 0000 0000 0000 0000 0000
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00?*P\xFF\xD4OJs\xD3L\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

Test test_variant_ext_n1en64:
    EXT number -1e-64, not named.
    1500 8000 0000 0001 0005 000B 0000 0100 00BF 2A50 FFD4 4F4A 73D3 4C00 0000 0000 0000 0000 0000
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x05\x00\x0B\x00\x00\x01\x00\x00\xBF*P\xFF\xD4OJs\xD3L\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
 */

/*
FIXME: Does not look like unnamed

Test test_variant_unnamed_c32_waveform:
    Complex32 unnamed waveform constant with 3:00:00,001 02.01.1904 timestamp (UTC+03), delta t 0,75, 3-element array with elements 127+5i, 84-9i, 75+,75i and empty variant.
    1500 8000 0000 0001 0010 4054 0010 0857 6176 6566 6F72 6D00 0001 0000 0000 0000 0000 0000 0041 8937 4BC6 A7F0 3FE8 0000 0000 0000 0000 0003 42FE 0000 40A0 0000 42A8 0000 C110 0000 4296 0000 3F40 0000 0000 0000 0000 0000 0015 0080 0000 0000 0100 0400 0000 0100 0000 0000 0000 0000 00
    b"\x15\x00\x80\x00\x00\x00\x00\x01\x00\x10@T\x00\x10\x08Waveform\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00A\x897K\xC6\xA7\xF0?\xE8\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03B\xFE\x00\x00@\xA0\x00\x00B\xA8\x00\x00\xC1\x10\x00\x00B\x96\x00\x00?@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x15\x00\x80\x00\x00\x00\x00\x01\x00\x04\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
 */

// vim: tw=80
