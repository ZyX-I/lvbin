nnoremap <buffer> ,C w"tyt:0jw"dy$jj"by$
nnoremap <buffer> ,P o<Esc>"ep$"dpjj"tP/);<CR>:nohl<CR>"bP
nnoremap <buffer> ,x s<C-r>=printf('\x%02X', char2nr(@"))<CR><Esc>
nnoremap <buffer> ,s i\<CR><Esc>
nnoremap <buffer> ,S k$hjli\<CR><Esc>
syn sync fromstart

setlocal textwidth=80
